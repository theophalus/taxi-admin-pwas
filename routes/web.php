<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

/*
 * ----------------------------------------------------------------------------
 * HOME
 * ----------------------------------------------------------------------------
 * */
Route::get('/home', 'HomeController@index')->name('home');

/*
 * ----------------------------------------------------------------------------
 * ADMIN USERS
 * ----------------------------------------------------------------------------
 * */
Route::get('/home/edit_user/{id}', 'HomeController@edit_user')->name('edit_user');
Route::post('/home/update_user/{id}', 'HomeController@update_user')->name('update_user');
Route::get('/users', 'HomeController@users')->name('users');
Route::post('/create_user', 'HomeController@create_user')->name('create_user');
Route::get('/delete_user/{id}', 'HomeController@delete_user')->name('delete_user');
Route::get('/user_details/{id}', 'HomeController@user_details')->name('user_details');

/*
 * ----------------------------------------------------------------------------
 * TAXI USERS
 * ----------------------------------------------------------------------------
 * */
Route::get('/taxi_users', 'HomeController@taxi_users')->name('taxi_users');
Route::get('/delete_taxi_user/{id}', 'HomeController@delete_taxi_user')->name('delete_taxi_user');
Route::get('/taxi_user_details/{id}', 'HomeController@taxi_user_details')->name('taxi_user_details');
Route::post('/home/update_taxi_user/{id}', 'HomeController@update_taxi_user')->name('update_taxi_user');
Route::post('/home/edit_taxi_user/{id}', 'HomeController@edit_taxi_user')->name('edit_taxi_user');

/*
 * ----------------------------------------------------------------------------
 * TAXI ASSOCIATIONS
 * ----------------------------------------------------------------------------
 * */
Route::get('/taxi_assocs', 'HomeController@taxi_assocs')->name('taxi_assocs');
Route::get('/main', 'TaxiController@main')->name('taxi_assocs');
Route::get('/home/edit_taxi_assoc/{id}', 'HomeController@edit_taxi_assoc')->name('edit_taxi_assoc');
Route::post('/home/update_taxi_assoc/{id}', 'HomeController@update_taxi_assoc')->name('update_taxi_assoc');
Route::get('/delete_taxi_assoc/{id}', 'HomeController@delete_taxi_assoc')->name('delete_taxi_assoc');
Route::get('/taxi_assoc_details/{id}', 'HomeController@taxi_assoc_details')->name('taxi_assoc_details');
Route::post('/taxi_assoc_approval_status/{id}', 'HomeController@taxi_assoc_approval_status')->name('taxi_assoc_approval_status');

Route::get('/taxi_assoc_edit_approval_status/{id}', 'HomeController@taxi_assoc_edit_approval_status')->name('taxi_assoc_edit_approval_status');

Route::get('/registers', 'HomeController@registers')->name('registers');



Route::get('/approval_status/{id}', 'HomeController@approval_status')->name('approval_status');

Route::get('/payments', 'HomeController@payments')->name('payments');
Route::get('/transactions', 'HomeController@transactions')->name('transactions');

/*
 * ----------------------------------------------------------------------------
 * 401 ERRORS
 * ----------------------------------------------------------------------------
 * */

Route::get('/errors', 'HomeController@errors')->name('errors');
