<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>Relief Fund</title>
<link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
<link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">

<link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">





        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="signin.css" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="welcome.css" /> 
        <link rel="manifest" href="manifest.json">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!--<script src="login.js"></script>-->
        <script>
            /*function myFunction() {
                var w = window.outerWidth;
                var h = window.outerHeight;
                var txt = "Window size: width=" + w + ", height=" + h;
                document.getElementById("demo").innerHTML = txt;
            }*/

            //Login.js file
            $(document).ready(function()
{
	if('serviceWorker' in navigator) 
	{ 
		//Register the Service Worker
		navigator.serviceWorker.register('sw2.js').then(function(registration) 
		{
			registration.update();
			registration.installing; // the installing worker, or undefined
			registration.waiting; // the waiting worker, or undefined
			registration.active; // the active worker, or undefine
			console.log('Service Worker Registered2');
		});
	} else console.log('Your browser does not support the Service-Worker!');
		if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
		{
			window.location.href = "profile.html"; // 	if Session created redirect to Profile page
		}
}); 


function checkIdFormat() {
	var id = document.getElementById("idNum").value;

	if(isNaN(id))
	{
		var newMyval = id.substring(0,15);
		document.getElementById("idNum").value = newMyval;
		document.getElementById("sender").disabled = true; 
		document.getElementById("error_message").innerHTML = "<br/><span style='color:#ffffff;font-family: eras-demi-itc;'>NUMBERS ONLY PLEASE!</span>";	
	}
	else
	{
		var newMyval = id.substring(0,15);
		document.getElementById("idNum").value = newMyval;
		document.getElementById("sender").disabled = false;
		document.getElementById("error_message").innerHTML = "";
	}
}
/*

	Authentification code function 

*/
        </script>
    </head>

    <body id="loginPage"> 
        <div class="container">
            <!-- Row start to create 10% margin spacing here-->
            <div class="row">
                <div class="col-md-12" style="margin-top:0%;">&nbsp;</div>
            </div>
            <!-- Row Ends to create 10% margin spacing here-->

            <!-- Row start to divide into 3 sections left margin-->
            <div class="row">
                <!-- Start Collumn for left margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for left margin-->
<br/><br/><br/><br/>
                <!-- Start Center Collumn where all information will be sitting -->
                <div class="col-md-4">
                    <div class="mainBlock">
                        <div class="logoDiv">
                            <img src="relief.JPG" width="250" />  
                        </div>  
                        
                        <div class="mainBlockSection">
                            <p id="error_message" align="center"> </p>
                            <form class="form-signin chooseForm" action="upload.php" method="post" style="margin-top:0px;padding-top:0px;">
                                <h4 align="center" style="font-family: Helvetica">Please choose your relevant description
                                </h4> 
                                <?php
                                if(isset($_GET['reason']))
                                {
                                    echo '<p style="font-family: Helvetica">Invalid '.$_GET['reason'].'</p>';
                                } 
                                ?>
                                <input type="button" onclick="Auhtenticater_func('Taxi Owner')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Taxi Owner" name="memberType">
                                <br/>
                                <input type="button" onclick="Auhtenticater_func('Taxi Driver')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Taxi Driver" name="memberType">
                                <br/>
								<input type="button" onclick="Auhtenticater_func('Rank Marshall')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Rank Marshall" name="memberType">
                                <br/>
                                <input type="button" onclick="Auhtenticater_func('Queue Marshall')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Queue Marshall" name="memberType">
                                <br/>
                                <input type="button" onclick="Auhtenticater_func('Association Staff')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Association Staff" name="memberType">
                                <br/>
                                <input type="button" onclick="Auhtenticater_func('Other')" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Other">
                               <input type="hidden" id="memberType" name="memberType" />
                               </form>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 33%;">
                                        <a href="http://www.taxitimes.net"><img src="logos-02.jpg" width="95" height="95" /></a>
                                    </td>
                                    <td style="width: 33%;text-align:center">
                                        <a href="https://api.whatsapp.com/send?phone=27660714008&text=Hi"> <img src="Whatsapp-512.png" width="60" height="60" ></a>
                                    </td>
                                    <td style="width: 33%;text-align:left;">
                                        <a href="http://www.santaco.co.za"><img src="logos-01.jpg" width="98" height="94" /></a>
                                    </td>
                                </tr>
                            </table>
                           <br/>
                           <table style="width: 100%;">
                                <tr>
                                    <td style="width: 100%;background-color: #000000;text-align: center;padding-top:2px;padding-bottom:2px;">
                                        <a href="https://sacoronavirus.co.za" style="color: #ffffff; font-size:11px;text-align: center;font-family: Helvetica">For more information about the coronavirus click here
</a>
                                    </td>
                                </tr>
<tr>
                                    <td style="width: 100%;text-align: center;padding-top:2px;padding-bottom:2px;">
                                        <a href="downloadPdf.php"  style="font-family: Helvetica;color: #000000; font-size:11px;text-align: center;">Click here for Terms and Conditions
</a>
                                    </td>
                                </tr>

                            </table>
                            <script>
                                //THIS SCRIPT SECTION WILL BE MOVED UP INTO HEAD ONCE RETURN KEY FUNCTIONING IS WORKING
                                function testEnter(){
                                    console.log("Return Button pressed");
                                }
                                
                                document.getElementById('loginPage').onkeydown = function(e){
                                    if(e.keyCode === 13){
                                        Auhtenticater_func();
                                        e.preventDefault();
                                    }
                                };

                            </script>
                            
                            <br/>
                        </div>
                    </div> 
                </div>
                <!-- End Center Collumn where all information will be sitting -->

                <!-- Start Collumn for right margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for right margin-->
            </div>
            <!-- End Row Main with 3 sections division (left margin/ middle for data / right margin)-->
        </div>
        <!-- Container Ends here-->
<script>
function Auhtenticater_func(memberType)
{
	//location.href="upload.html";
	document.getElementById("memberType").value = memberType;
	document.querySelector(".chooseForm").submit();
}
</script>
    </body>
</html>