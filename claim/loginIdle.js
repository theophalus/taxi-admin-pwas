function userTimedOut() {
    if (typeof timeOutObj != "undefined") {
        clearTimeout(timeOutObj);
    }

    timeOutObj = setTimeout(function () {
        localStorage.clear();
        window.location = "logout.html";
    }, 300000);
}