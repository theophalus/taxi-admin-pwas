<?php

try {

$dsn = "mysql:host=localhost;dbname=taxi";
$user = "root";
$passwd = "k4k3fBk%bdJS20"; 

$conn = new PDO($dsn, $user, $passwd);



$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );



} catch (PDOException $e) {



    echo "Failed to get DB handle: " . $e->getMessage() . "\n";



    exit;



  }

?>

<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>Relief Fund</title>
<link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
<link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">

<link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="signin.css" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="welcome.css" />
        <link rel="manifest" href="manifest.json">
        <!--<script src="login.js"></script>-->
        <script>
            /*function myFunction() {
                var w = window.outerWidth;
                var h = window.outerHeight;
                var txt = "Window size: width=" + w + ", height=" + h;
                document.getElementById("demo").innerHTML = txt;
            }*/

            //Login.js file
            $(document).ready(function()
{
if(localStorage.getItem("LandedOnExpiry"))
{
 location.href ="index.html";
}
    $( "#expirydate" ).datepicker({  dateFormat: 'yy-mm-dd' });
    $( "#pnpexpiry" ).datepicker({  dateFormat: 'yy-mm-dd' });
	if('serviceWorker' in navigator)
	{
		//Register the Service Worker
		navigator.serviceWorker.register('sw2.js').then(function(registration)
		{
			registration.update();
			registration.installing; // the installing worker, or undefined
			registration.waiting; // the waiting worker, or undefined
			registration.active; // the active worker, or undefine
			console.log('Service Worker Registered2');
		});
	} else console.log('Your browser does not support the Service-Worker!');
		if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
		{
			window.location.href = "profile.html"; // 	if Session created redirect to Profile page
		}
});



/*

	Authentification code function

*/
        </script>


        <script>
            /*check if the id has alrady been registered*/
            function checkIdAvailability() {
                jQuery.ajax({
                    url: "validate.php",
                    data:'displayName='+$("#idnum").val(),
                    type: "POST",
                    success:function(data){
                        $("#idmsg").html(data);
                        $("#loaderIcon").hide();
                    },
                    error:function (){}
                });
            }
        </script>


        <style>
            #tooltiptextEmail {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:550px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextLicense {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:670px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextID {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:220px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    @media only screen and (max-width: 768px) {
        #tooltiptextID {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:220px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
    #tooltiptextLicense {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:790px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
            #tooltiptextEmail {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:645px;
    left:120px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
    }


       #tooltiptextPhone {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:560px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

        </style>
    </head>

    <body id="loginPage" style="font-family: Helvetica;">
        <div class="container">
            <!-- Row start to create 10% margin spacing here-->
            <div class="row">
                <div class="col-md-12" style="margin-top:0%;">&nbsp;</div>
            </div>
            <!-- Row Ends to create 10% margin spacing here-->
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

            <!-- Row start to divide into 3 sections left margin-->
            <div class="row">
                <!-- Start Collumn for left margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for left margin-->

                <!-- Start Center Collumn where all information will be sitting -->
                <div class="col-md-4">
                    <div class="mainBlock">
                        <div class="logoDiv">
                            <img src="relief.JPG" width="250" />
                        </div>

                        <div class="mainBlockSection">
                        <div id="tooltiptextLicense">Please Enter a Valid License Number</div>
                            <div id="tooltiptextEmail"  style="font-family: Helvetica">Needs to be a valid email eg. john@doe.com</div>
                            <div id="tooltiptextPhone">Only numerics allowed eg. 0821234567</div>
                            <form class="form-signin registerForm" id="registerForm" id="registerForm" action="otpv1.php" method="post" style="margin-top:0px;padding-top:0px;">
                                <h4 align="center"  style="font-family: Helvetica">Please confirm and complete the details below
                                </h4>
                                <?php

                                //print_r("<pre>");var_dump($_POST);die();

                               


if(isset($_POST['previousId']))
{ 
?>

<input type="hidden" id="regIdNow1" name="regIdNow" value="<?php echo $_POST['regIdNow1']; ?>" />
<input type="hidden" name="ini1" id="ini" class="form-control" value="<?php echo $_POST['ini1']; ?>" required>
<input type="hidden" name="pnrLicenseExpiryDate1" id="pnrLicenseExpiryDate" value="<?php echo $_POST['pnrLicenseExpiryDate1']; ?>">
<input type="hidden" name="pnrLicense1" id="pnrLicense" value="<?php echo $_POST['pnrLicense1']; ?>" class="form-control">
<input type="hidden" name="licenseExpiryDate1" id="licenseExpiryDate" value="<?php echo $_POST['licenseExpiryDate1']; ?>">
<input type="hidden" name="licenseNo1" id="licenseNo" value="<?php echo $_POST['licenseNo1']; ?>" class="form-control">
<input type="hidden" name="cell1" id="cell" value="<?php echo $_POST['cell1']; ?>" class="form-control">
<input type="hidden" name="surname1" id="surname" value="<?php echo $_POST['surname1']; ?>"> 
<input type="hidden" name="fullName1" id="fullName" value="<?php echo $_POST['fullName1']; ?>">
<input type="hidden" name="memberType1" id="memberType" value="<?php echo $_POST['memberType1']; ?>">
<input type="hidden" name="idnum1" id="idnum" value="<?php echo $_POST['idnum1']; ?>">
<input type="hidden" name="gender1" id="gender" value="<?php echo $_POST['gender1']; ?>">
<input type="hidden" name="email1" id="email" value="<?php echo $_POST['email1']; ?>">
<input type="hidden" name="city1" id="city1" value="<?php echo $_POST['city1']; ?>">
<input type="hidden" name="province1" value="<?php echo $_POST['province1']; ?>"> 
<input type="hidden" name="photolocation1" id="photolocation" class="form-control"  value="<?php echo $_POST['photolocation1'];?>">
<?php
}
 ?> 


							<input type="hidden" name="memberType" id="memberType" class="form-control" value="<?php echo $_POST['memberType']; ?>">
                             <input type="hidden" name="photolocation" id="photolocation" class="form-control"  value="<?php echo $_POST['photolocation']; ?>">
                                <br/>
                                <label  style="font-family: Helvetica">ID: <br/><span id="error_message"> </span></label>
							<input style="font-family: Helvetica" type="text" name="idnum" id="idNum" class="form-control" required maxlength="13" onblur="validateRSAidnumber()" oninput="return checkForDuplicateId(this);">
							<div id="debug" style="color: red; display: none;font-family: Helvetica;"></div><br>
                                <div id="idmsg" style="color: red; display: none;font-family: Helvetica;"></div>
							<label style="font-family: Helvetica">NAME:</label>
							<input type="text" name="fullName" id="phone" class="form-control" required style="font-family: Helvetica;">
							
<label style="font-family: Helvetica">INITIALS:</label>
							<input type="text" name="ini" value="<?php if(isset($_GET['ini'])){ echo $_GET['ini']; } ?>" id="phone" class="form-control" required style="font-family: Helvetica;">
														

<label style="font-family: Helvetica">SURNAME:</label>
							<input type="text" name="surname" id="surname" class="form-control" required style="font-family: Helvetica;">
							<label style="font-family: Helvetica" style="font-family: Helvetica">CELL NUMBER:</label>
							<input type="number" name="cell" id="cellphone" class="form-control" oninput="return phonenumber(this)" required placeholder="eg. 0821234567" maxlength="10" style="font-family: Helvetica;">
							<label style="font-family: Helvetica">EMAIL:</label>
							<input type="text" name="email" id="email" class="form-control" oninput="return emailValidate(this);" required style="font-family: Helvetica;">
							<input type="hidden" id="idvalueCheck" value="0" > 
							<label style="font-family: Helvetica">GENDER:</label>
                             <input type="text" id="gender" name="gender" class="form-control" placeholder="This will be auto populated" readonly style="font-family: Helvetica">
                            <?php
                            if(!isset($_POST['otherAcccount']))
                            {
if(($_POST['memberType'] === "Taxi Owner") || ($_POST['memberType'] === "Taxi Driver"))
{
                                ?>
   <label style="font-family: Helvetica">Driver License Number:</label>
							<input style="font-family: Helvetica" type="text" name="licenseNo" id="licenseNumber" class="form-control" oninput="return licenseTest(this);" required>
							<label style="font-family: Helvetica">Driver License Number Expiry Date:</label>
							<input type="text" name="licenseExpiryDate" id="expirydate" class="form-control" required  placeholder="yyyy-mm-dd" onchange="checkLicenseDate()" onkeydown="return false;" style="font-family: Helvetica;">
							<input type="hidden" name="pnrLicense" id="pnrLicense" value="n/a" class="form-control" required>
							<label style="font-family: Helvetica">PNR Number (PDP) Expiry Date:</label>
							<input type="text" name="pnrLicenseExpiryDate" id="pnpexpiry" class="form-control" placeholder="yyyy-mm-dd" required  onchange="pdpLicenseDate()" onkeydown="return false;" style="font-family: Helvetica;">
	


             
      					
<?php
}
else
{
?>
                       


            <input type="hidden" id="assoc" name="assoc" value="what" >
<input type="hidden" name="province" value="nothing" />
<input type="hidden" name="pnrLicenseExpiryDate" id="pnpexpiry" value="2020-04-12">
<input type="hidden" name="licenseExpiryDate" id="expirydate"  class="form-control"  value="2020-04-12" required >
<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required>
<input style="font-family: Helvetica" type="hidden" name="licenseNo" id="licenseNumber" class="form-control" value="22hhh2222" required> 
	



<?php

}
?>							<label style="font-family: Helvetica">NAME OF TAXI ASSOCIATION:</label>
                            <label style="font-family: Helvetica">PROVINCE:</label>
                            <select id="province" name="province" class="form-control"  style="font-family: Helvetica" onchange="return getcities()" required>

    <option value="">Select province

    </option>

<?php



$query = $conn->query("select DISTINCT province from cities order by province asc");

while($result = $query->fetch(PDO::FETCH_ASSOC))

{ 

?>

<option value="<?php echo $result['province']; ?>"> 

<?php echo $result['province']; ?>

</option>

<?php



}

?>

    

</select>
                            
                            <br/>
							<label style="font-family: Helvetica">TOWN:</label>
                                <div id="cities">
                            <input  style="font-family: Helvetica" type="text" name="phone" id="phone" class="form-control" value="Choose Your Province First" disabled required>
</div>

 
<br/>
<label style="font-family: Helvetica">Taxi Association:</label>
<div id="test"></div>
<div id="assoclist">
<select id="getTownId" name="assoc" class="form-control"  style="font-family: Helvetica"  required>
<option value="" disabled selected>Select Your Taxi Association</option> 
   <!-- --><?php
/*        $query = $conn->query("Select
        taxi_assocs.name
    From
        taxi_assocs
     Where 
        taxi_assocs.type = 'Chairperson'  
    And  
        taxi_assocs.status = '1'  ");

        while($result = $query->fetch(PDO::FETCH_ASSOC))
    {
        echo'<option value="'.$result['name'].'">'.$result['name'].'</option>';
    }

    */?>
</select>

</div>
<p style='font-family: Helvetica; font-size: 14px;text-align: left;'><b>Please contact us via WhatsApp if you can&#39;t find your association</b></p>
    <script>
        function getTown(val) {
var xhr = new XMLHttpRequest();  
var data = new FormData(); // instanciate Form data
data.append('city', val);   
xhr.open('POST', 'validateAssoc.php', true);
xhr.onprogress = function () { $("#test").addClass("loader"); }; 
xhr.onload = function () {
   $("#city-list").html(this.responseText);
   $("#city-list").removeClass("loader");
   document.getElementById("assoclist").innerHTML = this.responseText;
if(document.getElementById("getTownId") === null)
{
document.getElementById("sender").style.display='none';
}
else
{
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
}






}; 
xhr.send(data);








/*
            $.ajax({
                type: "POST",
                url: "validateAssoc.php",
                data:'city='+val,
                beforeSend: function() {
                    $("#test").addClass("loader");
                },
                success: function(data){
alert(this.responseText);
                    $("#city-list").html(data);
                    $("#city-list").removeClass("loader");
                    document.getElementById("test").innerHTML = this.responseText;
                }
            });
*/
        }


    </script>


<div id="gaz" onmouseover="return searchKeep()" onblur="return outSearch();" onmouseout="return outSearch();"  style="font-family: Helvetica;height:200px; overflow: auto;position:absolute; z-index: 10000;display:none;"> 
</div>

    <br/>
                            
                            
                            <?php
                            }
                            else{
                                ?>

<input type="hidden" id="otherAcccount" name="otherAcccount" value="otherAcccount" >
<input type="hidden" id="assoc" name="assoc" value="what" >
<input type="hidden" name="province" value="nothing" />
<input type="hidden" name="pnrLicenseExpiryDate" id="pnpexpiry" value="2020-04-12">
<input type="hidden" name="licenseExpiryDate" id="expirydate"  class="form-control"  value="2020-04-12" required >
<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required>
<input style="font-family: Helvetica" type="hidden" name="licenseNo" id="licenseNumber" class="form-control" value="22hhh2222" required>
<input type="hidden" name="otherBankInfo" value="nothing" />
<?php
                            }
                           
function cvf_ps_generate_random_code($length=10) {

  $string = '';
  // You can define your own characters here.
  $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

  for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters)-1)];
  }

  return $string;

}
$regIdNow = cvf_ps_generate_random_code(8);
?>
							
							<input type="hidden" name="regIdNow" value="<?php echo $regIdNow; ?>"
                            <br/>
                            <div class="text-center">


                                <!--<input type="button" id="validateAssoc" class="button" style="border-radius: 17px;font-family: Helvetica;text-align:center;height:40px;color:#ffffff;" value="Validate Taxi Association" onclick="validateAssoc()">
                                <span id="validateAssoc-status"></span>-->


                                    <input type="submit" id="sender" class="button" style="border-radius: 20px;font-family: Helvetica;text-align:center;height:60px;color:#ffffff;" value="Proceed" onclick="this.disabled=true;this.value='Sending...';mysubmiForm();">
                          <input type="button" id="sender2" class="button" style="border-radius: 20px;background-color:black;font-family: Helvetica;text-align:center;height:60px;color:#ffffff;display:none;" value="Proceed" onclick="return exp()">          
      <!-- <a href="expired.php" type="button" id="sender2" class="button" style="border-radius: 20px;font-family: Helvetica;text-align:center;height:60px;color:#ffffff; display:none" value="Proceed" onclick="clearDate()">Proceed</a> -->
                                </div>
                            </form>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 33%;">
                                        <a href="http://www.taxitimes.net"><img src="logos-02.jpg" width="95" height="95" /></a>
                                    </td>
                                    <td style="width: 33%;text-align:center">
                                        <a href="https://api.whatsapp.com/send?phone=27660714008&text=Hi"> <img src="Whatsapp-512.png" width="60" height="60" ></a>
                                    </td>
                                    <td style="width: 33%;text-align:left;">
                                        <a href="http://www.santaco.co.za"><img src="logos-01.jpg" width="98" height="94" /></a>
                                    </td>
                                </tr>
                            </table>
                           <table style="width: 100%;">
                               <tr>

                                    <td style="width: 100%;background-color: #000000;text-align: center;padding-top:2px;padding-bottom:2px;">

                                        <a href="https://sacoronavirus.co.za"   style="font-family: Helvetica;font-family: Helvetica;color: #ffffff; font-size:11px;text-align: center;">For more information about the coronavirus click here

</a>

                                    </td>

                                </tr>
<tr>
                                    <td style="width: 100%;text-align: center;padding-top:2px;padding-bottom:2px;">
                                        <a href="downloadPdf.php"  style="font-family: Helvetica;color: #000000; font-size:11px;text-align: center;">Click here for Terms and Conditions
</a>
                                    </td>
                                </tr>
                            </table> 
                            <script>

                                function clearDate(){
                                    document.getElementById("expirydate").valueAsDate = null;
                                }
                            </script>

                            <script>
                        //prevent form from being submited by pressing enter
                        $('#formid').on('keyup keypress', function(e) {
                            var keyCode = e.keyCode || e.which;
                            if (keyCode === 13) {
                                e.preventDefault();
                                return false;
                            }
                        });

                    </script>


                            <script>
                                //THIS SCRIPT SECTION WILL BE MOVED UP INTO HEAD ONCE RETURN KEY FUNCTIONING IS WORKING
                                function testEnter(){
                                    console.log("Return Button pressed");
                                }

                                document.getElementById('loginPage').onkeydown = function(e){
                                    if(e.keyCode === 13){
                                        Auhtenticater_func();
                                        e.preventDefault();
                                    }
                                };

                            </script>

<!--
                            <script>
                                // check if the entered taxi association is an approved one
                                    function validateAssoc() {
                                        var btn = document.getElementById(validateAssoc);
                                        var selectedAssoc = document.getElementById(assoc).value;

                                        jQuery.ajax({
                                            url: "validateAssoc.php",
                                            data: 'assoc='+$("#assoc").val(),
                                            type: "POST",
                                            success:function(data){
                                                $("#validateAssoc-status").html(data);
                                                $("#loaderIcon").hide();
                                            },
                                            error:function (){}
                                        });
                                    }

                            </script>-->

                            <br/>
                        </div>
                    </div>
                </div>
                <!-- End Center Collumn where all information will be sitting -->

                <!-- Start Collumn for right margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for right margin-->
            </div>
            <!-- End Row Main with 3 sections division (left margin/ middle for data / right margin)-->
        </div>
        <!-- Container Ends here-->
<script>
document.getElementById("sender").style.display='none';
/*function Auhtenticater_func()
{
    var idLength = document.getElementById("idNum").value.length;
    //alert(idLength);
    if(document.getElementById("idNum").value.length === 13)
    {
    document.querySelector(".registerForm").submit();
	//location.href="otp.html";
	//alert(idLength);
    }
    else
    {
       document.getElementById("error_message").innerHTML = "<br/><span style='color: red;font-family: Helvetica'>YOUR ID NUMBER IS INCORRECT !</span>";
    }

}*/

 /*function validateID() {
                var ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
                  var theIDnumber = document.getElementById("idNum").value;
                  var newMyval = theIDnumber.substring(0,13);
                  if (ex.test(theIDnumber) == false) {

		document.getElementById("idNum").value = newMyval;
                    // alert code goes here
                   // document.getElementById("error_message").innerHTML = "<br/><span style='color:red;font-family: eras-demi-itc;'>Please Enter a valid ID Number</span>";
                    //alert('Please supply a valid ID number');
                    document.getElementById("tooltiptextID").style.display = "block";
                    return false;
                  }
                  //alert(theIDnumber + ' is a valid ID number');
                  document.getElementById("idNum").value = newMyval;
                  // here you would normally obviously
                  // return true;
                  document.getElementById("tooltiptextID").style.display = "none";
                  return true;
            }

            */
function emailValidate(sel) {
            var email = sel.value;
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                document.getElementById("sender").disabled = false;
                document.getElementById("email").style.borderColor = "#282E54";
                document.getElementById("tooltiptextEmail").style.display = "none";

            }
            else{
                document.getElementById("sender").disabled = true;
                document.getElementById("email").style.borderColor = "red";
                document.getElementById("tooltiptextEmail").style.display = "block";
            }
        }



function validateRSAidnumber() {

            $('#debug').empty();
            if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}


            var idnumber = $('#idNum').val(),
                invalid = 0;

            // get the gender from the id number
            var genderCode = idnumber.substring(6, 10);
            var gender = parseInt(genderCode) < 5000 ? "Female" : "Male";
            var inputF = document.getElementById("gender"); // gender input field id

            // display debugging
            var debug = $('#debug');

            // check that value submitted is a number
            if (isNaN(idnumber)) {

                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            }

            // check length of 13 digits
            if (idnumber.length != 13) {
                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            }

            // check that YYMMDD group is a valid date
            var yy = idnumber.substring(0, 2),
                mm = idnumber.substring(2, 4),
                dd = idnumber.substring(4, 6);

            var dob = new Date(yy, (mm - 1), dd);

            // check values - add one to month because Date() uses 0-11 for months
            if (!(((dob.getFullYear() + '').substring(2, 4) == yy) && (dob.getMonth() == mm - 1) && (dob.getDate() == dd))) {
                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            }

            // evaluate GSSS group for gender and sequence
          //  var gender = parseInt(idnumber.substring(6, 10), 10) > 5000 ? "M" : "F";

            // ensure third to last digit is a 1 or a 0
            if (idnumber.substring(10, 11) > 1) {
                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            } else {
                // determine citizenship from third to last digit (C)
                var saffer = parseInt(idnumber.substring(10, 11), 10) === 0 ? "C" : "F";
            }

            // ensure second to last digit is a 8 or a 9
            if (idnumber.substring(11, 12) < 8) {
                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            }

            // calculate check bit (Z) using the Luhn algorithm
            var ncheck = 0,
                beven = false;

            for (var c = idnumber.length - 1; c >= 0; c--) {
                var cdigit = idnumber.charAt(c),
                    ndigit = parseInt(cdigit, 10);

                if (beven) {
                    if ((ndigit *= 2) > 9) ndigit -= 9;
                }

                ncheck += ndigit;
                beven = !beven;
            }

            if ((ncheck % 10) !== 0) {

                invalid++;
                document.getElementById("sender").style.display='none'; // hide submit button
                document.getElementById("gender").style.display='none'; // hide gender input field
            }

            // if one or more checks fail, display details
            if (invalid > 0) {
                debug.css('display', 'block');
                debug.append('Your ID Number is Invalid <br />');

            }


            // set value for the input field
                document.getElementById("gender").style.display='block'; // hide gender input field
                inputF.value = gender;
                innerHTML = "Value = " + "'" + inputF.value + "'";

if(document.getElementById("idvalueCheck").value == '1')
{
 document.getElementById("sender").style.display='none';
}

    return (ncheck % 10) === 0;
        }

        $('#details').submit(validateRSAidnumber);




function licenseTest(inputtxt) {
    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
        var max_chars = 12;
        var mylicence = inputtxt.value;
var newMyval = mylicence.substring(0,12);

        if(inputtxt.value.match(number)) {
            //document.getElementById("licenseNumber").style.borderColor = "#282E54";
            document.getElementById("tooltiptextLicense").style.display = "block";
            var newMyval = mylicence.substring(0,12);
		document.getElementById("licenseNumber").value = newMyval;
		if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
            if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        }
        else {
           // document.getElementById("licenseNumbere").style.borderColor = "red";
            document.getElementById("tooltiptextLicense").style.display = "none";
            var newMyval = mylicence.substring(0,12);
		document.getElementById("licenseNumber").value = newMyval;
		if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
            if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        }
        if(mylicence.length === 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
            if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
            if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        if(mylicence.length < 12)
        {
            document.getElementById("tooltiptextLicense").style.display = "block";
            document.getElementById("sender").style.display='none';
        }
}


function cellPhone(inputtxt) {
    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
        var max_chars = 10;
        var mylicence = inputtxt.value;


        if(inputtxt.value.match(number)) {
            //document.getElementById("licenseNumber").style.borderColor = "#282E54";
            //document.getElementById("tooltiptextLicense").style.display = "block";
            var newMyval = mylicence.substring(0,10);
		document.getElementById("cellphone").value = newMyval;
		if(mylicence.length > 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none";


        }

        }
        else {
           // document.getElementById("licenseNumbere").style.borderColor = "red";
           // document.getElementById("tooltiptextLicense").style.display = "none";
            var newMyval = mylicence.substring(0,10);
		document.getElementById("cellphone").value = newMyval;

		if(mylicence.length > 10)
        {
          // document.getElementById("tooltiptextLicense").style.display = "none";
        }
        }
        if(mylicence.length === 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none"; se
        }
        if(mylicence.length > 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none";
        }
}


 function phonenumber(inputtxt) {
            var phoneno = /^\d{10}$/;
            var max_chars = 10;

            if(inputtxt.value.match(phoneno)) {
                //document.getElementById("formChecker").value = 1;
                document.getElementById("cellphone").style.borderColor = "#282E54";
                document.getElementById("tooltiptextPhone").style.display = "none";
                document.getElementById("sender").disabled = false;
            }
            else {
                //document.getElementById("formChecker").value = 0;
                document.getElementById("cellphone").style.borderColor = "red";
                document.getElementById("tooltiptextPhone").style.display = "block";
                document.getElementById("sender").disabled = true;
            }
        }


        /*Check if the lisence expiery date*/
    function checkLicenseDate() {
        let d = Date.parse("2020-03-01"); // set min date to march and convert to milliseconds
        let selectedDate = document.getElementById('expirydate').value;
        let converted = Date.parse(selectedDate); // convert selected date to milliseconds
        if(converted < d){
            // notify the user that they dont qualify
            //document.getElementById("sender").disabled = true; // disable submit button
            document.getElementById("sender").style.display='none'; // hide submit button
            document.getElementById("sender2").style.display='block'; // Show link to error page
            localStorage.setItem("expiredLicense", "yes"); 
        }else{
			document.getElementById("sender").style.display='block'; // hide submit button
            document.getElementById("sender2").style.display='none'; // Show link to error page
			localStorage.removeItem("expiredLicense");
		}

    }






/*Check if the PDP expiery date*/
    function pdpLicenseDate() {
        let d = Date.parse("2020-03-01"); // set min date to march and convert to milliseconds
        let selectedDate = document.getElementById('pnpexpiry').value;
        let converted = Date.parse(selectedDate); // convert selected date to milliseconds
        if(converted < d){
            // notify the user that they dont qualify
            //document.getElementById("sender").disabled = true; // disable submit button
            document.getElementById("sender").style.display='none'; // hide submit button
            document.getElementById("sender2").style.display='block'; // Show link to error page
            localStorage.setItem("expiredPDP", "yes");
		}else{
			document.getElementById("sender").style.display='block'; // hide submit button
            document.getElementById("sender2").style.display='none'; // Show link to error page
			localStorage.removeItem("expiredPDP");
		}

    }


function mysubmiForm()
{
    var erroMessage = "";
    var correct = 0;
    var phoneno = /^\d{10}$/;
    var max_chars = 10;
    var inputtxt = document.getElementById("cellphone").value;
/*
    var empt = document.forms["registerForm"]["assoc"].value;
    if (empt == ""){
        alert('test');
        element.addEventListener("submit", function(evt) {
            evt.preventDefault();
            window.history.back();
        }, true);
    }
*/


if(document.getElementById("cellphone").value.length === 10) {
        correct++;

    }
    else
    {
        erroMessage += "Cell phone number is incorrect!</br/>";
    }

    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
    var max_chars = 12;
    //var mylicence = inputtxt.value;
    var inputtxt = document.getElementById("licenseNumber").value;

    if(inputtxt.length === 12)
    {
        correct++;
    }
    else
    {
       erroMessage += "License number is incorrect!</br/>";
    }


	var pnpexpiry = document.getElementById("pnpexpiry").value;		
	if(pnpexpiry.length !== 0)
    {
        correct++;
    }
    else
    {
       erroMessage += "Pdp number is incorrect!</br/>";
    }
var expirydate = document.getElementById("expirydate").value;
	if(expirydate.length !== 0)
    {
        correct++;
    }
    else
    {
       erroMessage += "License number date is incorrect!</br/>";
    }

if(document.getElementById("getTownId") === null)
{

correct--;
erroMessage += "Taxi Association not Selected Please choose one.</br/>";
}
else
{
  if(document.getElementById("getTownId").value == "")
{
correct--;
erroMessage += "Taxi Association not Selected Please choose one.</br/>";
}
}




    var email = document.getElementById("email").value;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
             correct++;
            }
            else{
                erroMessage += "Email is incorrect!</br/>";

            }
   //alert(document.getElementById("getTownId").value);
    if(correct === 5)
    {
      document.getElementById("registerForm").submit();
    }
    else
    {
        document.getElementById("error_message").innerHTML = erroMessage;
    }
    document.getElementById("sender").disabled=false;
    document.getElementById("sender").value="Proceed";

}
 


function getmysearch(mysearching)

{

    var mysearching = mysearching.replace("z1z", "'");

    document.getElementById("tags").value = mysearching;

    document.getElementById("gaz").style.display = "none"; 

}

function getcities() {
document.getElementById("assoclist").innerHTML = "";
document.getElementById("sender").style.display='none';
            var select1 = document.getElementById("province");

            var myoptions = select1.options[select1.selectedIndex].value;

var xhr = new XMLHttpRequest();  

            var datas = new FormData(); // instanciate Form data

            datas.append('province', myoptions);   

           xhr.open('POST', 'getcity.php', true);

            xhr.onprogress = function () { document.getElementById("cities").innerHTML = '<input  style="font-family: Helvetica" type="text" name="phone" id="phone" class="form-control" value="Loading cities...." disabled required>'; }; 

            xhr.onload = function () { 

                document.getElementById("cities").innerHTML = this.responseText;

                //alert(this.responseText); 

                var regCity = document.getElementById("city").value;

                var xhr1 = new XMLHttpRequest();  

            var datas1 = new FormData(); // instanciate Form data

            datas1.append('city', regCity);

            xhr1.open('POST', 'newsearch.php', true);

            xhr1.onprogress = function () {}; 

            xhr1.onload = function () { 

                

                document.getElementById("autodiv").innerHTML = this.responseText;

                }; 

                xhr1.send(datas1);

            }; 

            xhr.send(datas);

}

function cityChanged(ips)

{ 
getTown(ips.value);
     var province = document.getElementById("province").value;

     var cities = document.getElementById("city").value;

     //alert(ips.value);


     var xhr = new XMLHttpRequest();  

            var datas = new FormData(); // instanciate Form data

            datas.append('city', ips.value);

            datas.append('town', cities); 

            datas.append('province', province); 

           xhr.open('POST', 'msearch2.php', true);

            xhr.onprogress = function () {}; 

            xhr.onload = function () {

                

                document.getElementById("gaz").innerHTML = this.responseText;
if(document.getElementById("getTownId").value === undefined)
{

document.getElementById("sender").style.display='none';
}
else
{
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
}

            }; 

            xhr.send(datas);

}

function mycheck(ips)

{ 

     var province = document.getElementById("province").value;

     var cities = document.getElementById("city").value;

   
 
     var xhr = new XMLHttpRequest();  

            var datas = new FormData(); // instanciate Form data

            datas.append('city', ips.value);

            datas.append('town', cities); 

            datas.append('province', province); 

           xhr.open('POST', 'msearch2.php', true);
 
            xhr.onprogress = function () {}; 

            xhr.onload = function () {
var myresponseIs = this.responseText.trim();
//alert(myresponseIs);
var tb = '<table style="width: 100%;background-color: #ffffff;"></table>';

//alert(myresponseIs);
if((myresponseIs === "too small") || (myresponseIs === tb) )
{
//alert();
document.getElementById("gaz").style.display = "none"; 
}
else
{
document.getElementById("gaz").style.display = "block"; 
}
                

                document.getElementById("gaz").innerHTML = this.responseText;

            }; 

            xhr.send(datas);

}

function outSearch()

{

   document.getElementById("gaz").style.display = "none";  

}

function searchKeep()

{

   document.getElementById("gaz").style.display = "block";  

}
function checkForDuplicateId(sel)
{
	var xhr = new XMLHttpRequest(); 
    var data = new FormData();
    var idNum = sel.value;	
	data.append('idNum', idNum);
	xhr.open('POST', 'idcheck.php', true);
    xhr.onprogress = function () {
    };
    xhr.onload = function () {
	//alert(this.responseText.length);
		if(this.responseText.length === 9)
		{
		
		  document.getElementById("idvalueCheck").value = 1;
		  document.getElementById("error_message").innerHTML = "<font color='red'>This id number has already been used to register a claimant. Please contact us via WhatsApp to rectify.</font>";
		  /*
		  document.getElementById("surname").readOnly = true;
		  document.getElementById("fullName").readOnly = true;
		  document.getElementById("cellphone").readOnly = true;
		  document.getElementById("email").readOnly = true;
		  */
		}
		else
		{
		  document.getElementById("idvalueCheck").value = 0;
		  document.getElementById("error_message").innerHTML = "";
		  
		  /*
		  document.getElementById("surname").readOnly = false;
		  document.getElementById("fullName").readOnly = false;
		  document.getElementById("cellphone").readOnly = false;
		  document.getElementById("email").readOnly = false;
		  */
		}
       	
	   
    };
        xhr.send(data);
} 
function exp()
{ 
location.href = "expired.php";
}
</script>



<div id="autodiv">

  

</div>
    </body>
</html>
