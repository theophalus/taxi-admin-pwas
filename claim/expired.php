<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>Relief Fund</title>
<link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
<link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">

<link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">





        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="signin.css" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="welcome.css" /> 
        <link rel="manifest" href="manifest.json">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!--<script src="login.js"></script>-->
        <script>
            /*function myFunction() {
                var w = window.outerWidth;
                var h = window.outerHeight;
                var txt = "Window size: width=" + w + ", height=" + h;
                document.getElementById("demo").innerHTML = txt;
            }*/

            //Login.js file
            $(document).ready(function()
{
	



if('serviceWorker' in navigator) 
	{ 
		//Register the Service Worker
		navigator.serviceWorker.register('sw2.js').then(function(registration) 
		{
			registration.update();
			registration.installing; // the installing worker, or undefined
			registration.waiting; // the waiting worker, or undefined
			registration.active; // the active worker, or undefine
			console.log('Service Worker Registered2');
		});
	} else console.log('Your browser does not support the Service-Worker!');
		if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
		{
			window.location.href = "profile.html"; // 	if Session created redirect to Profile page
		}
}); 

/*

	Authentification code function 

*/

        </script>
    </head>

    <body id="loginPage" style="font-family: Helvetica;"> 
        <div class="container">
            <!-- Row start to create 10% margin spacing here-->
            <div class="row">
                <div class="col-md-12" style="margin-top:0%;">&nbsp;</div>
            </div>
            <!-- Row Ends to create 10% margin spacing here-->
<br/><br/><br/>
            <!-- Row start to divide into 3 sections left margin-->
            <div class="row">
                <!-- Start Collumn for left margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for left margin-->

                <!-- Start Center Collumn where all information will be sitting -->
                <div class="col-md-4">
                    <div class="mainBlock">
                        <div class="logoDiv">
                            <img src="relief.JPG" width="250" />  
                        </div> 
                        
                        <div class="mainBlockSection">
                            <p id="error_message" align="center"> </p>
                            <form class="form-signin otpForm" action="otp.php" method="post" style="margin-top:0px;padding-top:0px;">
                                <h4 align="center" style="font-family: Helvetica;" id="exp">Your Drivers Licence has expired.
                                </h4>
                                <p style="font-family: Helvetica;">We regret to inform you that you do not qualify for the Taxi Industry Coronavirus Relief Fund due to your drivers license that has expired.</p>
                               <br/>
                                <input type="button" id="sender" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="Home" onclick="return Auhtenticater_func()">
                                <input type="button" id="sender" class="button" style="border-radius: 20px;font-family: Helvetica;margin:10px;text-align:center;height:40px;color:#ffffff;" value="TICRF Website" onclick="return Auhtenticater_func2()">

                            </form><br/><br/>
                             
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 33%;">
                                        <a href="http://www.taxitimes.net"><img src="taxi.JPG" width="60" height="60" /></a>
                                    </td>
                                    <td style="width: 33%;text-align:center">
                                        <a href="whatsapp://send?phone=27660714008&text=Hi"> <img src="Whatsapp-512.png" width="60" height="60" ></a>
                                    </td>
                                    <td style="width: 33%;text-align:right;">
                                        <a href="http://www.santaco.co.za"><img src="hl.JPG" width="90" height="60" /></a>
                                    </td>
                                </tr>
                            </table>
                           <br/>
                           <table style="width: 100%;">
                                <tr>
                                    <td style="width: 100%;background-color: #000000;text-align: center;padding-top:2px;padding-bottom:2px;">
                                        <a href="https://sacoronavirus.co.za" style="color: #ffffff; font-size:11px;text-align: center;">For more information about the coronavirus click here
</a>
                                    </td>
                                </tr>
                            </table>
                            <script>
                                //THIS SCRIPT SECTION WILL BE MOVED UP INTO HEAD ONCE RETURN KEY FUNCTIONING IS WORKING
                                function testEnter(){
                                    console.log("Return Button pressed");
                                }
                                
                                document.getElementById('loginPage').onkeydown = function(e){
                                    if(e.keyCode === 13){
                                        Auhtenticater_func();
                                        e.preventDefault();
                                    }
                                };

                            </script>
                            
                            <br/>
                        </div>
                    </div>
                </div>
                <!-- End Center Collumn where all information will be sitting -->

                <!-- Start Collumn for right margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for right margin-->
            </div>
            <!-- End Row Main with 3 sections division (left margin/ middle for data / right margin)-->
        </div>
        <script>
            function Auhtenticater_func()
            {
                location.href="index.html";
            }

        </script>
        <script>
            function Auhtenticater_func2()
            {
                location.href="http://www.ticrf.co.za";
            }

var exChecker = 0;

if(localStorage.getItem("expiredLicense"))
{
exChecker += 3; 
localStorage.removeItem("expiredLicense");
}
if(localStorage.getItem("expiredPDP"))
{

exChecker += 2;
localStorage.removeItem("expiredPDP");
}
if(exChecker == 2)
{
//alert(document.getElementById("exp").innerHTML);
document.getElementById("exp").innerHTML = "Your PDP Licence has expired.";
}
if(exChecker == 3)
{
document.getElementById("exp").innerHTML = "Your Drivers Licence has expired.";
}
if(exChecker == 5)
{
document.getElementById("exp").innerHTML = "Your Drivers and PDP Licence has expired.";
}
localStorage.setItem("LandedOnExpiry", "yes");
        </script>
        <!-- Container Ends here-->
    </body>
</html>