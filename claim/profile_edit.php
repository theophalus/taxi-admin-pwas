<!doctype html>
<html lang="en">
<head>

    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>NuDebt eColls</title>
    <link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
    <link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">
    
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="signin.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
    <!--Log Users out after 5 minutes of sitting idle-->
    <script src="loginIdle.js"></script>

    <script>
        userTimedOut();
        $(document).ready(function(){

        /*
        window.addEventListener('blur', function(){
        window.location.href = "logout.html";
        }, false);

        
        //document.getElementById("profile_edit").disabled = true;
        let val = localStorage.getItem("Mysession");

        var obj = JSON.parse(val);

        document.getElementById("myname").innerHTML = obj.names;

        document.getElementById("mysurname").innerHTML = obj.surname;

        document.getElementById("email").value = obj.email;

        document.getElementById("phone").value = obj.phone;

        document.getElementById("idno").value = obj.id_num;
        document.getElementById("PreviousContactNo").value = obj.phone;
        document.getElementById("PreviousEmailAddress").value = obj.email;

        var phone =$("#phone").val();

        var email =$("#email").val();

        //alert(phone)

        //localStorage.removeItem("Mysession");   

        var error_message = document.getElementById("error_message");
*/
        });

        function mysender() {
            document.getElementById("error_message").innerHTML = "<img src='loading.gif' width='70' />";
            var xhr = new XMLHttpRequest(); 
            var data = new FormData();
            var ids = $("#idno").val(); 
            var phone = $("#phone").val();
            var email = $("#email").val();
            var PreviousContactNo = document.getElementById("PreviousContactNo").value;
            var PreviousEmailAddress = document.getElementById("PreviousEmailAddress").value;
            data.append('id', ids);
            data.append('phone', phone);
            data.append('email', email);
            data.append('PreviousContactNo', PreviousContactNo);
            data.append('PreviousEmailAddress', PreviousEmailAddress);

            xhr.open('POST', 'update_profile.php', true);
            xhr.onprogress = function () {
                document.getElementById("error_message").innerHTML = "AUTHORIZING...";
            };
            xhr.onload = function () {
                //alert(this.responseText);
                localStorage.removeItem("Mysession"); 
                document.getElementById("error_message").innerHTML = "";
                let obj = JSON.parse(this.responseText);
                let authorize_checker = obj.checker;
                let authorize_checker_size = authorize_checker.length;
                if(authorize_checker_size == 5) {
                    localStorage.setItem("Mysession", this.responseText); 
                    localStorage.setItem("gaz", this.responseText);
                    window.location.href = "profile.html";
                }
                else {
                    document.getElementById("error_message").innerHTML = "<span style='color:red'>authentication failed</span>";
                }
                //document.getElementById("error_message").innerHTML = this.responseText;
                //localStorage.setItem("Mysession", this.responseText);
                //let val = localStorage.getItem("Mysession");
                //console.log(val);
                //localStorage.removeItem("Mysession"); 
                //let val = localStorage.getItem("Mysession");
                //var obj = JSON.parse(val);
                //console.log(obj[0].ID);
            };
            xhr.send(data);
        }

        function phonenumber(inputtxt) {
            var phoneno = /^\d{10}$/;
            var max_chars = 10;
            
            if(inputtxt.value.match(phoneno)) {
                document.getElementById("profile_edit").disabled = false;
                document.getElementById("phone").style.borderColor = "#282E54"; 
                document.getElementById("tooltiptextPhone").style.display = "none";
            }
            else {
                document.getElementById("profile_edit").disabled = true;
                document.getElementById("phone").style.borderColor = "red";
                document.getElementById("tooltiptextPhone").style.display = "block";
            }
        }

        function emailValidate(sel) {   
            var email = sel.value;
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) { 
                document.getElementById("profile_edit").disabled = false;
                document.getElementById("email").style.borderColor = "#282E54"; 
                document.getElementById("tooltiptextEmail").style.display = "none";
            }
            else{
                document.getElementById("profile_edit").disabled = true;
                document.getElementById("email").style.borderColor = "red";
                document.getElementById("tooltiptextEmail").style.display = "block";
            }
        }
    </script>

    <style>
    @font-face {  
    font-family: eras-demi-itc;
    src:  url('erademi.ttf'); /* create eras demi font */
    }
    .button, input[type=button] 

    {

    background-color: #282E54; /* blue */

    border: 4px solid #282E54;

    color: white;

    font-weight: bold;
    padding: 3%;

    border: 3px solid #3851A3;

    text-align: center;

    text-decoration: none;

    display: inline-block;

    font-size: 16px;

    cursor: pointer;


    height: 50px;

    }
    #whatsappBtn
    {
    text-align: center;
    margin-left:45%;
    }

    .sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #fff;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    }

    .sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #3851A3;
    display: block;
    transition: 0.3s;
    }

    .sidenav a:hover {
    color: #f1f1f1;
    }

    .sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
    }


    #tooltiptextPhone {
    display: none;
    width: 250px; 
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:310px;
    left:68px; 
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextEmail {
    display: none;
    width: 250px; 
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:390px;
    left:68px; 
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }



    @media only screen and (max-width: 768px) {

    #tooltiptextPhone {
    display: none;
    width: 250px; 
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:310px;
    left:120px; 
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextEmail {
    display: none;
    width: 250px; 
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:390px;
    left:120px; 
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    }
    </style>
</head>

<body>
    
    <div class="container">
		<br/><br/><br/><br/>
		<div class="row">
			<div class="col-md-4">&nbsp;</div>
			<div class="col-md-4">
				<div style="padding:3%;font-family: eras-demi-itc;"> 
                	<!-- <div class="top-nav"><a href="logout.html"><img src="nav.png" /></a></div> -->

									<div style="text-align: center;">
										<table style="width:100%;margin-top: 0px;padding-top: 0px;">
										<tr>
												<td>
														<div id="mySidenav" class="sidenav">
														<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
																<a href="profile.html" style="font-size:18px;"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;&nbsp;Home</a>
																<a href="profile.html" style="font-size:18px;"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;&nbsp;My Profile</a>
																<a href="outstanding_matter.php" style="font-size:18px;"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span>&nbsp;&nbsp;My Matters</a>
																<a href="terms.html" style="font-size:18px;"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>&nbsp;&nbsp;Terms and Conditions</a>
																<a href="logout.html" style="font-size:18px;"><span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;&nbsp;Logout</a>
															  </div>
															  
															  <span style="font-size:25px;cursor:pointer;color: #ffffff;;font-family: eras-demi-itc;" onclick="openNav()">&#9776;</span>
															  
															  <script>
															  function openNav() {
																document.getElementById("mySidenav").style.width = "250px";
															  }
															  
															  function closeNav() {
																document.getElementById("mySidenav").style.width = "0";
															  }
															  </script>
												</td>

												<td style="text-align:center;"> 
													<img src="Logo_new.png" width="170" />&nbsp;&nbsp;
												</td>
												<td style="width:20%;text-align:left;">
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="profile.html"><img src="nav.png" width="40" /></a> 
													
												</td> 
											</tr>
										</table>
									</div> 
									<div id="tooltiptextPhone">Only numerics allowed eg. 0821234567</div>
									<div id="tooltiptextEmail">Needs to be a valid email eg. john@doe.com</div>   
					<div  style="color:#ffffff;padding-top:3%;padding-left:3%;padding-right:3%;"> 
						hrllll
						<form class="form-signin profile">

							<input type="hidden" id="PreviousEmailAddress" style="color:#000;" />
							<input type="hidden" id="PreviousContactNo" style="color:#000;" /> 
							<input type="hidden" id="validate_me1" style="color:#000;" value="1" />
							<input type="hidden" id="validate_me2" style="color:#000;" value="1" /> 
                            <p>Welcome <br/> <span id="myname"></span> <span id="mysurname"></span></p>
														<br/>
                            <p id="error_message"> </p>
                            
                            <label>I.D NUMBER:</label>
                            <input type="text" name="idno" id="idno" class="form-control" required  disabled>
                            
                            <label>PHONE:</label>
							<input type="text" name="phone" id="phone" class="form-control" oninput='phonenumber(this)' required>
							<div class="tooltip">
							    <span class="tooltiptext">Wrong Phone number</span>
                            </div>

							<label>EMAIL:</label> 
							<input type="text" id="email" name="email" class="form-control" oninput='emailValidate(this)' required>


                            <!-- <input type="text" name="address" id="address" class="form-control" placeholder="33, Cindy Str. Greendale" disabled required>

                            <input type="text" name="city" id="city" class="form-control" placeholder="Johannesburg" disabled required> -->

							<br/>

                            <!-- <p>Arrange a debit order on your account and we will send you R1000 coupon!</p> -->
                            <p> Pay now with your Debit or Credit Card and receive a R1000 coupon!</p>

                            <button type="button" id="profile_edit" style="width: 100%;background-color: #282E54;border: 1px solid #282E54;-moz-border-radius: 0px;
                            -webkit-border-radius: 0px;
                            border-radius: 0px;" class="btn btn-lg btn-primary btn-block" onclick="mysender()">OK</button>

						

							</form> 
							<a href="https://api.whatsapp.com/send?phone=27726935327&text=Hi NuDebt, I need assistance on your PWA" id="whatsappBtn"><img src="Whatsapp-512.png" width="60" height="60" style="margin-left: -4%;margin-bottom: 6%;"/></a> 
					</div>
 
				</div>
			</div>

			<div class="col-md-4">

				&nbsp;

			</div>

		</div>
<p align="center" style="color:#ffffff;font-family: eras-demi-itc;font-size: 11px;"> <br/>Copyright  &copy; NuDebt 2019  <p> 
<br/>
    </div>

</body>

</html>

