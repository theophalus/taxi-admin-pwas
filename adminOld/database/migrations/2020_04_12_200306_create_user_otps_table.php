<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserOtpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_otps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('beneficiary_id');
            $table->unsignedBigInteger('claimant_id');
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('payment_id');
            $table->string('code');
            $table->string('random_key');
            $table->timestamps();
        });

        // REFERENCIAL INTEGRITY
        Schema::table('user_otps', function ($table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('beneficiary_id')
                ->references('id')
                ->on('taxi_beneficiaries')
                ->onDelete('cascade');

            $table->foreign('claimant_id')
                ->references('id')
                ->on('claimants')
                ->onDelete('cascade');

            $table->foreign('member_id')
                ->references('id')
                ->on('taxi_association_members')
                ->onDelete('cascade');

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_otps');
    }
}
