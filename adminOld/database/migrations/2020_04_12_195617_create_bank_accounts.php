<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('bank_id');
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('beneficiary_id');
            $table->unsignedBigInteger('claimant_id');
            $table->integer('account_number');
            $table->string('account_name');
            $table->string('account_type');
            $table->integer('bank_code');
            $table->string('branch_name');
            $table->integer('account_availability_status')->default(0);
            $table->timestamps();
        });

        // REFERENCIAL INTEGRITY
        Schema::table('bank_accounts', function ($table) {
            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');

            $table->foreign('claimant_id')
                ->references('id')
                ->on('claimants')
                ->onDelete('cascade');

            $table->foreign('member_id')
                ->references('id')
                ->on('taxi_association_members')
                ->onDelete('cascade');

            $table->foreign('beneficiary_id')
                ->references('id')
                ->on('taxi_beneficiaries')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
