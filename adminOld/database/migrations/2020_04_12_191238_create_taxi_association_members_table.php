<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxiAssociationMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxi_association_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('taxi_assoc_id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('cellphone_number');
            $table->string('gender');
            $table->integer('license_number');
            $table->integer('pnr_number');
            $table->tinyInteger('id_validity_status')->default(0);
            $table->timestamps();
        });

        // REFERENCIAL INTEGRITY
        Schema::table('taxi_association_members', function ($table) {
            $table->foreign('taxi_assoc_id')
                ->references('id')
                ->on('taxi_associations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxi_association_members');
    }
}
