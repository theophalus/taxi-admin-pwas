<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-menu-bk {
            background-color: #000000;
        }

        .menu-items {
            font-size: 13px;
        }

        /*Side Menu*/

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            max-width: 370px;
            margin: auto;
            text-align: center;
            font-family: arial;
            border: 1px solid #e9ecef;
            border-radius: 10px;
        }

        #myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {
            opacity: 0.7;
        }

        .main-title {
            font-size: 2.1rem;
        }

        .title {
            color: grey;
            font-size: 18px;
        }

        .sub-title {
            color: #000000;
            font-weight: bold;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9);
            /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation */
        .modal-content,
        #caption {
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {
                -webkit-transform: scale(0)
            }

            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes zoom {
            from {
                transform: scale(0)
            }

            to {
                transform: scale(1)
            }
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 90px;
            right: 90px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px) {
            .modal-content {
                width: 100%;
            }
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed" style="padding: 0px !important; margin: 0px !important;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a class="dropdown-item" href="http://54.246.148.187/admin/public/taxi_assocs_home">
                        Home
                    </a></li>
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Navbar 
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
     Left navbar links
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Payments</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Transactions</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Audit Trails</a>
            </li>
        </ul>
 -->
        <!-- Right navbar links 
        <ul class="navbar-nav ml-auto">

        </ul>
    </nav>
    s/.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="80"> </h4>
                    <h4>{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style="background-color: #000000; padding: 10px;">
                            <br /><br />
                            <a href="http://54.246.148.187/admin/public/taxiMain" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    All taxi assciations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiApproved" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Approved associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiPending" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Pending Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiDeclined" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Declined Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="background-color: #ffffff;">
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach()
                    </div>
                    @endif
                    <!-- Main content  -->
                    <section class="content">
                        <div class="container-fluid">

                            <div class="col-lg-12 mt-2" style="background-color: #ffffff;">
                                <br />
                                <a class="btn btn-default" onclick="goBack()">
                                    Back
                                </a>
                                <br />
                                <br />

                                <!-- Posts list -->
                                <h4>Chairperson/Secretary profile<h4>
                                        <hr />
                                        <?php //print_r("<pre>"); var_dump($data); die(); 
                                        ?>

                                        <div class="row">
                                            <div class="col-xs col-sm col-md">
                                                <div class="card">
                                                    <?php if ($data[0]->image === NULL) { ?>
                                                        <img src="{{ asset('images/personIcon.jpg') }}" style="width:100%">
                                                    <?php } else { ?>
                                                        <img id="myImg" src="http://54.246.148.187/register-association/<?php echo $data[0]->image; ?>" style="width:100%">
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <div class="col-xs col-sm col-md">
                                                <div class="card">
                                                    <h1 class="main-title">{{ $data[0]->type }} </h1>
                                                    <hr />
                                                    <p class="title"> <span class="sub-title">ID:</span> {{$data[0]->idNum}}</p>
                                                    <p class="title"> <span class="sub-title">Name:</span> {{ $data[0]->meName }} {{ $data[0]->surname }}</p>
                                                    <p class="title"> <span class="sub-title">Cell:</span> {{ $data[0]->cell}}</p>
                                                    <p class="title"> <span class="sub-title">Email:</span> {{ $data[0]->email}}</p>
                                                    <p class="title"> <span class="sub-title">Gender:</span> {{ $data[0]->gender}}</p>

                                                    <?php if ($data[0]->type !== "Secretary") { ?>
                                                        <p class="title"><span class="sub-title">License number:</span> {{ $data[0]->licenseNo}}</p>
                                                        <p class="title"><span class="sub-title">Licence expiry date:</span> {{ $data[0]->licenseExpiryDate}}</p>
                                                        <p class="title"><span class="sub-title">Pnr expiry date:</span> {{ $data[0]->pnrLicenseExpiryDate}}</p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <!-- /.card -->

                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                    <!-- The Modal -->
                    <div id="myModal" class="modal">
                        <span class="close">&times;</span>
                        <img class="modal-content" id="img01">
                        <div id="caption"></div>
                    </div>

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->


        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>

    <script>
        // Get the modal
        var modal = document.getElementById("myModal");

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById("myImg");
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function() {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>

</html>