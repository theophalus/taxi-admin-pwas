<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">


    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-menu-bk {
            background-color: #000000;
        }

        .menu-items {
            font-size: 13px;
        }

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed" style="padding: 0px !important; margin: 0px !important;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a class="dropdown-item" href="http://54.246.148.187/admin/public/taxi_assocs_home">
                        Home
                    </a>
                </li>
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->




        <!-- Navbar 
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
     Left navbar links
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Payments</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Transactions</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Audit Trails</a>
            </li>
        </ul>
 -->
        <!-- Right navbar links 
        <ul class="navbar-nav ml-auto">

        </ul>
    </nav>
    s/.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="80"> </h4>
                    <h4>{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style=" padding: 10px;">
                            <br /><br />
                            <a href="http://54.246.148.187/admin/public/taxiMain" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    All taxi assciations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiApproved" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Approved associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiPending" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Pending Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiDeclined" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Declined Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="background-color: #ffffff;">


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach()
                    </div>
                    @endif



                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">

                            <div class="col-lg-12 mt-2 main-content">
                                <br />
                                <a class="btn btn-default" onclick="goBack()">
                                    Back
                                </a>
                                <br />
                                <br />

                                <!-- Posts list -->
                                <h4>{{ $name_link }}</h4>
                                <div class="row" style="background-color: #ffffff;">
                                    <div class="col-lg-12 margin-tb">

                                        <div>
                                            <input type="text" style="width: 30%;" class="form-control" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                                            <br />
                                        </div>
                                    </div>
                                </div>
                                <div class="row bx-shw">
                                    <div class="col-xs-12 col-sm-12 col-md-12" style="padding-right: 0px;">
                                        <table class="table table-striped task-table" id="myTable" style="width: 100%;">
                                            <!-- Table Headings -->
                                            <thead class="text-center cust-table-theads">
                                                <th>Date Created</th>
                                                <th>Taxi Associations</th>
                                                <th>Province</th>
                                                <th>Town</th>
                                                <th>Telephone</th>
                                                <th>Email</th>
                                                <th>Number of taxis</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </thead>

                                            <?php
                                            //var_dump($data); 
                                            //die();
                                            ?>

                                            <!-- Table Body -->
                                            <tbody class="text-center">
                                                @foreach($data as $datas)
                                                <tr>
                                                    <td class="table-text">
                                                        <div>{{$datas->date}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->name}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->province}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div><?php echo trim($datas->town); ?></div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->telephone}}</div>
                                                    </td>
                                                    <td class="ta>ble-text">
                                                        <div>{{$datas->email}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->number}}</div>
                                                    </td>

                                                    <td class="table-text" valign="middle">
                                                        <div>
                                                            <?php if ($datas->status === 1) { ?>
                                                                <i class="fas fa-check" style="color:green"></i>
                                                            <?php } ?>
                                                        </div>

                                                        <div>
                                                            <?php if ($datas->status === 2) { ?>
                                                                <p style="color: #ff2200;font-size:17px;"><b>X</b></p>
                                                            <?php } ?>
                                                        </div>

                                                        <div>
                                                            <?php if ($datas->status === 0) { ?>
                                                                <i class="fa fa-question" style="color: orange;"></i>
                                                            <?php } ?>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <a href="http://54.246.148.187/admin/public/taxiDetails/{{$datas->id}}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>


                                        <?php
                                        //die();
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card -->

                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->


        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- DataTables -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>


    <script>
        // data tables with export buttons
        $(document).ready(function() {
            $('#myTable').DataTable();
        } );

        /*   $(document).ready(function() {
               $('#myTable').DataTable( {
                   dom: 'Bfrtip',
                   rowReorder: {
                       selector: 'tr',
                       //   update: true,
                       //   dataSrc: '.ord-id'
                   },

               } );
           } );*/
    </script>


    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>

</html>