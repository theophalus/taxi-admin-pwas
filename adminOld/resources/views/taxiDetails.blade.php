<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-menu-bk {
            background-color: #000000;
        }

        .menu-items {
            font-size: 13px;
        }

        /*END of Side Menu*/

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .reason-4-decline {
            font-weight: bold;
            font-size: 1.3rem;
        }

        .reason-declined {
            color: #ff2200;
            font-weight: bold;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed" style="padding: 0px !important; margin: 0px !important;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a class="dropdown-item" href="http://54.246.148.187/admin/public/taxi_assocs_home">
                        Home
                    </a></li>
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Navbar 
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
     Left navbar links
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Payments</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Transactions</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Audit Trails</a>
            </li>
        </ul>
 -->
        <!-- Right navbar links 
        <ul class="navbar-nav ml-auto">

        </ul>
    </nav>
    s/.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="index3.html" class="brand-link">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="80"> </h4>
                    <h4>{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style="background-color: #000000; padding: 10px;">
                            <br /><br />
                            <a href="http://54.246.148.187/admin/public/taxiMain" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    All taxi assciations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiApproved" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Approved associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiPending" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Pending Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="http://54.246.148.187/admin/public/taxiDeclined" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Declined Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="background-color: #ffffff;">


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach()
                    </div>
                    @endif



                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">

                            <div class="col-lg-12 mt-2 main-content">
                                <?php if ($data[0]->status === 1) { ?>
                                    <br />
                                    <a class="btn btn-default" onclick="goBack()">
                                        Back
                                    </a>
                                    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#rejectModal" style="background-color:#ff2200;border: 1px solid #ff2200;">
                                        Decline
                                    </a>
                                <?php }
                                if ($data[0]->status === 2) { ?>
                                    <br />
                                    <a class="btn btn-default" onclick="goBack()">
                                        Back
                                    </a>
                                    <a class="btn btn-success" href="http://54.246.148.187/admin/public/reprove/<?php echo $id; ?>">
                                        Approve
                                    </a>
                                <?php }
                                if ($data[0]->status === 0) { ?>
                                    <br />
                                    <a class="btn btn-default" onclick="goBack()">
                                        Back
                                    </a>
                                    <a class="btn btn-success" href="http://54.246.148.187/admin/public/reprove/<?php echo $id; ?>">
                                        Approve
                                    </a>
                                    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#rejectModal" style="background-color:#ff2200;border: 1px solid #ff2200;">
                                        Decline
                                    </a>
                                <?php } ?>
                                <!-- Posts list -->

                                <div class="row" style="background-color: #ffffff;">
                                    <div class="col-lg-12 margin-tb">

                                        <div>
                                            <br />
                                            <input type="text" id="myInput" style="width: 30%;" class="form-control" onkeyup="myFunction()" placeholder="Search for names..">
                                            <br />
                                        </div>
                                    </div>
                                </div>

                                <div class="bx-shw">
                                    <div class="row text-center ">
                                        <div class="col-xs col-sm col-md">
                                            <h1><?php echo $data[0]->name; ?></h1>
                                        </div>
                                    </div>

                                    <?php if ($data[0]->status === 2) { ?>
                                        <div class="row text-center">
                                            <div class="col-xs col-sm col-md">
                                                <p>
                                                    <span class="reason-4-decline">Reason for decline:</span>
                                                    <span class="reason-declined"><?php echo $addReason[0]->reason; ?> </span>
                                                </p>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <table class="table table-striped task-table" id="myTable">
                                                <!-- Table Headings -->
                                                <thead class="text-center cust-table-theads">
                                                    <th>Role</th>
                                                    <th>ID Number</th>
                                                    <th width="12.25%">Name</th>
                                                    <th width="12.25%">Surname</th>
                                                    <th width="12.25%">Cell Number</th>
                                                    <th width="12.25%">Email</th>
                                                    <th width="12.25%">Gender</th>
                                                    <th width="12.25%">Action</th>
                                                </thead>
                                                <?php
                                                //var_dump($data); 
                                                //die();
                                                ?>
                                                <!-- Table Body -->
                                                <tbody class="text-center">
                                                    @foreach($data as $datas)
                                                    <tr>
                                                        <td class="table-text">
                                                            <div>{{$datas->type}}</div>
                                                        </td>
                                                        <td class="table-text">
                                                            <div>{{$datas->idNum}}</div>
                                                        </td>
                                                        <td class="table-text">
                                                            <div>{{$datas->name}}</div>
                                                        </td>
                                                        <td class="table-text">
                                                            <div>{{$datas->surname}}</div>
                                                        </td>
                                                        <td class="table-text">
                                                            <div>{{$datas->cell}}</div>
                                                        </td>
                                                        <td class="table-text">
                                                            <div>{{$datas->email}}</div>
                                                        </td>
                                                        <td class="ta>ble-text">
                                                            <div>{{$datas->gender}}</div>
                                                        </td>

                                                        <td>
                                                            <a href="http://54.246.148.187/admin/public/taxiProfile/{{$datas->taxiassocId}}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                <i class="fas fa-eye"></i>
                                                            </a>
                                                        </td>

                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->

                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->


        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>


    <!-- Modal -->
    <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <form method="post" action="{{ route('decline_reason') }}">
                        @csrf
                        <h4 align="center"><u>Reason for decline</u></h4>
                        <br />
                        <select name="dReason" class="form-control" style="color:#000000;">
                            <option value="Association not in existence">
                                Association not in existence
                            </option>
                            <option value="Association fake">
                                Association fake
                            </option>
                            <option value="Association not a registered association">
                                Association not a registered association
                            </option>
                            <option value="Association illegal">
                                Association illegal
                            </option>

                        </select>

                        <br />
                        <!-- Additional comments:<br/><br/>
<textarea class="form-control" name="addReason">
</textarea>
<br/>-->
                        <input type="hidden" name="id" value="{{ $id }}">
                        <button type="submit" class="btn btn-primary" style="width:100%;background-color: #000000;color: #ffffff;border:1px solid #000000;">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</body>

</html>