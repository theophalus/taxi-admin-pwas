<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-menu-bk {
            background-color: #000000;
        }

        .menu-items {
            font-size: 13px;
        }

        /*END OF Side Menu*/

        .box-padding{
            padding: 25px;
        }
        .custom-box{
            box-shadow: 0px 4px 8px #888888;
        }
        .box-font{
            font-size: 1.3rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="80"> </h4>
                    <h4>{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style="background-color: #000000; padding: 10px;">
                            <br /><br />
                            <a href="{{ route('taxi_assocs_home')  }}" class="nav-link active">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Taxi assciations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="{{ route('claimants')  }}" class="nav-link active">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Claimants
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="{{ route('query_management')  }}" class="nav-link active">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Query Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="{{ route('fund_administrator')  }}" class="nav-link active">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                                <p class="menu-items">
                                    Fund Administrator
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br />
                            <a href="{{ route('user_management')  }}" class="nav-link active">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p class="menu-items">
                                    User Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>


                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">

                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach()
                    </div>
                    @endif

                    <!-- Info boxes -->
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-6 box-padding">
                            <div class="info-box custom-box">
                                <div class="info-box-content">
                                    <br />
                                    <h4 class="info-box-text" style="text-align: center">
                                        Registered Taxi Associations
                                    </h4>
                                    <span class="info-box-number" style="text-align: center">
                                        {{ $taxi_assoc_count }}
                                    </span>
                                    <br />
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-lg-6 box-padding">

                            <div class="info-box mb-6 custom-box">
                                <div class="info-box-content">
                                    <br />
                                    <h4 class="info-box-text" style="text-align: center">
                                        Registered Claimants
                                    </h4>
                                    <span class="info-box-number" style="text-align: center">
                                        {{ $claimant_count }}
                                    </span>
                                    <br />
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->

                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>

                    </div>
                    <!-- /.row -->



                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">
                            <div class="col-lg-12">
                                @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                                @endif
                                <!-- Posts list -->
                                @if(!empty($users))
                                <div class="row">
                                    <div class="col-lg-12 margin-tb">
                                        <div class="pull-left">
                                            <h2></h2>
                                        </div>
                                        <div class="pull-right">
                                            <!-- <a class="btn btn-success" href="{{ route('home') }}" data-toggle="modal" data-target="#modal-lg">
                                                    Add User
                                                </a>-->
                                        </div>
                                    </div>
                                </div>

                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="{{ route('taxi_assocs_home')  }}">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="box-font" align="center">Taxi Associations Module</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="box-font" align="center">Claimants Module</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="box-font" align="center">Query Management Module</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="box-font" align="center">Fund Administratot Module</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="{{ route('fund_administrator')  }}">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="box-font" align="center">User Management Module</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.row -->
                                @endif
                            </div>
                            <!-- /.card -->

                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <! --------------------------------------------------------------------- MODAL ----------------------------------------------------------------------->
            <!-- .modal -->
            <div class="modal fade" id="modal-lg">
                <!-- .modal-dialog -->
                <div class="modal-dialog modal-lg col-md-5">
                    <form method="post" action="{{ route('create_user') }}">
                        @csrf
                        <div class="modal-content" style="width: 55%">
                            <div class="modal-header">
                                <h4 class="modal-title">User Registration</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Firstname" required>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Surname" required>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Email Address" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input id="password" type="password" style="margin-top: 10px;" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <input id="password-confirm" type="password" style="margin-top: 10px;" placeholder="Re-password" class="form-control" name="password_confirmation" required autocomplete="off">
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add User</button>
                            </div>
                        </div>
                    </form>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
            </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
</body>

</html>