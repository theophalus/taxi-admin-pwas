<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = [
        'user_id', 'code',
    ];

    protected $casts = [
        'createdAt' => 'datetime',
    ];
}
}
