<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelHasPermission extends Model
{
    protected $fillable = [
        'role_id', 'model_type', 'permission_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function permission(){
        return $this->belongsTo('Spatie\Permission\Models\Permission');
    }
}
