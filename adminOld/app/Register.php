<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $fillable = [
        'taxName', 'taxProvince', 'taxTown', 'taxPhone',
        'taxEmail', 'taxNumber', 'person', 'idNum', 'fulName',
        'cell', 'email', 'gender', 'code'
    ];

    protected $casts = [
        'createdAt' => 'datetime',
    ];
}
