<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc; 
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

class TaxiController extends Controller
{
    public function index($id)
    { 
//echo $id; die();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $addReason = DB::select("select * from decline_reason where taxiAssocId = '$id'");
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '".$data1[0]->province."' and taxi_assocs.town = '".$data1[0]->town."' and taxi_assocs.name = '".addslashes($data1[0]->name)."'");
        return view('taxiDetails', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'id' => $id,
             'addReason' => $addReason
        
        
        ]);
    }

public function decline_reason(Request $request)
    {

DB::table('decline_reason')->insert([
                'id' => null,
                'taxiAssocId' => $request->id, 'reason' => $request->dReason,
                'addReason' => ""
            ]);
$id = $request->id;
$data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 2 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
       return redirect()->route('taxi_assocs_home');
}

public function reprove($id)
    {
//echo "hello"; die();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $RemoveDecline = DB::select("delete from decline_reason where taxiAssocId = '$id'");
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 1 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
        return redirect()->route('taxi_assocs_home');
    }

public function deprove($id)
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 2 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
       return redirect()->route('taxi_assocs_home');
    }
   
    public function main()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM `taxi_assocs` where `type` = 'Chairperson' ");
$fas = "List of all taxi association";
//print_r("<pre>");var_dump($data);die();
        return view('taxidetaild', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }
    
    public function pending()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs where status = 0 and type = 'Chairperson'");
	$fas = "List of all pending association";
        return view('taxidetaild', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }
   public function approved()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs  where status = 1 and type = 'Chairperson'");
$fas = "List of all pending association";
        return view('taxidetaild', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
'name_link' => $fas
        ]);
    }

public function declined()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs  where status = 2 and type = 'Chairperson'");
$fas = "List of all declined association";
        return view('taxidetaild', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
'name_link' => $fas
        ]); 
    }

public function profile($id)
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.name as meName, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.id = $id");
     //var_dump($data); die();   
return view('taxiProfile', ['data' => $data,
        'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count
        ]);
    }


}
 
?>