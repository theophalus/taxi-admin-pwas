<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

//Importing laravel-permission models
use Spatie\Permission\Models\Permission;
//use App\ModelHasPermission;
use Spatie\Permission\Models\Role;
//se App\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $claimants = Claimant::orderBy('id', 'desc')->get();
        $beneficiaries = TaxiBeneficiary::orderBy('id', 'desc')->get();
        $members = TaxiAssociationMember::orderBy('id', 'desc')->get();
        $users = User::orderBy('id', 'desc')->get();
        $taxi_assoc_count = TaxiAssoc::count();
        $taxi_assoc_count = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' ");
        $taxi_assoc_count = $taxi_assoc_count[0]->mycount;
        $claimant_count = Claimant::count();
        return view('home', [
            'claimants' => $claimants,
            'members' => $members,
            'beneficiaries' => $beneficiaries,
            'users' => $users,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    public function users()
    {
        $users = User::orderBy('id', 'desc')->get();
        return view('users', [
            'users' => $users,
        ]);
    }
    /*
     * ----------------------------------------------------
     *          ADMIN USERS
     * ----------------------------------------------------
     * */
    public function create_user(Request $request)
    {

        //validate data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $form_data = array(
            'first_name' =>   $request->first_name,
            'last_name' =>   $request->last_name,
            'email' =>   $request->email,
            'password' => Hash::make($request->password),
            'role_id' => $request->role_id
        );

        User::create($form_data);
        //store status message
        Session::flash('success_msg', 'User createed successfully!');

        //redirect back to the main page
        return back();
    }

    public function delete_user($id){
        //update post data
        User::find($id)->delete();

        //store status message
        Session::flash('success_msg', 'User deleted successfully!');

        return redirect()->route('users');
    }

    public function user_details($id){
        //fetch user data
        $user = User::find($id);

        //pass user data to view and load list view
        return view('user_details', ['user' => $user]);
    }

    public function edit_user($id){
        //get post data by id
        $user = User::find($id);

        //load form view
        return view('edit_user', ['user' => $user]);
    }

    public function update_user($id, Request $request){
        //validate post data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);

        //get post data
        $userData = $request->all();

        //update post data
        User::find($id)->update($userData);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('home');
    }

    /*
     * ----------------------------------------------------
     *          TAXI USERS
     * ----------------------------------------------------
     * */
    public function taxi_users(){
        $taxi_users = TaxiUser::orderBy('id', 'desc')->get();
        $taxi_users = TaxiUser::paginate(15);
        $cnt = TaxiUser::count();
        return view('taxi_users', [
            'taxi_users' => $taxi_users,
            'cnt' => $cnt
        ]);
    }

    public function edit_taxi_user($id){
        //get post data by id
        $taxi_user = TaxiUser::find($id);

        //load form view
        return view('edit_taxi_user', ['taxi_user' => $taxi_user]);
    }

    public function update_taxi_user($id, Request $request){
        //validate post data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);

        //get post data
        $userData = $request->all();

        //update post data
        TaxiUser::find($id)->update($userData);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('home');
    }

    public function taxi_user_details($id){
        //fetch user data
        $taxi_user = TaxiUser::find($id);

        //pass user data to view and load list view
        return view('taxi_user_details', ['taxi_user' => $taxi_user]);
    }

    public function delete_taxi_user($id){
        //update post data
        TaxiUser::find($id)->delete();

        //store status message
        Session::flash('success_msg', 'User deleted successfully!');

        return redirect()->route('taxi_users');
    }

    /*
* ----------------------------------------------------------------------------
* TAXI ASSOCIATIONS HOME
* ----------------------------------------------------------------------------
* */
    public function taxi_assocs_home(){
        $user = User::orderby('id', 'desc')->where('id', '=', '1')->get();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        $taxi_assoc_count = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' ");
        $taxi_assoc_count = $taxi_assoc_count[0]->mycount;
        $member_counts = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 0");
        $member_counts = $member_counts[0]->mycount;
        $member_countss = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 1");
        $member_countss = $member_countss[0]->mycount;
        $member_countsss = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 2");
        $member_countsss = $member_countsss[0]->mycount;
        $claimant_count = Claimant::count();
        return view('taxi_assocs_home', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'member_counts' => $member_counts,
            'member_countss' => $member_countss,
            'member_countsss' => $member_countsss,
            'user' => $user
        ]);
    }

    /*
 * ----------------------------------------------------------------------------
 * TAXI ASSOCIATIONS
 * ----------------------------------------------------------------------------
 * */

    public function taxi_assocs(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        return view('taxi_assocs', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count
        ]);
    }

    public function taxi_associations(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assoc_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $claimant_count = Claimant::count();
        return view('taxi_associations', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    public function delete_taxi_assoc($id){
        //update data
        TaxiAssoc::find($id)->delete();

        //store status message
        Session::flash('success_msg', 'User deleted successfully!');

        return redirect()->route('users');
    }

    public function taxi_assoc_details($id){
        //fetch user data
        $taxi_assoc = TaxiAssoc::find($id);

        //pass data to view and load list view
        return view('taxi_assoc_details', ['taxi_assoc' => $taxi_assoc]);
    }

    public function edit_taxi_assoc($id){
        //get data by id
        $taxi_assoc = TaxiAssoc::find($id);

        //load form view
        return view('edit_taxi_assoc', ['taxi_assoc' => $taxi_assoc]);
    }

    public function update_taxi_assoc($id, Request $request){
        //validate data
        $this->validate($request, [
            'name' => 'required',
            'province' => 'required',
            'town' => 'required'
        ]);

        //get data
        $taxi_assocData = $request->all();

        //update data
        TaxiAssoc::find($id)->update($taxi_assocData);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('taxi_assocs');
    }

    public function taxi_assoc_edit_approval_status($id){
        //get data by id
        $taxi_assoc = TaxiAssoc::find($id);

        //load form view
        return view('taxi_assoc_edit_approval_status', ['taxi_assoc' => $taxi_assoc]);
    }

    public function taxi_assoc_approval_status($id, Request $request){
        $this->validate($request, [
            'status_reason' => 'required',
            'status' => 'required',
        ]);

       // $taxi_assoc = TaxiAssoc::find($id);

        if(ApprovalStatus::find($id) == null){
            $form_data = array(
                'status_reason' =>   $request->status_reason,
                'status' =>   $request->status,
                'user_id'  =>  Auth::user()->id,
                'taxi_assoc_id' => $request->taxi_assoc_id
            );

            ApprovalStatus::create($form_data);

        }else{
            //get data
            $taxi_assocData = $request->all();

            //update data
            ApprovalStatus::find($id)->update($taxi_assocData);
        }


        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('taxi_assocs');
    }


    /*
 * ----------------------------------------------------------------------------
 * STORE CLAIMANTS
 * ----------------------------------------------------------------------------
 * */
    public function claimants(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        return view('claimants', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* QUERY MANAGEMENT
* ----------------------------------------------------------------------------
* */
    public function query_management(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        return view('query_management', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* FUND ADMINISTRATOR
* ----------------------------------------------------------------------------
* */
    public function fund_administrator(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        return view('fund_administrator', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* USER MANAGEMENT
* ----------------------------------------------------------------------------
* */
    public function user_management(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        //$roles = Role::orderBy('id', 'desc')->get();
        //$permissions = Permission::orderBy('id', 'desc')->get();
        $roles = Role::all(); //Get all roles
        $permissions = Permission::all();

        return view('user_management', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'roles' => $roles,
            'permissions' => $permissions
        ]);

    }

    public function store_claimant(Request $request)
    {

        //validate data
        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'cellphone_number' => 'required',
            'gender' => 'required',
            'license_number' => 'required',
            'pnr_number' => 'required',
            'id_validity_status' => 'required',
        ]);

        $form_data = array(
            'title'          =>   $request->title,
            'first_name'       =>   $request->first_name,
            'last_name' =>   $request->last_name,
            'email'           =>   $request->email,
            'cellphone_number' => $request->cellphone_number,
            'gender' => $request->gender,
            'license_number' => $request->license_number,
            'pnr_number' => $request->pnr_number,
            'id_validity_status' => $request->id_validity_status
        );

        Claimant::create($form_data);

        //store status message
        Session::flash('success_msg', 'Folder added successfully!');

        //redirect back to the main page
        return back();
    }

    /*
 * ----------------------------------------------------------------------------
 * TAXI MEMBERS
 * ----------------------------------------------------------------------------
 * */


    /*
    --------------------------------------------------------
        PWA USERS
    --------------------------------------------------------
    */

    public function registers(){
        $registers = Register::orderBy('id', 'desc')->get();
        $registers = Register::paginate(15);
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        return view('registers', [
            'registers' => $registers,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count
        ]);
    }

    public function taxi_members(){
        $taxi_members = TaxiMember::orderBy('id', 'desc')->get();
        return view('taxi_members', [
            'taxi_members' => $taxi_members
        ]);
    }

    public function approval_status($id){
        //get status data by id
        $register = Register::find($id);

        //load form view
        return view('approval_status', ['register' => $register]);
    }

    public function update_approval($id, Request $request)
    {
        //validate data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);
    }

    public function payments(){
        $payments = Payment::orderBy('id', 'desc')->get();
        return view('payments', [
            'payments' => $payments
        ]);
    }

    public function transactions(){
        $transactions = BankAccount::orderBy('id', 'desc')->get();
        return view('transactions', [
            'transactions' => $transactions
        ]);
    }

    public function errors(){
        return view('errors');
    }

}
