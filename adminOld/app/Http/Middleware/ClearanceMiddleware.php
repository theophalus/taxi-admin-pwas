<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::user()->hasPermissionTo('Administer roles & permissions')) //If user has this //permission
        {
            return $next($request);
        }

        if ($request->is('home/user_management'))//If user is creating a user
        {
            if (!Auth::user()->hasPermissionTo('Edit All Fields'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is('Edit/*/Fields')) //If user is editing a fields
        {
            if (!Auth::user()->hasPermissionTo('Edit All Taxi Association and Related Fields')) {
                abort('401');
            } else {
                return $next($request);
            }
        }

        return $next($request);
    }
}
