<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiAssoc extends Model
{
    protected $fillable = [
        'user_id', 'name', 'province', 'town',
        'telephone', 'email', 'number', 'type'
    ];

    protected $casts = [
        'date' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
