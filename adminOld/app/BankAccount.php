<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
        'beneficiary_id', 'member_id', 'claimant_id', 'account_number', 'account_name',
        'account_type', 'branch_name', 'branch_code', 'account_availability_status',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
