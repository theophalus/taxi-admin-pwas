<?php if(!isset($_POST['province'])){ header("Location: https://app.ticrf.co/taxiassociations/"); } ?>

<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Relief Fund</title>
    <link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
    <link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">

    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">


 


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="signin.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="welcome.css" />
    <link rel="manifest" href="manifest.json">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!--<script src="login.js"></script>-->
    <script>
/*
if((localStorage.getItem("page") === "4") || (localStorage.getItem("page") === "5"))
{
localStorage.setItem("page", "5");
}
else
{
location.href = "index.html";
}
*/
<?php
/*if(isset($_SESSION["newsession"]))
{
session_start();
if(($_SESSION["newsession"] == "4") || ($_SESSION["newsession"] == "5"))
{
$_SESSION["newsession"] = "5";
}
else
{
header("Location: http://54.246.148.187/register-association/index.html");
}
}
*/?>
        /*function myFunction() {
            var w = window.outerWidth;
            var h = window.outerHeight;
            var txt = "Window size: width=" + w + ", height=" + h;
            document.getElementById("demo").innerHTML = txt;
        }*/

        //Login.js file
        $(document).ready(function()
        {
            if('serviceWorker' in navigator)
            {
                //Register the Service Worker
                navigator.serviceWorker.register('sw2.js').then(function(registration)
                {
                    registration.update();
                    registration.installing; // the installing worker, or undefined
                    registration.waiting; // the waiting worker, or undefined
                    registration.active; // the active worker, or undefine
                    console.log('Service Worker Registered2');
                });
            } else console.log('Your browser does not support the Service-Worker!');
            if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
            {
                window.location.href = "profile.html"; // 	if Session created redirect to Profile page
            }
        });

        /*

            Authentification code function

        */
    </script>
</head>

<body id="loginPage" style="font-family: Helvetica;">
<div class="container">
    <!-- Row start to create 10% margin spacing here-->
    <div class="row">
        <div class="col-md-12" style="margin-top:0%;">&nbsp;</div>
    </div>
    <!-- Row Ends to create 10% margin spacing here-->

    <!-- Row start to divide into 3 sections left margin-->
    <div class="row">
        <!-- Start Collumn for left margin-->
        <div class="col-md-4">&nbsp;</div>
        <!-- End Collumn for left margin-->

        <!-- Start Center Collumn where all information will be sitting -->
        <div class="col-md-4">
            <div class="mainBlock">
                <div class="logoDiv">
                    <img src="relief.JPG" width="250" />
                </div>

                <div class="mainBlockSection">
                    <!-- <p id="error_message" align="center"> </p> -->
                    <form style="font-family: Helvetica !important" class="form-signin cameraForm" action="register.php" method="post" style="margin-top:0px;padding-top:0px;" onkeydown="return event.key != 'Enter';>
                        <h4 align="center" style="font-family: Helvetica !important"></h4>
                    <h3 align="center" style="font-family: Helvetica !important">Photo has been uploaded</h3>                 <br/>
                    <input type="hidden" name="province" value="<?php echo $_POST['province']; ?>" class="form-control">
                    <input type="hidden" name="city" value="<?php echo $_POST['city']; ?>" class="form-control">
                    <input type="hidden" name="assoc" value="<?php echo $_POST['assoc']; ?>" class="form-control">
                    <input type="hidden" name="assocPhone" id="phone" class="form-control" value="<?php echo $_POST['assocPhone']; ?>">
                    <input type="hidden" name="assocEmail" id="phone" class="form-control" value="<?php echo $_POST['assocEmail']; ?>">
                    <input type="hidden" name="numOfTaxies" id="phone" class="form-control" value="<?php echo $_POST['numOfTaxies']; ?>">
                    <input type="hidden" name="memberType" id="memberType" class="form-control"  value="<?php echo $_POST['memberType']; ?>">
                    <input type="hidden" name="photolocation" id="photolocation" class="form-control"  value="<?php 

function cvf_ps_generate_random_code($length=10) {

  $string = '';
  // You can define your own characters here.
  $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

  for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters)-1)];
  }

  return $string;

}
  

//print_r("<pre>");var_dump($_POST);die();
$fileStringName = cvf_ps_generate_random_code(4);
        // create a random file name
        $randName = uniqid();
        $randName .= md5($randName);
        $randName .= rand(100000, 5000000);
        mkdir('photos/' . $randName . '/');
        $imagedata = base64_decode($_POST['mymainpic']); 
	$file_name = 'photos/' . $randName . '/'.$fileStringName.'.jpg';
	file_put_contents($file_name,$imagedata);
        $location = $file_name;



echo $location; ?>">
                    <div class="row justify-content-center">
                        <table style="width:60%;">

                            <tr>
                                <td style="width:41%;padding-left:0px; text-align: center;">

                                </td>
                            </tr>
                            <tr>
                                <td style="width:41%;padding-left:0px; text-align: center;">
                                    <!--displaying the photo for preview-->

                                    <div class="center-block"><img id="photo" src="" style="display: none" alt="."  /></div>

                                </td>
                            </tr> 
                        </table>
                    </div>
                    <br/>



                    <input type="submit" id="sender" onclick="Auhtenticater_func()" class="button" style="border-radius: 17px; font-family: Helvetica;margin: 10px;height: 40px; " value="Proceed">
                    </form>


                    <script>
                        //prevent form from being submited by pressing enter
                        $('#formid').on('keyup keypress', function(e) {
                            var keyCode = e.keyCode || e.which;
                            if (keyCode === 13) {
                                e.preventDefault();
                                return false;
                            }
                        });

                    </script>

                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 33%;">
                                <a href="http://www.taxitimes.net"><img src="logos-02.jpg" width="95" height="95" /></a>
                            </td>
                            <td style="width: 33%;text-align:center">
                                <a href="https://api.whatsapp.com/send?phone=27660714008&text=Hi"> <img src="Whatsapp-512.png" width="60" height="60" ></a>
                            </td>
                            <td style="width: 33%;text-align:left;">
                                <a href="http://www.santaco.co.za"><img src="logos-01.jpg" width="98" height="94" /></a>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%;">
                        <tr>

                            <td style="width: 100%;background-color: #000000;text-align: center;padding-top:2px;padding-bottom:2px;">

                                <a href="https://sacoronavirus.co.za"   style="font-family: Helvetica;font-family: Helvetica;color: #ffffff; font-size:11px;text-align: center;">For more information about the coronavirus click here

                                </a>

                            </td>

                        </tr>
                        <tr>
                            <td style="width: 100%;text-align: center;padding-top:2px;padding-bottom:2px;">
                                <a href="downloadPdf.php"  style="font-family: Helvetica;color: #000000; font-size:11px;text-align: center;">Click here for Terms and Conditions
                                </a>
                            </td>
                        </tr>
                    </table>
                    <script>
                        //THIS SCRIPT SECTION WILL BE MOVED UP INTO HEAD ONCE RETURN KEY FUNCTIONING IS WORKING
                        function testEnter(){
                            console.log("Return Button pressed");
                        }

                        document.getElementById('loginPage').onkeydown = function(e){
                            if(e.keyCode === 13){
                                Auhtenticater_func();
                                e.preventDefault();
                            }
                        };

                    </script>

                    <br/>
                </div>
            </div>
        </div>
        <!-- End Center Collumn where all information will be sitting -->

        <!-- Start Collumn for right margin-->
        <div class="col-md-4">&nbsp;</div>
        <!-- End Collumn for right margin-->
    </div>
    <!-- End Row Main with 3 sections division (left margin/ middle for data / right margin)-->
</div>
<!-- Container Ends here-->
<script>
    function Auhtenticater_func()
    {
        //location.href="register.html";
        document.querySelector(".cameraForm").submit();
    }

</script>
</body>
</html>