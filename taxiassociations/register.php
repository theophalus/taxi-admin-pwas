<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>Relief Fund</title>
<link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
<link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">

<link rel="icon" type="image/png" sizes="192x192"  href="favicon/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="favicon/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon/img/favicon-16x16.png">

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="signin.css" />
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="welcome.css" />
        <link rel="manifest" href="manifest.json">
        <!--<script src="login.js"></script>-->
        <script>
<?php
/*session_start();
if(($_SESSION["newsession"] == "5") || ($_SESSION["newsession"] == "6"))
{
$_SESSION["newsession"] = "6";
}
else
{
header("Location: http://54.246.148.187/register-association/index.html");
}

*/?>
//if((localStorage.getItem("page") === "5") || (localStorage.getItem("page") === "6"))
//{
//localStorage.setItem("page", "6");
//}
//else
//{
//location.href = "index.html";
//}

            /*function myFunction() {
                var w = window.outerWidth;
                var h = window.outerHeight;
                var txt = "Window size: width=" + w + ", height=" + h;
                document.getElementById("demo").innerHTML = txt;
            }*/

            //Login.js file
            $(document).ready(function()
{
if(localStorage.getItem("LandedOnExpiry"))
{

 window.location.href ="https://app.ticrf.co/taxiassociations/index.html";
//alert("still on page");
}
    $( "#expirydate" ).datepicker({  dateFormat: 'yy-mm-dd' });
    $( "#pnpexpiry" ).datepicker({  dateFormat: 'yy-mm-dd' });
	if('serviceWorker' in navigator)
	{
		//Register the Service Worker
		navigator.serviceWorker.register('sw2.js').then(function(registration)
		{
			registration.update();
			registration.installing; // the installing worker, or undefined
			registration.waiting; // the waiting worker, or undefined
			registration.active; // the active worker, or undefine
			console.log('Service Worker Registered2');
		});
	} else console.log('Your browser does not support the Service-Worker!');
		if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
		{
			window.location.href = "profile.html"; // 	if Session created redirect to Profile page
		}
});



/*

	Authentification code function

*/
        </script>


        <script>
            /*check if the id has already been registered*/
            function checkIdAvailability() {
                jQuery.ajax({
                    url: "validate.php",
                    data:'idnum='+$("#idnum").val(),
                    type: "POST",
                    success:function(data){
                        $("#idmsg").html(data);
                        $("#loaderIcon").hide();
                    },
                    error:function (){}
                });
            }
        </script>


        <style>
            #tooltiptextEmail {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:550px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextLicense {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:670px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    #tooltiptextID {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:220px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }

    @media only screen and (max-width: 768px) {
        #tooltiptextID {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:220px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
    #tooltiptextLicense {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:667px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
            #tooltiptextEmail {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:545px;
    left:120px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
    }


       #tooltiptextPhone {
    display: none;
    width: 250px;
    background-color: white;
    color: #000;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1000;
    top:470px;
    left:68px;
    font-size:11px;

    -webkit-box-shadow: 5px 5px  rgba(0,0,0,0.75);
    -moz-box-shadow: 5px 2px  rgba(0,0,0,0.75);
    box-shadow: 5px 2px rgba(0,0,0,0.75);
    }
#sender2
{
display: none;
}
        </style>
    </head>

    <body id="loginPage" style="font-family: Helvetica;">
	
        <div class="container">
            <!-- Row start to create 10% margin spacing here-->
            <div class="row">
                <div class="col-md-12" style="margin-top:0%;">&nbsp;</div>
            </div>
            <!-- Row Ends to create 10% margin spacing here-->
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>


<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

            <!-- Row start to divide into 3 sections left margin-->
            <div class="row">
                <!-- Start Collumn for left margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for left margin-->

                <!-- Start Center Collumn where all information will be sitting -->
                <div class="col-md-4">
                    <div class="mainBlock">
                        <div class="logoDiv">
                            <img src="relief.JPG" width="250" />
                        </div>

                        <div class="mainBlockSection">
                        <div id="tooltiptextLicense">Please Enter a Valid License Number</div>
                            <div id="tooltiptextEmail"  style="font-family: Helvetica">Needs to be a valid email eg. john@doe.com</div>
                            <div id="tooltiptextPhone">Only numerics allowed eg. 0821234567</div>
                            <form class="form-signin registerForm" id="registerForm" action="otpv1.php" method="post" style="margin-top:0px;padding-top:0px;">
                                <h4 align="center"  style="font-family: Helvetica">Please confirm and complete the details below
                                </h4>
                                <?php

                                //print_r("<pre>");var_dump($_POST);die();

                                ?>
                               <input type="hidden" name="province" value="<?php if(isset($_GET['province'])){ echo $_GET['province']; } if(isset($_POST['province'])){ echo $_POST['province']; } ?>" class="form-control" required>
							<input type="hidden" name="city" value="<?php if(isset($_GET['city'])){ echo $_GET['city']; } if(isset($_POST['city'])){ echo $_POST['city']; } ?>" class="form-control" required>
							<input type="hidden" name="assoc" value="<?php if(isset($_GET['assoc'])){ echo $_GET['assoc']; } if(isset($_POST['assoc'])){ echo $_POST['assoc']; }?>" class="form-control" required>
							<input type="hidden" name="assocPhone" id="phone" class="form-control" value="<?php if(isset($_GET['assocPhone'])){ echo $_GET['assocPhone']; } if(isset($_POST['assocPhone'])){ echo $_POST['assocPhone']; } ?>">
							<input type="hidden" name="assocEmail" id="phone" class="form-control" value="<?php if(isset($_GET['assocEmail'])){ echo $_GET['assocEmail']; } if(isset($_POST['assocEmail'])){ echo $_POST['assocEmail']; } ?>">
							<input type="hidden" name="numOfTaxies" id="phone" class="form-control" value="<?php if(isset($_GET['numOfTaxies'])){ echo $_GET['numOfTaxies']; }if(isset($_POST['numOfTaxies'])){ echo $_POST['numOfTaxies']; } ?>">
							<input type="hidden" name="memberType" id="memberType" class="form-control" value="<?php if(isset($_GET['memberType'])){ echo $_GET['memberType']; }if(isset($_POST['memberType'])){ echo $_POST['memberType']; } ?>">
                             <input type="hidden" name="photolocation" id="photolocation" class="form-control"  value="<?php if(isset($_GET['photolocation'])){ echo $_GET['photolocation']; }if(isset($_POST['photolocation'])){ echo $_POST['photolocation']; } ?>">
                                <br/>
                                <label  style="font-family: Helvetica">ID: <br/><span id="error_message"> <?php if(isset($_GET['idnum'])){ echo "Cell phone number format incorrect"; } ?></span></label>
							<input style="font-family: Helvetica" type="text" name="idnum" id="idNum" value="<?php if(isset($_GET['idnum'])){ echo $_GET['idnum']; } ?>"class="form-control" required maxlength="13" oninput="return checkForDuplicateId(this);" onblur="validateRSAidnumber() ; checkIdAvailability()">
							<div id="debug" style="color: red; display: none;font-family: Helvetica;"></div><br>
                                <div id="idmsg" style="color: red; display: none;font-family: Helvetica;"></div>
							<label style="font-family: Helvetica">NAME:</label>
							<input type="text" name="fullName" value="<?php if(isset($_GET['fullName'])){ echo $_GET['fullName']; } ?>" id="fullName" class="form-control" required style="font-family: Helvetica;">
<label style="font-family: Helvetica">SURNAME:</label>
							<input type="text" name="surname" id="surname" value="<?php if(isset($_GET['surname'])){ echo $_GET['surname']; } ?>" class="form-control" required style="font-family: Helvetica;">
							<label style="font-family: Helvetica" style="font-family: Helvetica">CELL NUMBER:</label>
							<input type="number" name="cell" id="cellphone" class="form-control" oninput="return phonenumber(this)" required placeholder="eg. 0821234567" maxlength="10" style="font-family: Helvetica;">
							<label style="font-family: Helvetica">EMAIL:</label>
							<input type="email" name="email" id="email" value="<?php if(isset($_GET['email'])){ echo $_GET['email']; } ?>" class="form-control" oninput="return emailValidate(this);" required style="font-family: Helvetica;">
                                <label style="font-family: Helvetica">GENDER:</label>
                                <input type="text" id="gender" name="gender" class="form-control" placeholder="This will be auto populated" style="font-family: Helvetica" readonly>
                            <?php
							if(isset($_POST['memberType']))
							{
                            if($_POST['memberType'] === "Chairperson")
                            {
                                ?>
                                <label style="font-family: Helvetica">Driver License Number:</label>
							<input style="font-family: Helvetica" type="text" name="licenseNo" id="licenseNumber" class="form-control" oninput="return licenseTest(this);" required>
							<label style="font-family: Helvetica">Driver License Number Expiry Date:</label>
							<input type="text" name="licenseExpiryDate" id="expirydate"  class="form-control" onchange="checkLicenseDate()" placeholder="YYYY-mm-dd" style="font-family: Helvetica;color: #000000;" required>
							<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required>
							<label style="font-family: Helvetica">PNR Number (PDP) Expiry Date:</label>
							<input type="text" name="pnrLicenseExpiryDate" id="pnpexpiry" class="form-control" onchange="pdpLicenseDate()" placeholder="YYYY-mm-dd" style="font-family: Helvetica;" required>
							<?php

                            }
                            else
                            {
                                ?>

							<input type="hidden" name="licenseNo" id="licenseNumber" class="form-control" value="non-applica1" required style="font-family: Helvetica;">
							<input type="hidden" name="licenseExpiryDate" id="expirydate"  value="n/a" class="form-control" required style="font-family: Helvetica;">
							<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required style="font-family: Helvetica;">
							<input type="hidden" name="pnrLicenseExpiryDate"  value="n/a" id="pnpexpiry" class="form-control" required style="font-family: Helvetica;">

                                <?php

                            }
							}
                            ?>
							
							
							
							
							
							
							
							
							              <?php
							if(isset($_GET['memberType']))
							{
                            if($_GET['memberType'] === "Chairperson")
                            {
                                ?>
                                <label style="font-family: Helvetica">Driver License Number:</label>
							<input style="font-family: Helvetica" type="text" name="licenseNo" value="<?php if(isset($_GET['licenseNo'])){ echo $_GET['licenseNo']; } ?>" id="licenseNumber" class="form-control" oninput="return licenseTest(this);" required>
							<label style="font-family: Helvetica">Driver License Number Expiry Date:</label>
							<input type="date" name="licenseExpiryDate"  value="<?php if(isset($_GET['licenseExpiryDate'])){ echo $_GET['licenseExpiryDate']; } ?>" id="expirydate"  class="form-control" onchange="checkLicenseDate()" style="font-family: Helvetica;" required>
							<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required>
							<label style="font-family: Helvetica">PNR Number (PDP) Expiry Date:</label>
							<input type="date"  value="<?php if(isset($_GET['pnrLicenseExpiryDate'])){ echo $_GET['pnrLicenseExpiryDate']; } ?>" name="pnrLicenseExpiryDate" id="pnpexpiry" class="form-control"  required onchange="pdpLicenseDate()" style="font-family: Helvetica;" required> 
							<?php

                            }
                            else
                            {
                                ?>

							<input type="hidden" name="licenseNo" id="licenseNumber" class="form-control" value="non-applica1" required style="font-family: Helvetica;">
							<input type="hidden" name="licenseExpiryDate" id="license"  value="n/a" class="form-control" required style="font-family: Helvetica;">
							<input type="hidden" name="pnrLicense" id="pnrLicense"   value="n/a" class="form-control" required style="font-family: Helvetica;">
							<input type="hidden" name="pnrLicenseExpiryDate"  value="n/a" id="license" class="form-control" required style="font-family: Helvetica;">

                                <?php

                            }
							}
                            ?>
							<label style="font-family: Helvetica">NAME OF TAXI ASSOCIATION:</label>
							<input type="text" name="phone" id="phone" class="form-control" value="<?php if(isset($_GET['assoc'])){ echo $_GET['assoc']; } if(isset($_POST['assoc'])){ echo $_POST['assoc']; } ?>" readonly="readonly" required style="font-family: Helvetica;">

							<label style="font-family: Helvetica">TOWN:</label>
							<input type="text" name="phone" id="phone" class="form-control"  value="<?php if(isset($_GET['city'])){ echo $_GET['city']; } if(isset($_POST['city'])){ echo $_POST['city']; }?>" readonly="readonly" required style="font-family: Helvetica;">
							<label style="font-family: Helvetica">PROVINCE:</label>
							<input type="text" name="phone" id="phone" class="form-control"  value="<?php if(isset($_GET['province'])){ echo $_GET['province']; } if(isset($_POST['province'])){ echo $_POST['province']; } ?>" readonly="readonly" required style="font-family: Helvetica;">
                            <input type="hidden" id="idvalueCheck" value="0" > 

							   <div class="text-center"> 
                                    <input type="submit" id="sender" class="button" style="border-radius: 20px;background-color:black;font-family: Helvetica;text-align:center;height:60px;color:#ffffff;" value="Proceed" onclick="this.disabled=true;this.value='Sending...';mysubmiForm();">
<input type="button" id="sender2" class="button" style="border-radius: 20px;background-color:black;font-family: Helvetica;text-align:center;height:60px;color:#ffffff;display:none;" value="Proceed" onclick="return exp()">
                                    <!-- <a href="expired.php" type="button" id="sender2" class="button" style="border-radius: 20px;background-color:black;font-family: Helvetica;text-align:center;height:60px;color:#ffffff;" onclick="clearDate()">Proceed</a> -->
                                </div>



                            </form>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 33%;">
                                        <a href="http://www.taxitimes.net"><img src="logos-02.jpg" width="95" height="95" /></a>
                                    </td>
                                    <td style="width: 33%;text-align:center">
                                        <a href="https://api.whatsapp.com/send?phone=27660714008&text=Hi"> <img src="Whatsapp-512.png" width="60" height="60" ></a>
                                    </td>
                                    <td style="width: 33%;text-align:left;">
                                        <a href="http://www.santaco.co.za"><img src="logos-01.jpg" width="98" height="94" /></a>
                                    </td>
                                </tr>
                            </table>
                           <table style="width: 100%;">
                               <tr>

                                    <td style="width: 100%;background-color: #000000;text-align: center;padding-top:2px;padding-bottom:2px;">

                                        <a href="https://sacoronavirus.co.za"   style="font-family: Helvetica;font-family: Helvetica;color: #ffffff; font-size:11px;text-align: center;">For more information about the coronavirus click here

</a>

                                    </td>

                                </tr>
<tr>
                                    <td style="width: 100%;text-align: center;padding-top:2px;padding-bottom:2px;">
                                        <a href="downloadPdf.php"  style="font-family: Helvetica;color: #000000; font-size:11px;text-align: center;">Click here for Terms and Conditions
</a>
                                    </td>
                                </tr>
                            </table>
                            <script>

                                function clearDate(){
                                    document.getElementById("expirydate").valueAsDate = null;
                                }
                            </script>

                            <script>
                        //prevent form from being submited by pressing enter
                        $('#formid').on('keyup keypress', function(e) {
                            var keyCode = e.keyCode || e.which;
                            if (keyCode === 13) {
                                e.preventDefault();
                                return false;
                            }
                        });

                    </script>


                            <script>
                                //THIS SCRIPT SECTION WILL BE MOVED UP INTO HEAD ONCE RETURN KEY FUNCTIONING IS WORKING
                                function testEnter(){
                                    console.log("Return Button pressed");
                                }

                                document.getElementById('loginPage').onkeydown = function(e){
                                    if(e.keyCode === 13){
                                        Auhtenticater_func();
                                        e.preventDefault();
                                    }
                                };

                            </script>

                            <br/>
                        </div>
                    </div>
                </div>
                <!-- End Center Collumn where all information will be sitting -->

                <!-- Start Collumn for right margin-->
                <div class="col-md-4">&nbsp;</div>
                <!-- End Collumn for right margin-->
            </div>
            <!-- End Row Main with 3 sections division (left margin/ middle for data / right margin)-->
        </div>
        <!-- Container Ends here-->
<script>

<?php
	if(isset($_GET['idnum'])){
	?>
	document.getElementById("sender").style.display = "block";
	<?php
	}
	else
	{
	?>
	document.getElementById("sender").style.display='none';
	<?php
	}
	?>
/*function Auhtenticater_func()
{
    var idLength = document.getElementById("idNum").value.length;
    //alert(idLength);
    if(document.getElementById("idNum").value.length === 13)
    {
    document.querySelector(".registerForm").submit();
	//location.href="otp.html";
	//alert(idLength);
    }
    else
    {
       document.getElementById("error_message").innerHTML = "<br/><span style='color: red;font-family: Helvetica'>YOUR ID NUMBER IS INCORRECT !</span>";
    }

}*/

 /*function validateID() {
                var ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
                  var theIDnumber = document.getElementById("idNum").value;
                  var newMyval = theIDnumber.substring(0,13);
                  if (ex.test(theIDnumber) == false) {

		document.getElementById("idNum").value = newMyval;
                    // alert code goes here
                   // document.getElementById("error_message").innerHTML = "<br/><span style='color:red;font-family: eras-demi-itc;'>Please Enter a valid ID Number</span>";
                    //alert('Please supply a valid ID number');
                    document.getElementById("tooltiptextID").style.display = "block";
                    return false;
                  }
                  //alert(theIDnumber + ' is a valid ID number');
                  document.getElementById("idNum").value = newMyval;
                  // here you would normally obviously
                  // return true;
                  document.getElementById("tooltiptextID").style.display = "none";
                  return true;
            }

            */
function emailValidate(sel) {
            var email = sel.value;
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                document.getElementById("sender").disabled = false;
                document.getElementById("email").style.borderColor = "#282E54";
                document.getElementById("tooltiptextEmail").style.display = "none";

            }
            else{
                document.getElementById("sender").disabled = true;
                document.getElementById("email").style.borderColor = "red";
                document.getElementById("tooltiptextEmail").style.display = "block";
            }
        }




function licenseTest(inputtxt) {
    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
        var max_chars = 12;
        var mylicence = inputtxt.value;

        if(inputtxt.value.match(number)) {
            //document.getElementById("licenseNumber").style.borderColor = "#282E54";
            document.getElementById("tooltiptextLicense").style.display = "block";
            var newMyval = mylicence.substring(0,12);
		document.getElementById("licenseNumber").value = newMyval;
		if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
        }
        }
        else {
           // document.getElementById("licenseNumbere").style.borderColor = "red";
            document.getElementById("tooltiptextLicense").style.display = "none";
            var newMyval = mylicence.substring(0,12);
		document.getElementById("licenseNumber").value = newMyval;
		if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        }
        if(mylicence.length === 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        if(mylicence.length > 12)
        {
           document.getElementById("tooltiptextLicense").style.display = "none";
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}
        }
        if(mylicence.length < 12)
        {
            document.getElementById("tooltiptextLicense").style.display = "block";
            document.getElementById("sender").style.display='none';
        }
}


function cellPhone(inputtxt) {
    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
        var max_chars = 10;
        var mylicence = inputtxt.value;


        if(inputtxt.value.match(number)) {
            //document.getElementById("licenseNumber").style.borderColor = "#282E54";
            //document.getElementById("tooltiptextLicense").style.display = "block";
            var newMyval = mylicence.substring(0,10);
		document.getElementById("cellphone").value = newMyval;
		if(mylicence.length > 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none";


        }

        }
        else {
           // document.getElementById("licenseNumbere").style.borderColor = "red";
           // document.getElementById("tooltiptextLicense").style.display = "none";
            var newMyval = mylicence.substring(0,10);
		document.getElementById("cellphone").value = newMyval;

		if(mylicence.length > 10)
        {
          // document.getElementById("tooltiptextLicense").style.display = "none";
        }
        }
        if(mylicence.length === 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none"; 
        }
        if(mylicence.length > 10)
        {
           //document.getElementById("tooltiptextLicense").style.display = "none";
        }
}


 function phonenumber(inputtxt) {
            var phoneno = /^\d{10}$/;
            var max_chars = 10;

            if(inputtxt.value.match(phoneno)) {
                //document.getElementById("formChecker").value = 1;
                document.getElementById("cellphone").style.borderColor = "#282E54";
                document.getElementById("tooltiptextPhone").style.display = "none";
                document.getElementById("sender").disabled = false;
            }
            else {
                //document.getElementById("formChecker").value = 0;
                document.getElementById("cellphone").style.borderColor = "red";
                document.getElementById("tooltiptextPhone").style.display = "block";
                document.getElementById("sender").disabled = true;
            }
        }


        /*Check if the lisence expiery date*/
    function checkLicenseDate() {
        let d = Date.parse("2020-03-01"); // set min date to march and convert to milliseconds
        let selectedDate = document.getElementById('expirydate').value;
        let converted = Date.parse(selectedDate); // convert selected date to milliseconds
        if(converted < d){
            // notify the user that they dont qualify
            //document.getElementById("sender").disabled = true; // disable submit button
            document.getElementById("sender").style.display='none'; // hide submit button
            document.getElementById("sender2").style.display='block'; // Show link to error page
			localStorage.setItem("expiredLicense", "yes"); 
		}else{
			document.getElementById("sender").style.display='block'; // hide submit button
            document.getElementById("sender2").style.display='none'; // Show link to error page
			localStorage.removeItem("expiredLicense");
		}

    }





/*Check if the PDP expiery date*/
    function pdpLicenseDate() {
        let d = Date.parse("2020-03-01"); // set min date to march and convert to milliseconds
        let selectedDate = document.getElementById('pnpexpiry').value;
        let converted = Date.parse(selectedDate); // convert selected date to milliseconds
        if(converted < d){
            // notify the user that they dont qualify
            //document.getElementById("sender").disabled = true; // disable submit button
            document.getElementById("sender").style.display='none'; // hide submit button
            document.getElementById("sender2").style.display='block'; // Show link to error page
            localStorage.setItem("expiredPDP", "yes");
		}else{
			document.getElementById("sender").style.display='block'; // hide submit button
            document.getElementById("sender2").style.display='none'; // Show link to error page
			localStorage.removeItem("expiredPDP");
		}



    }





function validateRSAidnumber() {

    $('#debug').empty();
    //document.getElementById("sender").style.display='block';
if(document.getElementById("sender2").style.display == 'none')  
{
            document.getElementById("sender").style.display='block';
}

    var idnumber = $('#idNum').val(),
        invalid = 0;

    // get the gender from the id number
    var genderCode = idnumber.substring(6, 10);
    var gender = parseInt(genderCode) < 5000 ? "Female" : "Male";
    var inputF = document.getElementById("gender"); // gender input field id

    // display debugging
    var debug = $('#debug');

    // check that value submitted is a number
    if (isNaN(idnumber)) {

        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    }

    // check length of 13 digits
    if (idnumber.length != 13) {
        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    }

    // check that YYMMDD group is a valid date
    var yy = idnumber.substring(0, 2),
        mm = idnumber.substring(2, 4),
        dd = idnumber.substring(4, 6);

    var dob = new Date(yy, (mm - 1), dd);

    // check values - add one to month because Date() uses 0-11 for months
    if (!(((dob.getFullYear() + '').substring(2, 4) == yy) && (dob.getMonth() == mm - 1) && (dob.getDate() == dd))) {
        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    }

    // evaluate GSSS group for gender and sequence
    //  var gender = parseInt(idnumber.substring(6, 10), 10) > 5000 ? "M" : "F";

    // ensure third to last digit is a 1 or a 0
    if (idnumber.substring(10, 11) > 1) {
        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    } else {
        // determine citizenship from third to last digit (C)
        var saffer = parseInt(idnumber.substring(10, 11), 10) === 0 ? "C" : "F";
    }

    // ensure second to last digit is a 8 or a 9
    if (idnumber.substring(11, 12) < 8) {
        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    }

    // calculate check bit (Z) using the Luhn algorithm
    var ncheck = 0,
        beven = false;

    for (var c = idnumber.length - 1; c >= 0; c--) {
        var cdigit = idnumber.charAt(c),
            ndigit = parseInt(cdigit, 10);

        if (beven) {
            if ((ndigit *= 2) > 9) ndigit -= 9;
        }

        ncheck += ndigit;
        beven = !beven;
    }

    if ((ncheck % 10) !== 0) {

        invalid++;
        document.getElementById("sender").style.display='none'; // hide submit button
        document.getElementById("gender").style.display='none'; // hide gender input field
    }

    // if one or more checks fail, display details
    if (invalid > 0) {
        debug.css('display', 'block');
        debug.append('Your ID Number is Invalid <br />');

    }


    // set value for the input field
    document.getElementById("gender").style.display='block'; // hide gender input field
    inputF.value = gender;
    //innerHTML = "value = " + "'" + inputF.value + "'";
    document.getElementById("gender").value = gender;

if(document.getElementById("idvalueCheck").value == '1')
{
 document.getElementById("sender").style.display='none';
}

    return (ncheck % 10) === 0;
}

$('#details').submit(validateRSAidnumber);



function mysubmiForm()
{
    var erroMessage = "";
    var correct = 0;
    var phoneno = /^\d{10}$/;
    var max_chars = 10;
    var inputtxt = document.getElementById("cellphone").value;
    if(inputtxt.match(phoneno)) {
        correct++;

    }
    else
    {
        erroMessage += "Cell phone number is incorrect!</br/>";
    }

    var number = /^[0-9a-zA-Z]+$/; // check for numbers and letters
    var max_chars = 12;
    //var mylicence = inputtxt.value;
    var inputtxt = document.getElementById("licenseNumber").value;
//alert(inputtxt.length);
    if(inputtxt.length === 12)
    {
        correct++;
    }
    else
    {
       erroMessage += "License number is incorrect!</br/>";
    }

    var expirydate = document.getElementById("expirydate").value;
	if(expirydate.length !== 0)
    {
        correct++;
    }
    else
    {
       erroMessage += "License number date is incorrect!</br/>";
    }

    var email = document.getElementById("email").value;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
             correct++;
            }
            else{
                erroMessage += "Email is incorrect!</br/>";

            }
	var pnpexpiry = document.getElementById("pnpexpiry").value;		
	if(pnpexpiry.length !== 0)
    {
        correct++;
    }
    else
    {
       erroMessage += "Pdp number is incorrect!</br/>";
    }
    
	
	
	

    if(correct === 5)
    {
//alert("show me correct is correct is "+correct);

     document.getElementById("registerForm").submit();
    }
    else
    {
        document.getElementById("error_message").innerHTML = erroMessage;
    } 
    document.getElementById("sender").disabled=false;
    document.getElementById("sender").value="Proceed";

}
function checkForDuplicateId(sel)
{
	var xhr = new XMLHttpRequest(); 
    var data = new FormData();
    var idNum = sel.value;	
	data.append('idNum', idNum);
	xhr.open('POST', 'idcheck.php', true);
    xhr.onprogress = function () {
    };
    xhr.onload = function () {
		if(this.responseText.length === 9)
		{
		
		  document.getElementById("idvalueCheck").value = 1;
		  document.getElementById("error_message").innerHTML = "<font color='red'>This id number has already been used to register a taxi associaction. Please contact us via WhatsApp to rectify.</font>"; 
		  /*
		  document.getElementById("surname").readOnly = true;
		  document.getElementById("fullName").readOnly = true;
		  document.getElementById("cellphone").readOnly = true;
		  document.getElementById("email").readOnly = true;
		  */
		}
		else
		{
		  document.getElementById("idvalueCheck").value = 0;
		  document.getElementById("error_message").innerHTML = "";
		  
		  /*
		  document.getElementById("surname").readOnly = false;
		  document.getElementById("fullName").readOnly = false;
		  document.getElementById("cellphone").readOnly = false;
		  document.getElementById("email").readOnly = false;
		  */
		}
       	
	   
    };
        xhr.send(data);
}
function exp()
{
location.href = "expired.php";
}
</script>
    </body>
</html>
