<!DOCTYPE HTML>
<html>
    <head>
        <!--Google Tag Manager-->
        <script src="googleAnalytics.js"></script>
        <!-- End Google Tag Manager -->    
        <meta charset="utf-8">
        <title>Terms & Conditions</title>
<link rel="apple-touch-icon" href="images/icons/icon-512x512.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icons/icon-152x152.png">
<link rel="apple-touch-icon" sizes="192x192" href="images/icons/icon-192x192.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icons/icon-144x144.png">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="css/header.css" type="text/css" />
<style>
ol { counter-reset: item; font-family: Helvetica; }
li{ display: block; font-family: Helvetica; }
li:before { content: counters(item, ".") " "; counter-increment: item }
</style>
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZCQZJ5" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->    
        <header class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="back-nav"><a href="outstanding_matter.php?mypayatnow=<?php
                            echo $_GET['mypayatnow'];
                            ?>&number=<?php
                            echo $_GET['number']; ?>">
                            <img src="nav.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    <div class = "container stretch">
		<h1 class="text-center terms" style='font-family: Helvetica;'>TERMS AND CONDITIONS</h1>
        <article class="row">
        	<section class="col-md-12 col-xs-12">
<hr/>
                <p style='font-family: Helvetica;'>
                YOUR ATTENTION IS DRAWN TO THESE TERMS AND CONDITIONS AS THEY ARE IMPORTANT AND SHOULD BE CAREFULLY NOTED. USE OF THIS WEBSITE IS SUBJECT TO THESE TERMS AND CONDITIONS AS WELL AS THE PRIVACY POLICY PUBLISHED ON THIS WEBSITE. BY CONTINUING TO USE THIS WEBSITE YOU AGREE TO THESE TERMS AND CONDITIONS AND RELATED PRIVACY POLICY. 
           </p>
<hr/>
<p>
<ol>
  <li><b>INTRODUCTION</b> 
    <ol>
      <li>This website is owned and operated by Bligvest Global Business Services (Pty) Ltd a company duly registered and incorporated in terms of the company laws of the Republic of South Africa with registration number: 2019/225855/07 (hereinafter referred to as 'Bligvest GBS'). </li>
      <li>These Terms and Conditions apply to visitors and users of the Bligvest GBS website. Use of this website is made available subject to these Terms and Conditions as set out below, including, but not limited to policies, notices as well as any documents that are referred to in clauses set out in this website and Terms and Conditions.</li>
      <li>By using this website you agree to be bound by, and to comply with, these Terms and Conditions and any further Terms and Conditions that Bligvest GBS may prescribe from time to time. These Terms and Conditions may be changed in the future without notice, as such, it is your responsibility to keep up to date with any changes made to ensure that you still agree with the Terms and Conditions, as your continued use of this website will signify your acceptance of the updated and/or modified Terms and Conditions.
<ol>
	<li>Any new features or tools which are added to the current TRIPS portal shall also be subject to these Terms and Conditions.</li>
	</ol>
<ol>
	<li>The TRIPS portal is hosted on www.ticrf.co.za. The operator of the aforementioned website provides Bligvest GBS with an online e-commerce platform that allows Bligvest GBS to sell its products and services to you.</li>
	</ol>


 </li>
   <li>By using this website you acknowledge that you have read, and agree to be bound by these Terms and Conditions</li>
 <li>By agreeing to the Terms and Conditions of these Terms and Conditions you represent that you are reached the age of majority in your state or province of residence, alternatively, that being a major in your state or province of residence you hereby consent to allow your minor dependants to use this website. </li>
<li>For purposes of convenience clauses contained herein that are deemed to be important (clauses that may limit our responsibility or involve risk for you) will be in bold and/or italics and/or highlighted, as such, special attention is required for such clauses.  </li>
<li>This website is offered to you under the provision that you agree to use the website only in line with these Terms and Conditions and any applicable laws, rules and regulations.</li>
<li><b>These Terms and Conditions form an agreement between you and Bligvest GBS, it is therefore your prerogative to ensure that you understand all provisions contained herein.</b></li>
<li><b>By continuing to use this website you agree to accept, without modification, limitation or qualification these Terms and Conditions. </b></li>

<li><b>In terms of section 11 of the Electronic Communications and Transactions Act 25 of 2002 (�ECTA�) and the common law of contracts, these Terms and Conditions are valid, binding and enforceable against all persons that access this website.</b></li>

<li><b>Disclosures in terms of section 43 of the Electronic Communications and Transactions Act. 
<br/><br/>
The full name and legal status of the proprietor of this website is: 
</b>
<br><br>
Bligvest Global Business Services (Pty) Ltd <br/>
Registration number: 2019/225855/07 <br/>
<br/>
<b>The full address of the proprietor of the website is: </b>
<br><br>
Registered Address: 13 Lenin Street, Raceview, Alberton, Gauteng, 1450 <br/>
Postal Address: P.O Box 136368, Alberton North, Alberton, Gauteng, 1456  <br/>
</li>

</ol>
</li>
</ol>
</p>
            </section>
        </article>
	</div>
</body>
</html>
