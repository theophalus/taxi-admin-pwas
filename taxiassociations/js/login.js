$(document).ready(function()
{

	if('serviceWorker' in navigator) 
	{ 
		/*
		*
		* Register the Service Worker
		*
		* */
		navigator.serviceWorker.register('sw2.js').then(function(registration) 
		{
			registration.update();
			registration.installing; // the installing worker, or undefined
			registration.waiting; // the waiting worker, or undefined
			registration.active; // the active worker, or undefine
			console.log('Service Worker Registered2');
		});
	
	
	} else console.log('Your browser does not support the Service-Worker!');

		if(localStorage.getItem("Mysession")) // Check if user Already LoggedIn / session variable created
		{
			window.location.href = "profile.html"; // 	if Session created redirect to Profile page
		}


		

}); 


function checkIdFormat() {
	var id = document.getElementById("idNum").value;
	if(isNaN(id))
	{
		document.getElementById("error_message").innerHTML = "<span style='color:red'>Only Number Please!</span>";
	}
	
}
/*

	Authentification code function 

*/
function Auhtenticater_func()
{
	document.getElementById("error_message").innerHTML = "<img src='loading.gif' width='70' />"; // insert loader gif
	/*
		Prepare post information to send with form data send Id Number
	*/
	var data = new FormData(); // instanciate Form data
	var idNumber = $("#idNum").val(); // Get I.D Number value form field input into variable
	data.append('id', idNumber); // Bind id nunmer to Form data 
	/*
		Ajaxs calls
	*/
	var xhr = new XMLHttpRequest(); // Instantiate Ajaxs XMLHttpRequest object 
	xhr.open('POST', 'authorize.php', true); // Intialise http post and insert post URL parameter
	xhr.onprogress = function() // while request in progress
	{
		document.getElementById("error_message").innerHTML = "<img src='loading.gif' width='70' />"; // show loader while in progress
	};
	xhr.onload = function() // on request success return
	{
		document.getElementById("error_message").innerHTML = ""; // remove loader image
		let obj = JSON.parse(this.responseText); // get gack response Json and turn to object
		let authorize_checker = obj.checker; // get checker parameter from json/object 
		let authorize_checker_size = authorize_checker.length; // get length of checker
		if(authorize_checker_size == 5) // check length to 5 character which is the word found
		{
			localStorage.setItem("Mysession", this.responseText); // If checker length good register object in session variable
			window.location.href = "profile.html"; // Redirect page to Profile
		}
		else
		{
			document.getElementById("error_message").innerHTML = "<span style='color:red'>authentication failed</span>"; // If Fail replace Loader with Failed message
		} 
	};
	xhr.send(data);
}