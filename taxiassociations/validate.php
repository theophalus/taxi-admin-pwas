<?php
    // db connection
    $host = 'localhost';
    $db   = 'taxi';
    $user = 'root';
    $pass = 'k4k3fBk%bdJS20';
    $charset = 'utf8mb4';

    $options = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    try {
        $pdo = new \PDO($dsn, $user, $pass, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }


    // check if the ID ha been registered
    if(!empty($_POST["idnum"])) {
        $idnum = $_POST["idnum"];
        $stmt = $pdo->prepare("SELECT * FROM taxi_users WHERE idnum=?");
        $stmt->execute([$idnum]);
        //$userCount = $stmt->fetch();
        $userCount = $stmt->rowCount();

        if($userCount>0) {
            echo "<span class='status-not-available red-text'>ID Number: '$idnum' is already registered.</span>";
        }else{
            echo " ";
        }
    }
