<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('logout', 'HomeController@logout')->name('logout');
/*
 * ----------------------------------------------------------------------------
 * ADMIN USERS
 * ----------------------------------------------------------------------------
 * */
Route::get('/home/edit_user/{id}', 'HomeController@edit_user')->name('edit_user');
Route::post('/home/update_user/{id}', 'HomeController@update_user')->name('update_user');
Route::get('/taxiMain', 'TaxiController@main')->name('taxiMain');
Route::get('/users', 'HomeController@users')->name('users');
Route::post('/decline_reason', 'TaxiController@decline_reason')->name('decline_reason');
Route::post('/create_user', 'HomeController@create_user')->name('create_user');
Route::get('/delete_user/{id}', 'HomeController@delete_user')->name('delete_user');
Route::get('/user_details/{id}', 'HomeController@user_details')->name('user_details');



    /*
     * ----------------------------------------------------------------------------
     * AUDIT TRAIL
     * ----------------------------------------------------------------------------
     * */
Route::get('/audit', 'HomeController@audit')->name('audit');
Route::get('/auditList', 'HomeController@auditList')->name('auditList');
Route::get('/auditListAssoc', 'HomeController@auditList')->name('auditListAssoc');
Route::get('/auditListClaim', 'HomeController@auditList')->name('auditListClaim');



    /*
     * ----------------------------------------------------------------------------
     * REPORTING
     * ----------------------------------------------------------------------------
     * */
    Route::get('/report', 'ClaimentController@report')->name('report');
    Route::get('/reportList', 'ClaimentController@reportList')->name('reportList');
    Route::get('/rejectedListPayments', 'ClaimentController@reportList')->name('rejectedListPayments');


/*
 * ----------------------------------------------------------------------------
 * TAXI USERS
 * ----------------------------------------------------------------------------
 * */
Route::get('/taxi_users', 'HomeController@taxi_users')->name('taxi_users');
Route::get('/delete_taxi_user/{id}', 'HomeController@delete_taxi_user')->name('delete_taxi_user');
Route::get('/taxi_user_details/{id}', 'HomeController@taxi_user_details')->name('taxi_user_details');
Route::post('/home/update_taxi_user/{id}', 'HomeController@update_taxi_user')->name('update_taxi_user');
Route::post('/home/edit_taxi_user/{id}', 'HomeController@edit_taxi_user')->name('edit_taxi_user');

/*
 * ----------------------------------------------------------------------------
 * TAXI ASSOCIATIONS
 * ----------------------------------------------------------------------------
 * */
Route::get('/taxi_assocs_home', 'HomeController@taxi_assocs_home')->name('taxi_assocs_home');

/*
 * ----------------------------------------------------------------------------
 * TAXI ASSOCIATIONS
 * ----------------------------------------------------------------------------
 * */
Route::get('/taxi_associations', 'HomeController@taxi_associations')->name('taxi_associations');
Route::get('/taxi_assocs', 'HomeController@taxi_assocs')->name('taxi_assocs');
Route::get('/home/edit_taxi_assoc/{id}', 'HomeController@edit_taxi_assoc')->name('edit_taxi_assoc');
Route::post('/home/update_taxi_assoc/{id}', 'HomeController@update_taxi_assoc')->name('update_taxi_assoc');
Route::get('/delete_taxi_assoc/{id}', 'HomeController@delete_taxi_assoc')->name('delete_taxi_assoc');
Route::get('/taxi_assoc_details/{id}', 'HomeController@taxi_assoc_details')->name('taxi_assoc_details');
Route::post('/taxi_assoc_approval_status/{id}', 'HomeController@taxi_assoc_approval_status')->name('taxi_assoc_approval_status');

Route::get('/taxi_assoc_edit_approval_status/{id}', 'HomeController@taxi_assoc_edit_approval_status')->name('taxi_assoc_edit_approval_status');

Route::get('/registers', 'HomeController@registers')->name('registers');



Route::get('/approval_status/{id}', 'HomeController@approval_status')->name('approval_status');

Route::get('/payments', 'HomeController@payments')->name('payments');
Route::get('/transactions', 'HomeController@transactions')->name('transactions');
Route::get('/taxiDetails/{id}', 'TaxiController@index')->name('taxi_assocs');

/*
 * ----------------------------------------------------------------------------
 * CLAIMANTS
 * ----------------------------------------------------------------------------
 * */
Route::get('/claimants', 'HomeController@claimants')->name('claimants');
Route::get('/claimant_list', 'HomeController@claimant_list')->name('claimant_list');
Route::get('/claimant_approved', 'HomeController@claimant_approved')->name('claimant_approved');
Route::get('/claimant_pending', 'HomeController@claimant_pending')->name('claimant_pending');
Route::get('/claimant_declined', 'HomeController@claimant_declined')->name('claimant_declined');

Route::get('/claimant_approved_detail/{id}', 'HomeController@claimant_approved_detail')->name('claimant_approved_detail');
Route::get('/claimant_pending_detail/{id}', 'HomeController@claimant_pending_detail')->name('claimant_pending_detail');
Route::get('/claimant_declined_detail/{id}', 'HomeController@claimant_declined_detail')->name('claimant_declined_detail');
Route::post('/claimant_approved_update/{id}', 'HomeController@claimant_approved_update')->name('claimant_approved_update');

Route::post('/claimant_approved_profile/{id}', 'HomeController@claimant_approved_profile')->name('claimant_approved_profile');

/*
 * ----------------------------------------------------------------------------
 * QUERY MANAGER
 * ----------------------------------------------------------------------------
 * */
Route::get('/query_management', 'HomeController@query_management')->name('query_management');

/*
 * ----------------------------------------------------------------------------
 * FUND ADMINISTRATOR
 * ----------------------------------------------------------------------------
 * */
Route::get('/fund_administrator', 'HomeController@fund_administrator')->name('fund_administrator');

/*
 * ----------------------------------------------------------------------------
 * USER MANAGEMENT
 * ----------------------------------------------------------------------------
 * */
Route::get('/user_management', 'HomeController@user_management')->name('user_management');
Route::get('/taxiApproved', 'TaxiController@approved')->name('taxiApproved');
//Route::resource('user_management', 'UserController');
//Route::resource('user_management', 'RoleController');
//Route::resource('user_management', 'PermissionController');


Route::get('/taxiProfile/{id}', 'TaxiController@profile')->name('taxiProfiles');
Route::get('/deprove/{id}', 'TaxiController@deprove')->name('deprove');
Route::get('/reprove/{id}', 'TaxiController@reprove')->name('reprove');

Route::get('/taxiPending', 'TaxiController@pending')->name('taxiPending');
Route::get('/taxiDeclined', 'TaxiController@declined')->name('taxiDeclined');

/*
 * ----------------------------------------------------------------------------
 * ACCESS DENIED
 * ----------------------------------------------------------------------------
 * */
Route::get('/access_denied', 'HomeController@access_denied')->name('access_denied');





Route::get('/otpLogin', 'LogsinController@otpLogin')->name('otpLogin');Route::post('/otpLogin', 'LogsinController@otpLogin')->name('otpLogin');Route::get('/idLogin', 'LogsinController@idLogin')->name('idLogin');Route::post('/idLogin', 'LogsinController@idLogin')->name('idLogin');Route::get('/claimantlogin', 'LogsinController@index')->name('claimantlogin');Route::get('/claimantHalfDecline/{id}', 'HomeController@claimantHalfDecline')->name('claimantHalfDecline');Route::get('/claimantFullDecline/{id}', 'HomeController@claimantFullDecline')->name('claimantFullDecline');Route::get('/claimantHalfApproved/{id}', 'HomeController@claimantHalfApproved')->name('claimantHalfApproved');Route::get('/claimant_half_declined_detail/{id}', 'HomeController@claimant_half_declined_detail')->name('claimant_half_declined_detail');Route::get('/claimant_full_declined_detail/{id}', 'HomeController@claimant_full_declined_detail')->name('claimant_full_declined_detail');/*
 * ----------------------------------------------------------------------------
 * QUERY MANAGEMENT AGENT
 * ----------------------------------------------------------------------------
 * */
Route::get('/view_list_associations', 'HomeController@view_list_associations')->name('view_list_associations');
Route::get('/view_pending_associations', 'HomeController@view_pending_associations')->name('view_pending_associations'); 
Route::get('/claimantFullApproved/{id}', 'HomeController@claimantFullApproved')->name('claimantFullApproved');     
Route::get('/view_approved_associations', 'HomeController@view_approved_associations')->name('view_approved_associations');Route::get('/claimant_half_approved_detail/{id}', 'HomeController@claimant_half_approved_detail')->name('claimant_half_approved_detail');Route::get('/claimantsMainList', 'ClaimentController@claimantsMainList')->name('claimantsMainList');Route::get('/claimantsMain', 'ClaimentController@claimantsMain')->name('claimantsMain');Route::get('/claimant_full_approved/{id}', 'HomeController@claimant_full_approved')->name('claimant_full_approved');
Route::get('/view_declined_associations', 'HomeController@view_declined_associations')->name('view_declined_associations');



Route::get('/claimant_list_half_approved', 'HomeController@claimant_list_half_approved')->name('claimant_list_half_approved');
Route::get('/claimant_list_full_approved', 'HomeController@claimant_list_full_approved')->name('claimant_list_full_approved'); 
Route::get('/claimant_list_pending', 'HomeController@claimant_list_pending')->name('claimant_list_pending');  
Route::get('/claimant_list_half_declined', 'HomeController@claimant_list_half_declined')->name('claimant_list_half_declined');
Route::get('/claimant_list_full_declined', 'HomeController@claimant_list_full_declined')->name('claimant_list_full_declined');
Route::get('/claimantsMainHAList', 'ClaimentController@claimantsMainHAList')->name('claimantsMainHAList');
Route::get('/claimantsMainFAList', 'ClaimentController@claimantsMainFAList')->name('claimantsMainFAList');
Route::get('/claimantsMainHDList', 'ClaimentController@claimantsMainHDList')->name('claimantsMainHDList');
Route::get('/claimantsMainHFList', 'ClaimentController@claimantsMainHFList')->name('claimantsMainHFList');
Route::get('/claimantsMainPPList', 'ClaimentController@claimantsMainPPList')->name('claimantsMainPPList'); 






Route::get('/incompleteTaxi', 'TaxiController@incompleteTaxi')->name('incompleteTaxi');
 Route::get('/paymentHome', 'PaymentController@index')->name('paymentHome');
Route::get('/import', 'PaymentController@import')->name('import');
 Route::post('/importTable', 'PaymentController@importTable')->name('importTable'); Route::get('/importTables', 'PaymentController@importTable')->name('importTables');
Route::post('/importTableinsert', 'PaymentController@importTableinsert')->name('importTableinsert'); 
Route::get('/importTableTest/{id}', 'PaymentController@importTableTest')->name('importTableTest');  
Route::get('/batchlist', 'PaymentController@batchlist')->name('batchlist'); 
Route::get('/viewBatch/{id}', 'PaymentController@viewBatch')->name('viewBatch');
Route::get('/importForm', 'PaymentController@importForm')->name('importForm');
Route::post('/importFormFiles', 'PaymentController@importFormFiles')->name('importFormFiles');
Route::get('/getImportlist', 'PaymentController@getImportlist')->name('getImportlist');
Route::get('/importPay', 'PaymentController@importPay')->name('importPay');
Route::get('/downloadExcel/{id}', 'PaymentController@downloadExcel')->name('downloadExcel');
Route::get('/updateImportStatus/{id}', 'PaymentController@updateImportStatus')->name('updateImportStatus');
Route::get('/viewImportBatch/{id}', 'PaymentController@viewImportBatch')->name('viewImportBatch');
Route::get('/getNumRec', 'PaymentController@getNumRec')->name('getNumRec');
Route::get('/deleteImport/{id}', 'PaymentController@deleteImport')->name('deleteImport');