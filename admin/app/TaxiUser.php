<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiUser extends Model
{
    protected $fillable = [
        'user_id', 'idNum', 'name', 'surname',
        'cell', 'email', 'gender', 'type'
    ];

    protected $casts = [
        'pnrLicenseExpiryDate' => 'datetime',
        'licenseExpiryDate' => 'datetime',
    ];
}
