<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claimant extends Model
{
    protected $fillable = [
        'taxi_assoc_id', 'title', 'first_name', 'lastname_name', 'email', 'cellphone_number', 'gender',
        'license_number', 'pnr_number', 'id_validity_status'
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
