<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claimant extends Model
{
    protected $fillable = [
        'date','section', 'action', 'userId'
    ];


}
