<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

//Importing laravel-permission models
use Spatie\Permission\Models\Permission;
//use App\ModelHasPermission;
use Spatie\Permission\Models\Role;
//se App\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
session_start();
        $myUser = Auth::user()->id;
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;

         // add login logs to audit db
        $section = 'Login / Logout';
        $action = 'Logged in';
	if($roleId != "3")
	{
        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' ('.$role.')',
            'userId' => Auth::user()->id,
            'full_name' => $user[0]->first_name." ".$user[0]->last_name,
        ]);
	}
	else
	{
	   DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' ('.$_SESSION['type'].' - '.$_SESSION['taxiName'].')',
            'userId' => Auth::user()->id,
            'full_name' => $_SESSION['taxiMan'],
        ]);
	}





        $claimants = Claimant::orderBy('id', 'desc')->get();
        $beneficiaries = TaxiBeneficiary::orderBy('id', 'desc')->get();
        $members = TaxiAssociationMember::orderBy('id', 'desc')->get();
        $users = User::orderBy('id', 'desc')->get();
        $taxi_assoc_count = TaxiAssoc::count();
		$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $taxi_assoc_count = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' ");
        $taxi_assoc_count = $taxi_assoc_count[0]->mycount;
        $claimant_count = Claimant::count();

//print_r("<pre>");var_dump($_SESSION);die();
//var_dump($request->session()->all()); die();
if(isset($_SESSION['taxiMan'])){ 
return redirect()->route('claimantsMain');
$taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        $approved_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 1)
            ->count();
        $pending_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 0)
            ->count();
        $declined_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 2)
            ->count();
        $declined_half_count = DB::table('claimants')
            ->where('application_status', '=', 3)
            ->count();
$declined_full_count = DB::table('claimants')
            ->where('application_status', '=', 4)
            ->count();
       $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('claimants', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'getRolesId' => $getRolesId,
            'approved_claimant_count' => $approved_claimant_count,
            'pending_claimant_count' => $pending_claimant_count,
            'declined_claimant_count' => $declined_claimant_count,
            'declined_half_count' => $declined_half_count,
            'declined_full_count' => $declined_full_count
        ]);
}
else{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
if($getRolesId == 5)
{
return redirect()->route('paymentHome');
}
else
{
return view('home', [
            'claimants' => $claimants,
            'members' => $members,
            'beneficiaries' => $beneficiaries,
            'users' => $users,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
			 'getRolesId' => $getRolesId
        ]);
}
        
}
    }

    public function users()
    {
        //$users = User::orderBy('id', 'desc')->get();
$users = DB::select("SELECT users.id as currentUserId, users.*, dummy_permissions.*
FROM users
INNER JOIN dummy_permissions ON users.id = dummy_permissions.userId order by users.id desc");


        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;



        return view('users', [
            'users' => $users,
            'getRolesId' => $getRolesId
        ]);
    }
    /*
     * ----------------------------------------------------
     *          ADMIN USERS
     * ----------------------------------------------------
     * */
    public function create_user(Request $request)
    {
//echo "here"; die();
        //validate data
        $myvalidate = $this->validate($request, [
            'role_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
        ]);
        $form_data = array(
            'first_name' =>   $request->first_name,
            'last_name' =>   $request->last_name,
            'email' =>   $request->email,
            'password' => Hash::make($request->password),
            'role_id' => $request->role_id
        );

        $user = User::create($form_data);



     // add login logs to audit db
        $section = 'User Management';
        $action = 'Created a new user - '. $request->email;

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;



        DB::table('dummy_permissions')->insert([
            'id' => null,
            'userId' => $user->id,
            'roleId' => $request->role_id
        ]);


        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");

        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' (Created by '.$role.')',
            'userId' => Auth::user()->id,
            'full_name' => $user[0]->first_name." ".$user[0]->last_name,
        ]);



        //store status message
        return redirect()->route('user_management')->with('message','User created successfully!');
/*if($myvalidate)
{
return redirect()->route('users');
} 
*/
   
        //redirect back to the main page
    /*  if($myvalidate)
{
return redirect()->route('users');
} */
        //return back()->withInput();
    }

    public function delete_user($id){
        //update post data
        //User::find($id)->delete();
        $shodata = DB::table('users')->where('id', '=', $id)->delete();
        DB::table('dummy_permissions')->where('userId', '=', $id)->delete();
        //store status message

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;

        // add login logs to audit db
        $section = 'User Management';
        $action = 'Deleted a user';
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' (Deleted by '.$role.')',
            'userId' => Auth::user()->id,
            'full_name' => $user[0]->first_name." ".$user[0]->last_name,
        ]);

//var_dump($shodata); die();
        Session::flash('success_msg', 'User deleted successfully!');







        return redirect()->route('users');
    }

    public function user_details($id){
        //fetch user data
        $user = User::find($id);

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;



        //pass user data to view and load list view
        return view('user_details', [
            'user' => $user,
            'getRolesId' => $getRolesId
        ]);
    }

    public function edit_user($id){
$myUser = Auth::user()->id;
        $getRoless = DB::select("select * from dummy_permissions where userId = $myUser");
$getRolesIds =  $getRoless[0]->roleId;  
$user = User::find($id);
$id = (int) $id;
$getRoles = DB::select("select * from dummy_permissions where userId = $id");
//print_r("<pre>");var_dump($getRoles);die();
        $getRolesId = $getRoles[0]->roleId;

        //load form view
        return view('edit_user', ['user' => $user, 'getRolesId' => $getRolesId, 'getRolesIds' => $getRolesIds ]);

    }

    public function update_user($id, Request $request){
        //validate post data
//print_r("<pre>"); var_dump($userData); die();
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required', 
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        //get post data
$_POST['password'] = Hash::make($_POST['password']);
unset($_POST['password_confirmation']);

$userData = $_POST;
        //$userData = $request->all();
//var_dump($userData); die();
        //update post data
        User::find($id)->update($userData);
        DB::table('dummy_permissions')
            ->where('userId', $id)
            ->update([
                'roleId' => $_POST['role_id']
                ]);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');




        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;

        // add login logs to audit db
        $section = 'User Management';
        $action = 'Updated User Record ' . $_POST['email'];
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' (Updated by '.$role.')',
            'userId' => Auth::user()->id,
            'full_name' => $user[0]->first_name." ".$user[0]->last_name,
        ]);



        return redirect()->route('home');
    }

    /*
     * ----------------------------------------------------
     *          TAXI USERS
     * ----------------------------------------------------
     * */
    public function taxi_users(){
        $taxi_users = TaxiUser::orderBy('id', 'desc')->get();
        $taxi_users = TaxiUser::paginate(15);
        $cnt = TaxiUser::count();
        return view('taxi_users', [
            'taxi_users' => $taxi_users,
            'cnt' => $cnt
        ]);
    }

    public function edit_taxi_user($id){
        //get post data by id
        $taxi_user = TaxiUser::find($id);

        //load form view
        return view('edit_taxi_user', ['taxi_user' => $taxi_user]);
    }

    public function update_taxi_user($id, Request $request){
        //validate post data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);

        //get post data
        $userData = $request->all();

        //update post data
        TaxiUser::find($id)->update($userData);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('home');
    }

    public function taxi_user_details($id){
        //fetch user data
        $taxi_user = TaxiUser::find($id);

        //pass user data to view and load list view
        return view('taxi_user_details', ['taxi_user' => $taxi_user]);
    }

    public function delete_taxi_user($id){
        //update post data
        TaxiUser::find($id)->delete();

        //store status message
        Session::flash('success_msg', 'User deleted successfully!');

        return redirect()->route('taxi_users');
    }

    /*
* ----------------------------------------------------------------------------
* TAXI ASSOCIATIONS HOME
* ----------------------------------------------------------------------------
* */
    public function taxi_assocs_home(){
session_start();
        $user = User::orderby('id', 'desc')->where('id', '=', '1')->get();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        $taxi_assoc_counts = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where astatus = '2' and bstatus = '2' ");
        $taxi_assoc_counts = $taxi_assoc_counts[0]->mycount;
        $taxi_assoc_counts = (integer) $taxi_assoc_counts;
$taxi_assoc_count = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where bstatus = '1' ");
        $taxi_assoc_count = $taxi_assoc_count[0]->mycount;
        $taxi_assoc_count = (integer) $taxi_assoc_count; 
        $taxi_assoc_count =  $taxi_assoc_counts +  $taxi_assoc_count;
        $member_counts = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 0 and bstatus = '2'");
        $member_counts = $member_counts[0]->mycount;
        $member_countss = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 1 and bstatus = '2'");
        $member_countss = $member_countss[0]->mycount;
        $member_countsss = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' and status = 2 and bstatus = '2'");
        $member_countsss = $member_countsss[0]->mycount;
        $member_countssss = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where bstatus = '1'");
        $member_countssss = $member_countssss[0]->mycount; 
        $claimant_count = Claimant::count();
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
if(($getRolesId === 2) || ($getRolesId === 1) || ($getRolesId === 4 ) || ($getRolesId === 5 ))
{
        return view('taxi_assocs_home', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'member_counts' => $member_counts,
            'member_countss' => $member_countss,
            'member_countsss' => $member_countsss,
            'member_countssss' => $member_countssss,
            'user' => $user,
'getRolesId' => $getRolesId
        ]);
}
else
{
return redirect()->route('home');
}
    }

    /*
 * ----------------------------------------------------------------------------
 * TAXI ASSOCIATIONS
 * ----------------------------------------------------------------------------
 * */

    public function taxi_assocs(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        return view('taxi_assocs', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count
        ]);
    }

    public function taxi_associations(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assoc_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $claimant_count = Claimant::count();
        return view('taxi_associations', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count
        ]);
    }

    public function delete_taxi_assoc($id){
        //update data
        TaxiAssoc::find($id)->delete();

        //store status message
        Session::flash('success_msg', 'User deleted successfully!');

        return redirect()->route('users');
    }

    public function taxi_assoc_details($id){
        //fetch user data
        $taxi_assoc = TaxiAssoc::find($id);

        //pass data to view and load list view
        return view('taxi_assoc_details', ['taxi_assoc' => $taxi_assoc]);
    }

    public function edit_taxi_assoc($id){
        //get data by id
        $taxi_assoc = TaxiAssoc::find($id);

        //load form view
        return view('edit_taxi_assoc', ['taxi_assoc' => $taxi_assoc]);
    }

    public function update_taxi_assoc($id, Request $request){
        //validate data
        $this->validate($request, [
            'name' => 'required',
            'province' => 'required',
            'town' => 'required'
        ]);

        //get data
        $taxi_assocData = $request->all();

        //update data
        TaxiAssoc::find($id)->update($taxi_assocData);

        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('taxi_assocs');
    }

    public function taxi_assoc_edit_approval_status($id){
        //get data by id
        $taxi_assoc = TaxiAssoc::find($id);

        //load form view
        return view('taxi_assoc_edit_approval_status', ['taxi_assoc' => $taxi_assoc]);
    }

    public function taxi_assoc_approval_status($id, Request $request){
        $this->validate($request, [
            'status_reason' => 'required',
            'status' => 'required',
        ]);

        // $taxi_assoc = TaxiAssoc::find($id);

        if(ApprovalStatus::find($id) == null){
            $form_data = array(
                'status_reason' =>   $request->status_reason,
                'status' =>   $request->status,
                'user_id'  =>  Auth::user()->id,
                'taxi_assoc_id' => $request->taxi_assoc_id
            );

            ApprovalStatus::create($form_data);

        }else{
            //get data
            $taxi_assocData = $request->all();

            //update data
            ApprovalStatus::find($id)->update($taxi_assocData);
        }


        //store status message
        Session::flash('success_msg', 'User updated successfully!');

        return redirect()->route('taxi_assocs');
    }


    /*
 * ----------------------------------------------------------------------------
 * STORE CLAIMANTS
 * ----------------------------------------------------------------------------
 * */
	public function claimantHalfDecline($id){
session_start();
//echo $_SESSION['type']; die();
if($_SESSION['type'] === "Chairperson")
{

DB::select("update claimants set application_status = 3, secretary_status = 1 where id = $id");

    // get claimant details
    $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
    $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;
    $cell = $ClaimantInfo[0]->cellphone_number;

    // add login logs to audit db
    $section = 'Claimants';
    $action = 'Chairperson Partially Declined a Claimant';
    $actionBy =  $_SESSION['taxiMan'] . ' (Chairperson)';


    DB::table('auditClaim')->insert([
        'id' => null,
        'date' => DB::raw('now()'),
        'section' => $section,
        'claimant' => $name,
        'association' => $_SESSION['taxiName'],
        'action' => $action,
        'userId' => $actionBy,
    ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Partially Declined",
            'person' => "Chairperson",
            'date' => DB::raw('now()'),
            'status' => '',
        ]);



}

if($_SESSION['type'] === "Secretary")
{

DB::select("update claimants set application_status = 3, secretary_status = 2 where id = $id");


    // get claimant details
    $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
    $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;
    $cell = $ClaimantInfo[0]->cellphone_number;

    // add login logs to audit db
    $section = 'Claimants';
    $action = 'Secretary Partially Declined a Claimant';
    $actionBy =  $_SESSION['taxiMan'] . ' (Secretary)';


    DB::table('auditClaim')->insert([
        'id' => null,
        'date' => DB::raw('now()'),
        'section' => $section,
        'claimant' => $name,
        'association' => $_SESSION['taxiName'],
        'action' => $action,
        'userId' => $actionBy
    ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Partially Declined",
            'person' => "Secretary",
            'date' => DB::raw('now()'),
            'status' => ''
        ]);


}

		

if(isset($_SESSION['taxiMan'])){
 return redirect()->route('claimantsMainList');
}
else
{
 return redirect()->route('claimant_list');
}
               
	}
	public function claimantFullDecline($id){
		DB::select("update claimants set application_status = 4 where id = $id");


        session_start();
        if(isset($_SESSION['taxiMan'])){

            // get claimant details
            $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
            $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;
            $cell = $ClaimantInfo[0]->cellphone_number;
            // send sms
            $url = 'http://99.80.158.197/sms/sendSMSNotice.php';
            $cellFormated = substr($cell, 1, 9);
            //Initiate cURL
            $curl = curl_init($url);
            $data = array(
                'cell' => $cellFormated,
                'sms' => 'You application has been declined by your taxi Association. Please contact your association for more information.');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_exec($curl);

            // add login logs to audit db
            $section = 'Claimants';
            $action = 'Fully Declined a Claimant';


            DB::table('auditClaim')->insert([
                'id' => null,
                'date' => DB::raw('now()'),
                'section' => $section,
                'association' => $_SESSION['taxiName'],
                'claimant' => $name,
                'action' => $action,
                'userId' => $_SESSION['taxiMan']." (".$_SESSION['type'].") ",
            ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Fully Declined",
            'person' => $_SESSION['type'],
            'date' => DB::raw('now()'),
            'status' => ''
        ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Sms sent to $cell You application has been declined by your taxi Association. Please contact your association for more information.",
            'person' => $_SESSION['type'],
            'date' => DB::raw('now()'),
            'status' => ''
        ]);
            return redirect()->route('claimantsMainList');
        }
        else
        {
            return redirect()->route('claimant_list');
        }




	}
	public function claimantHalfApproved($id){

session_start();
//echo $_SESSION['type']; die();
if($_SESSION['type'] === "Chairperson")
{

DB::select("update claimants set application_status = 1, secretary_status = 1 where id = $id");

// get claimant details
    $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
    $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;


    // add login logs to audit db
    $section = 'Claimants';
    $action = 'Chairperson Approved a Claimant';
    $actionBy =  $_SESSION['taxiMan'] . ' (Chairperson)';


    DB::table('auditClaim')->insert([
        'id' => null,
        'date' => DB::raw('now()'),
        'section' => $section,
        'claimant' => $name,
        'association' => $_SESSION['taxiName'],
        'action' => $action,
        'userId' => $actionBy
    ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Partially Approved",
            'person' => "Chairperson",
            'date' => DB::raw('now()'),
            'status' => ''
        ]);


}

if($_SESSION['type'] === "Secretary")
{

    DB::select("update claimants set application_status = 1, secretary_status = 2 where id = $id");
    // get claimant details
    $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
    $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;
    // add login logs to audit db
    $section = 'Claimants';
    $action = 'Secretary Approved a Claimant';
    $actionBy =  $_SESSION['taxiMan'] . ' (Secretary)';


    DB::table('auditClaim')->insert([
        'id' => null,
        'date' => DB::raw('now()'),
        'section' => $section,
        'claimant' => $name,
        'association' => $_SESSION['taxiName'],
        'action' => $action,
        'userId' => $actionBy
    ]);
DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Partially Approved",
            'person' => "Secretary",
            'date' => DB::raw('now()'),
            'status' => ''
        ]);

}


if(isset($_SESSION['taxiMan'])){
 return redirect()->route('claimantsMainList');
}
else
{
 return redirect()->route('claimant_list');
}
	}
	public function claimantFullApproved($id){
		DB::select("update claimants set application_status = 2 where id = $id");
                session_start();


        // get claimant details
        $ClaimantInfo =  DB::select("Select * From  claimants Where id = $id");
        $name = $ClaimantInfo[0]->first_name . ' '.$ClaimantInfo[0]->last_name;
        $cell = $ClaimantInfo[0]->cellphone_number;


        if(isset($_SESSION['taxiMan'])){

            // send sms
            $url = 'http://99.80.158.197/sms/sendSMSNotice.php';
            $cellFormated = substr($cell, 1, 9);
            //Initiate cURL
            $curl = curl_init($url);
            $data = array(
                'cell' => $cellFormated,
                'sms' => 'We are please to announce that your application has been approved by your Taxi Association.');
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_exec($curl);

            // add login logs to audit db
            $section = 'Claimants';
            $action = 'Fully Approved a Claimant';

            DB::table('auditClaim')->insert([
                'id' => null,
                'date' => DB::raw('now()'),
                'section' => $section,
                'claimant' => $name, 
                'association' => $_SESSION['taxiName'],
                'action' => $action,
                'userId' => $_SESSION['taxiMan']." (".$_SESSION['type'].") ",
            ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Fully Approved",
            'person' => $_SESSION['type'],
            'date' => DB::raw('now()'),
            'status' => ''
        ]);

DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $_SESSION['taxiMan'],
            'claimId' => $id,
            'type'  => "Sms sent to $cell We are please to announce that your application has been approved by your Taxi Association.",
            'person' => $_SESSION['type'],
            'date' => DB::raw('now()'),
            'status' => ''
        ]);
            return redirect()->route('claimantsMainList');
        }
        else
        {
            return redirect()->route('claimant_list');
        }





	}
    public function claimants(){
session_start();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        $approved_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 1)
            ->count();
        $pending_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 0)
            ->count();
        $declined_claimant_count = DB::table('claimants')
            ->where('application_status', '=', 2)
            ->count();
        $declined_half_count = DB::table('claimants')
            ->where('application_status', '=', 3)
            ->count();
$declined_full_count = DB::table('claimants')
            ->where('application_status', '=', 4)
            ->count();
       $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('claimants', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'getRolesId' => $getRolesId,
            'approved_claimant_count' => $approved_claimant_count,
            'pending_claimant_count' => $pending_claimant_count,
            'declined_claimant_count' => $declined_claimant_count,
            'declined_half_count' => $declined_half_count,
            'declined_full_count' => $declined_full_count
        ]);
    }

    public function claimant_list(){
//echo "hello"; die();
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('taxi_assocs.*', 'claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('taxi_assocs.name as taxiName', 'claimants.*', 'claimants.id as claimantId')
            ->get(); 

// var_dump($claimants); die();
        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "All Claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
    }







    public function claimant_list_half_approved(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'claimants.id as claimantId','taxi_assocs.name as taxiName')
 	    ->where('application_status', '=', 1)
            ->get(); 

        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();
$label = "Partially approved claimants";
        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();

        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
             'label' => $label
        ]);
    }









    public function claimant_list_full_approved(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'claimants.id as claimantId','taxi_assocs.name as taxiName')
 	    ->where('application_status', '=', 2)
            ->get(); 

        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Approved claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
    }



    public function claimant_list_pending(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'claimants.id as claimantId','taxi_assocs.name as taxiName')
 	    ->where('application_status', '=', 0)
            ->get(); 

        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Pending claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
             'label' => $label
        ]);
    }










    public function claimant_list_half_declined(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'claimants.id as claimantId','taxi_assocs.name as taxiName')
 	    ->where('application_status', '=', 3)
            ->get(); 

        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Partially Declined claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
             'label' => $label
        ]);
    }










    public function claimant_list_full_declined(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'claimants.id as claimantId', 'taxi_assocs.name', 'claimants.id as claimantId') 
                      ->get();

        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'claimants.id as claimantId','taxi_assocs.name as taxiName')
 	    ->where('application_status', '=', 4)
            ->get(); 

        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();
 
        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Fully Declined claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
    }









    public function claimant_approved(){
        $myUser = Auth::user()->id;
        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->get();
        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 1)
            ->get();
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;


        return view('claimant_approved', [
            'claimants' => $claimants,
            'approved' => $approved,
            'getRolesId' => $getRolesId
        ]);
    }

    public function claimant_pending(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->get();
        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();
        return view('claimant_pending', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'pending' => $pending
        ]);
    }

    public function claimant_declined(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->get();
        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
        return view('claimant_declined', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'declined' => $declined
        ]);
    }

    public function claimant_approved_detail($id){
        $approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.application_status as application_status', 'claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code',
                'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 1)
            ->where('claimants.id', '=', $id)
            ->get();

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;


        return view('claimant_approved_detail', [
            'approved' => $approved,
            'getRolesId' => $getRolesId
        ]);
    }

    public function claimant_approved_update($id, Request $request){

        //get post data
        $approvedData = $request->all();

        //update post data
        Claimant::find($id)->update($approvedData);

        return back();
    }

    public function claimant_pending_detail($id){
$getRType = DB::select("SELECT * FROM `claimants` where id =  $id");
if($getRType[0]->account_used === "Other")
{
$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'receiving_account.bank_account_number as account_number',
                'receiving_account.bank_name as bank_name', 'receiving_account.account_type as account_type', 'receiving_account.branch as branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 0)
            ->where('claimants.id', '=', $id)
            ->get();

}
else 
{
 $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 0)
            ->where('claimants.id', '=', $id)
            ->get();
}

        
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
$claimEvents =DB::select("select * from claimEvents where claimId =  $id");
//var_dump($data[0]->taxiassocId); die();
        return view('claimant_pending_detail', [
            'pending' => $pending,
            'getRolesId' => $getRolesId,
            'claimEvents' => $claimEvents,
        ]);
    }

    public function claimant_declined_detail($id){
        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code',
                'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 2)
            ->where('claimants.id', '=', $id)
            ->get();



$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('claimant_declined_detail', [
            'declined' => $declined,
            'getRolesId' => $getRolesId
        ]);
    }
public function claimant_half_approved_detail($id){
/*
        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 1)
            ->where('claimants.id', '=', $id)
            ->get();
*/
$getRType = DB::select("SELECT * FROM `claimants` where id =  $id");
if($getRType[0]->account_used === "Other")
{
$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'receiving_account.bank_account_number as account_number',
                'receiving_account.bank_name as bank_name', 'receiving_account.account_type as account_type', 'receiving_account.branch as branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 1)
            ->where('claimants.id', '=', $id)
            ->get();

}
else 
{
 $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 1)
            ->where('claimants.id', '=', $id)
            ->get();
}



$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
       $claimEvents =DB::select("select * from claimEvents where claimId =  $id");
        $getRolesId = $getRoles[0]->roleId;
        return view('claimant_pending_detail', [
            'pending' => $pending,
            'getRolesId' => $getRolesId,
            'claimEvents' => $claimEvents
        ]); 
    }
public function claimant_full_approved($id){
        /*$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 2)
            ->where('claimants.id', '=', $id)
            ->get();
*/


$getRType = DB::select("SELECT * FROM `claimants` where id =  $id");
if($getRType[0]->account_used === "Other")
{
$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'receiving_account.bank_account_number as account_number',
                'receiving_account.bank_name as bank_name', 'receiving_account.account_type as account_type', 'receiving_account.branch as branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 2)
            ->where('claimants.id', '=', $id)
            ->get();

}
else 
{
 $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 2)
            ->where('claimants.id', '=', $id)
            ->get();
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
$claimEvents =DB::select("select * from claimEvents where claimId =  $id");
        return view('claimant_pending_detail', [
            'pending' => $pending,
            'getRolesId' => $getRolesId,
            'claimEvents' => $claimEvents
        ]);
    }
public function claimant_half_declined_detail($id){
/*        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 3)
            ->where('claimants.id', '=', $id)
            ->get();
*/


$getRType = DB::select("SELECT * FROM `claimants` where id =  $id");
if($getRType[0]->account_used === "Other")
{
$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'receiving_account.bank_account_number as account_number',
                'receiving_account.bank_name as bank_name', 'receiving_account.account_type as account_type', 'receiving_account.branch as branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 3)
            ->where('claimants.id', '=', $id)
            ->get();

}
else 
{
 $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 3)
            ->where('claimants.id', '=', $id)
            ->get();
}






$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
$claimEvents =DB::select("select * from claimEvents where claimId =  $id");
//var_dump($claimEvents); die();
        return view('claimant_pending_detail', [
            'pending' => $pending,
            'getRolesId' => $getRolesId,
            'claimEvents' => $claimEvents
        ]);
    }


public function claimant_full_declined_detail($id){
      /*  $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
            ->where('claimants.application_status', '=', 4)
            ->where('claimants.id', '=', $id)
            ->get();
*/

$getRType = DB::select("SELECT * FROM `claimants` where id =  $id");
if($getRType[0]->account_used === "Other")
{
$pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'receiving_account.bank_account_number as account_number',
                'receiving_account.bank_name as bank_name', 'receiving_account.account_type as account_type', 'receiving_account.branch as branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 4)
            ->where('claimants.id', '=', $id)
            ->get();

}
else 
{
 $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->leftJoin('bank_accounts', 'bank_accounts.claimant_id', '=', 'claimants.id')
            ->leftJoin('receiving_account', 'receiving_account.claimant_id', '=', 'claimants.id')
            ->select('claimants.*', 'claimants.id as claimantId','claimants.account_used', 'taxi_assocs.name', 'bank_accounts.account_number',
                'bank_accounts.bank_name', 'bank_accounts.account_type', 'bank_accounts.branch_code', 'receiving_account.receiver_name', 'receiving_account.receiver_surname', 'receiving_account.receiver_id_number',
                'receiving_account.receiver_email', 'receiving_account.cell', 'receiving_account.receiver_gender')
           // ->where('claimants.application_status', '=', 4)
            ->where('claimants.id', '=', $id)
            ->get();
}


$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
$claimEvents =DB::select("select * from claimEvents where claimId =  $id");
        return view('claimant_pending_detail', [
            'pending' => $pending,
            'getRolesId' => $getRolesId,
            'claimEvents' => $claimEvents
        ]);
    }

    public function claimant_detail(){
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->get();
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* QUERY MANAGEMENT
* ----------------------------------------------------------------------------
* */
    public function query_management(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
$taxi_assoc_count = DB::select("SELECT count(*) as mycount FROM `taxi_assocs` where `type` = 'Chairperson' ");
$taxi_assoc_count = $taxi_assoc_count[0]->mycount;
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('query_management', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
'getRolesId' => $getRolesId
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* FUND ADMINISTRATOR
* ----------------------------------------------------------------------------
* */
    public function fund_administrator(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;




        return view('fund_administrator', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'getRolesId' => $getRolesId
        ]);
    }

    /*
* ----------------------------------------------------------------------------
* USER MANAGEMENT
* ----------------------------------------------------------------------------
* */
    public function user_management(){
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = Claimant::count();
        //$roles = Role::orderBy('id', 'desc')->get();
        //$permissions = Permission::orderBy('id', 'desc')->get();
        $roles = Role::all(); //Get all roles
        $permissions = Permission::all();
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;

        return view('user_management', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'roles' => $roles,
            'permissions' => $permissions,
'getRolesId' => $getRolesId
        ]);

    }

    public function store_claimant(Request $request)
    {

        //validate data
        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required', 
            'cellphone_number' => 'required',
            'gender' => 'required',
            'license_number' => 'required',
            'pnr_number' => 'required',
            'id_validity_status' => 'required',
        ]);

        $form_data = array(
            'title'          =>   $request->title,
            'first_name'       =>   $request->first_name,
            'last_name' =>   $request->last_name,
            'email'           =>   $request->email,
            'cellphone_number' => $request->cellphone_number,
            'gender' => $request->gender,
            'license_number' => $request->license_number,
            'pnr_number' => $request->pnr_number,
            'id_validity_status' => $request->id_validity_status
        );

        Claimant::create($form_data);

        //store status message
        Session::flash('success_msg', 'Folder added successfully!');

        //redirect back to the main page
        return back();
    }

    /*
 * ----------------------------------------------------------------------------
 * Audit Trail
 * ----------------------------------------------------------------------------
 * */

    // Audit Trail
    public function audit()
    {

        $audit = DB::select("SELECT 
        audit.id,
        audit.date,
        audit.section,
        audit.action,
        audit.userId,
        audit.full_name,
        users.first_name,
        users.last_name
        FROM audit Inner Join users On audit.userId = users.id Order by audit.id desc");

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;



        return view('audit', [
            'audit' => $audit,
            'getRolesId' => $getRolesId
        ]);
    }

public function report()
    {
        
        return view('report');



    }






public function reportList(Request $request)
    {

        $section = $request->input('section');
        $fromDate = $request->input('fromDate').' 00:00:00';
        $toDate = $request->input('toDate').' 23:59:59';


        if($section == 'Duplicate ID'){
        $paymentLists = DB::select("SELECT claimants.first_name, 
	claimants.last_name, 
	claimants.id_number, 
	claimants.title, 
	claimants.cellphone_number,  
	claimants.application_status,
        claimants.created_at
	FROM claimants 
	INNER JOIN receiving_account ON 
	claimants.id_number = receiving_account.receiver_id_number  
        Where claimants.created_at between '$fromDate' AND '$toDate'    
        Order by claimants.id desc");

            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;



         return view('rejectedListPayments', [
            'paymentLists' => $paymentLists,
             'getRolesId' => $getRolesId
        ]); 
    }


 

        if($section == 'Duplicate Bank Accounts'){
            $paymentLists = DB::select("SELECT claimants.first_name, 
	claimants.last_name, 
	claimants.id_number, 
	claimants.title, 
	claimants.cellphone_number,  
	claimants.application_status,
        claimants.created_at,
        receiving_account.*,
        bank_accounts.*
	FROM claimants 
	INNER JOIN bank_accounts ON 
	claimants.id = bank_accounts.claimant_Id
    INNER JOIN receiving_account ON 
    bank_accounts.account_number = receiving_account.bank_account_number
     Where claimants.created_at between '$fromDate' AND '$toDate'    
        Order by claimants.id desc
        ");

            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;



            return view('rejectedListPayments', [
                'paymentLists' => $paymentLists,
                'banker' => 'yes',
                'getRolesId' => $getRolesId
            ]);
        }


if($section == 'Rejected Payments'){
            $paymentLists = DB::select("SELECT 
        *
        FROM rejected_payments
        Where status = 'Success' and date between '$fromDate' AND '$toDate'  
        Order by id desc
        ");



    $myUser = Auth::user()->id;
    $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
    $getRolesId = $getRoles[0]->roleId;



            return view('rejectedListPayments', [
                'paymentLists' => $paymentLists,
                 'rejected' => "yes",
                'banker' => 'yes',
                'getRolesId' => $getRolesId
            ]);
        }

       /* $audit = DB::select("SELECT 
        audit.id,
        audit.date,
        audit.section,
        audit.action,
        audit.userId,
        audit.full_name,
        users.first_name,
        users.last_name
        FROM audit Inner Join users On audit.userId = users.id 
        Where audit.section = '$section'
        And audit.date between '$fromDate' AND '$toDate'     
        Order by audit.id desc");
        return view('auditList', [
            'audit' => $audit,
        ]);
*/
    }
















    public function auditList(Request $request)
    {

        $section = $request->input('section');
        $fromDate = $request->input('fromDate').'00:00:00';
        $toDate = $request->input('toDate').'23:59:59';


        if($section == 'Taxi Associations'){
        $audit = DB::select("SELECT 
        auditAssoc.id,
        auditAssoc.date,
        auditAssoc.section,
        auditAssoc.assoc,
        auditAssoc.action,
        auditAssoc.userId
        FROM auditAssoc  
        Where auditAssoc.date between '$fromDate' AND '$toDate'    
        Order by auditAssoc.id desc");

            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;

        return view('auditListAssoc', [
            'audit' => $audit,
            'getRolesId' => $getRolesId
        ]);
    }




        if($section == 'Claimants'){
            $audit = DB::select("SELECT 
        auditClaim.id,
        auditClaim.date,
        auditClaim.section,
        auditClaim.claimant,
        auditClaim.association,
        auditClaim.action,
        auditClaim.userId
        FROM auditClaim 
        Where auditClaim.section = '$section'
        And auditClaim.date between '$fromDate' AND '$toDate'    
        Order by auditClaim.id desc
        ");


            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;




            return view('auditListClaim', [
                'audit' => $audit,
                'getRolesId' => $getRolesId
            ]);
        }


if($section == 'Fund Administrator'){

            $audit = DB::select("SELECT 
        auditFunds.id,
        auditFunds.date,
        auditFunds.section,
        auditFunds.batch,
        auditFunds.action,
        auditFunds.userId
        FROM auditFunds
        Where auditFunds.section = '$section'
        And auditFunds.date between '$fromDate' AND '$toDate'    
        Order by auditFunds.id desc
        ");

    $myUser = Auth::user()->id;
    $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
    $getRolesId = $getRoles[0]->roleId;



            return view('auditListFund', [
                'audit' => $audit,
                'getRolesId' => $getRolesId

            ]);
        }

       /* $audit = DB::select("SELECT
        audit.id,
        audit.date,
        audit.section,
        audit.action,
        audit.userId,
        audit.full_name,
        users.first_name,
        users.last_name
        FROM audit Inner Join users On audit.userId = users.id 
        Where audit.section = '$section'
        And audit.date between '$fromDate' AND '$toDate'     
        Order by audit.id desc");*/


        $audit = DB::select("SELECT 
        audit.id,
        audit.date,
        audit.section,
        audit.action,
        audit.userId,
        audit.full_name
        FROM audit 
        Where audit.section = '$section'
        And audit.date between '$fromDate' AND '$toDate'     
        Order by audit.id desc");

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;



        return view('auditList', [
            'audit' => $audit,
            'getRolesId' => $getRolesId
        ]);
    }

    /*
    --------------------------------------------------------
        PWA USERS
    --------------------------------------------------------
    */

    public function registers(){
        $registers = Register::orderBy('id', 'desc')->get();
        $registers = Register::paginate(15);
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        return view('registers', [
            'registers' => $registers,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count
        ]);
    }

    public function taxi_members(){
        $taxi_members = TaxiMember::orderBy('id', 'desc')->get();
        return view('taxi_members', [
            'taxi_members' => $taxi_members
        ]);
    }

    public function approval_status($id){
        //get status data by id
        $register = Register::find($id);

        //load form view
        return view('approval_status', ['register' => $register]);
    }

    public function update_approval($id, Request $request)
    {
        //validate data
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required'
        ]);
    }

    public function payments(){
        $payments = Payment::orderBy('id', 'desc')->get();

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;




        return view('payments', [
            'payments' => $payments,
            'getRolesId' => $getRolesId
        ]);
    }

    public function transactions(){
        $transactions = BankAccount::orderBy('id', 'desc')->get();

        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;




        return view('transactions', [
            'transactions' => $transactions,
            'getRolesId' => $getRolesId
        ]);
    }

    public function errors(){
        return view('errors');
    } 

    public function view_list_associations(){
        $data = null;
        $countAr = count($data);
        return view('view_list_associations', [
            'countAr' => $countAr
        ]);
    }





public function logout(Request $request) {
$myUser = Auth::user()->id;
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;

         // add login logs to audit db
        $section = 'Login / Logout';
        $action = 'Logged out';
        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' ('.$role.')',
            'userId' => Auth::user()->id,
            'full_name' => $user[0]->first_name." ".$user[0]->last_name,
        ]);
  Auth::logout(); 
  return redirect('/login');
}

}

?>