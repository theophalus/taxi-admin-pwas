<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

class ClaimentController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['claimantsMain', 'claimantsMainList', 'claimantsMainHAList', 'claimantsMainFAList', 'claimantsMainHFList', 'claimantsMainPPList']]);
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    public function index()
    {
Auth::logout();
		$data = "nothng";
		return view('auth.claimId', ['data' => $data]);
	}
 
	public function idLogin(Request $request)
	{
Auth::logout();

		$idNumber = $request->idNumber;
		$datas = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_users.idNum = '$idNumber' and taxi_assocs.status = 1 order by taxi_assocs.date desc limit 0,1");
		if(!empty($datas))
		{
			$cell = $datas[0]->cell;
			$url = 'https://app.ticrf.co/sms/sendSMS.php';
			$cellFormated = substr($cell, 1, 9);
			//Initiate cURL
			$curl = curl_init($url); 
			$data = array(
				'cell' => $cellFormated
			);
			//Set the Content-Type to text/xml.
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$result = curl_exec($curl);
			$otp = $result;
                        $myIdNumber = $datas[0]->idNum;
                        DB::select("delete from claimOtp where idNum = '$myIdNumber'");
			DB::table('claimOtp')->insert(['id' => null,'idNum' => $myIdNumber, 'otp' => $otp]);
			return view('auth.claimOtp', ['idNum' => $datas[0]->idNum]);
		}
		else
		{
			return view('auth.claimId', ['idError' => 'wrong']);
		}

	}

 

	public function otpLogin(Request $request)
	{
$otp = $request->otp;
                $idNum = $request->idNum;
//print_r("<pre"); var_dump($_POST); die();

		
		$data = DB::select("SELECT * from claimOtp where idNum = '$idNum' and otp = $otp");
		//print_r("<pre"); var_dump($data); die();
		 
                 
		if(!empty($data))
		{
                 $datas = DB::select("SELECT taxi_assocs.name as taxiName, taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_users.idNum = '$idNum' and taxi_assocs.status = 1 order by taxi_assocs.date desc limit 0,1");
		session_start();
                
		$_SESSION['taxiMan'] = $datas[0]->personName." ".$datas[0]->surname;
                $_SESSION['taxiName'] = $datas[0]->taxiName;
                $_SESSION['province'] = $datas[0]->province;
                $_SESSION['town'] = $datas[0]->town;
                $_SESSION['type'] = $datas[0]->type;

if (Auth::attempt([ 'email' => 'taxiuser@gmail.com', 'password' => 'password'
])) {
            // Authentication passed...
           return redirect()->route('home');
        }

		//return view('auth.enterDen', ['idError' => 'wrong']);		
		}
               else
               {
Auth::logout();
			//return view('auth.claimOtpCheck', ['idNum' => $request->idNum]);
$data = "nothng";
		return view('auth.claimId', ['data' => $data]);
               }
 

	}

	public function enterDen()
	{
		session_start();

		$_SESSION['taxiMan'] = "yebo";
		return view('auth.enterDen', ['idError' => 'wrong']);

	}
        public function claimantsMainList() 
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "All Claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
/*
$label = "All Claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' = $label
        ]);
*/

}






     public function claimantsMainHAList()
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery and application_status = 1");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Partially approved claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
             'label' => $label
        ]);
}






public function report()
    {
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;

        return view('report',[
            'getRolesId' => $getRolesId
        ]);

    }











public function reportList(Request $request)
    {

        $section = $request->input('section');
        $fromDate = $request->input('fromDate').' 00:00:00';
        $toDate = $request->input('toDate').' 23:59:59';


        if($section == 'Duplicate ID'){
        $paymentLists = DB::select("SELECT claimants.first_name, 
	claimants.last_name, 
	claimants.id_number, 
	claimants.title, 
	claimants.cellphone_number,  
	claimants.application_status,
        claimants.created_at
	FROM claimants 
	INNER JOIN receiving_account ON 
	claimants.id_number = receiving_account.receiver_id_number  
        Where claimants.created_at between '$fromDate' AND '$toDate'    
        Order by claimants.id desc");
 $paymentLists = array();

            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;


         return view('rejectedListPayments', [
            'paymentLists' => $paymentLists,
             'getRolesId' => $getRolesId
        ]); 
    }


 

        if($section == 'Duplicate Bank Accounts'){
            $paymentLists = DB::select("SELECT claimants.first_name, 
	claimants.last_name, 
	claimants.id_number, 
	claimants.title, 
	claimants.cellphone_number,  
	claimants.application_status,
        claimants.created_at,
        receiving_account.*,
        bank_accounts.*
	FROM claimants 
	INNER JOIN bank_accounts ON 
	claimants.id = bank_accounts.claimant_Id
    INNER JOIN receiving_account ON 
    bank_accounts.account_number = receiving_account.bank_account_number
     Where claimants.created_at between '$fromDate' AND '$toDate'    
        Order by claimants.id desc
        ");
$accountNumber = array();
foreach($paymentLists as $data)
{
   $accountNumber[] = $data->bank_account_number;
}
$paymentLists = array();
foreach($accountNumber as $val)
{
$getClaimants = DB::select("SELECT *, claimants.first_name claimName FROM `claimants` inner JOIN bank_accounts ON claimants.id = bank_accounts.claimant_id where bank_accounts.account_number = '$val' ");
$paymentLists[] = $getClaimants[0];
$getClaimants = DB::select("SELECT *, claimants.first_name claimName , receiving_account.bank_account_number as account_number, CONCAT(receiving_account.receiver_name, ' ' , receiving_account.receiver_surname) AS  account_name FROM `claimants` inner JOIN receiving_account ON claimants.id = receiving_account.claimant_Id where receiving_account.bank_account_number = '$val' ");
$paymentLists[] = $getClaimants[0];    
 
}


            $myUser = Auth::user()->id;
            $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
            $getRolesId = $getRoles[0]->roleId;



 return view('rejectedListPayments', [
                'paymentLists' => $paymentLists,
                'banker' => 'yes',
                'getRolesId' => $getRolesId
            ]);
        }


if($section == 'Rejected Payments'){
            $paymentLists = DB::select("SELECT 
        *
        FROM rejected_payments
        Where status = 'Success' and date between '$fromDate' AND '$toDate'  
        Order by id desc
        ");

    $myUser = Auth::user()->id;
    $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
    $getRolesId = $getRoles[0]->roleId;



            return view('rejectedListPayments', [
                'paymentLists' => $paymentLists,
                 'rejected' => "yes",
                'banker' => 'yes',
                'getRolesId' => $getRolesId
            ]);
        }

       /* $audit = DB::select("SELECT 
        audit.id,
        audit.date,
        audit.section,
        audit.action,
        audit.userId,
        audit.full_name,
        users.first_name,
        users.last_name
        FROM audit Inner Join users On audit.userId = users.id 
        Where audit.section = '$section'
        And audit.date between '$fromDate' AND '$toDate'     
        Order by audit.id desc");
        return view('auditList', [
            'audit' => $audit,
        ]);
*/
    }




















public function claimantsMainFAList()
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery and application_status = 2");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();

$label = "Approved claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
}









public function claimantsMainHDList()
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery and application_status = 3");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Partially Declined claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
}











public function claimantsMainHFList()
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery and application_status = 4");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Fully Declined claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
}

















public function claimantsMainPPList()
	{
session_start();
if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $claimants1[] = DB::table('claimants')
                      ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
                      ->select('claimants.*', 'taxi_assocs.name')
                      ->get();

$name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "claimants.taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                } 
           $claimants = DB::select("SELECT taxi_assocs.name as taxiName, claimants.id as claimantId, claimants.*, taxi_assocs.* FROM claimants INNER JOIN taxi_assocs ON claimants.taxi_assoc_id = taxi_assocs.id $addQuery and application_status = 0");
	
$approved = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('claimants.application_status', '=', 1)
            ->get();

        $pending = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 0)
            ->get();

        $declined = DB::table('claimants')
            ->leftJoin('taxi_assocs', 'claimants.taxi_assoc_id', '=', 'taxi_assocs.id')
            ->select('claimants.*', 'taxi_assocs.name')
            ->where('application_status', '=', 2)
            ->get();
$label = "Pending claimants";
        return view('claimant_list', [
            'getRolesId' => $getRolesId,
            'claimants' => $claimants,
            'approved' => $approved,
            'pending' => $pending,
            'declined' => $declined,
            'label' => $label
        ]);
}























        public function claimantsMain()
	{ 
		session_start();
                if(!isset($_SESSION['taxiName']))
{
return redirect()->route('claimantlogin');
}


                $name = $_SESSION['taxiName'];
                $province = $_SESSION['province'];
                $town = $_SESSION['town'];
                $data = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '$province' and taxi_assocs.town = '$town' and taxi_assocs.name = '".addslashes($name)."'");
                $mycount = count($data);
                //echo $mycount; die();
                $addQuery = " where (";
                $counter = 1;
                foreach($data as $datas)
                {
                   if($mycount >1)
                   {    if($counter !== $mycount){
                   	$addQuery .= "taxi_assoc_id = ".$datas->taxiassocId." or ";
                        }
                        else{
                           $addQuery .= "taxi_assoc_id = ".$datas->taxiassocId.")";
                        }
                   }else{
                     $addQuery .= "taxi_assoc_id = ".$datas->taxiassocId.")";
                   }
                 $counter++;
                }
                //echo $addQuery; 
$taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $taxi_assoc_count = TaxiAssoc::count();
        $claimant_count = DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery");
//var_dump($claimant_count); die();
        $claimant_count = $claimant_count[0]->mycount;
        $approved_claimant_count =DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery and application_status = 1");
        $approved_claimant_count = $approved_claimant_count[0]->mycount;
        $pending_claimant_count =DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery and application_status = 0");
        $pending_claimant_count = $pending_claimant_count[0]->mycount;
        $declined_claimant_count = DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery and application_status = 2");
        $declined_claimant_count = $declined_claimant_count[0]->mycount;
        $declined_half_count = DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery and application_status = 3");
        $declined_half_count = $declined_half_count[0]->mycount;
        $declined_full_count = DB::select("SELECT count(*) as mycount  FROM `claimants` $addQuery and application_status = 4");
        $declined_full_count = $declined_full_count[0]->mycount;
       $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('claimants', [
            'taxi_assocs' => $taxi_assocs,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxi_assoc_count' => $taxi_assoc_count,
            'claimant_count' => $claimant_count,
            'getRolesId' => $getRolesId,
            'approved_claimant_count' => $approved_claimant_count,
            'pending_claimant_count' => $pending_claimant_count,
            'declined_claimant_count' => $declined_claimant_count,
            'declined_half_count' => $declined_half_count,
            'declined_full_count' => $declined_full_count
        ]);

	}

 


}

?>
