<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

class TaxiController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    public function index($id)
    {
//echo $id; die();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $addReason = DB::select("select * from decline_reason where taxiAssocId = '$id'");
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT taxi_users.email as taxiEmail, taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.province = '".$data1[0]->province."' and taxi_assocs.town = '".$data1[0]->town."' and taxi_assocs.name = '".addslashes($data1[0]->name)."'");
        return view('taxiDetails', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'id' => $id,
            'addReason' => $addReason,
             'getRolesId' => $getRolesId


        ]);
    }

    public function decline_reason(Request $request)
    {

        DB::table('decline_reason')->insert([
            'id' => null,
            'taxiAssocId' => $request->id, 'reason' => $request->dReason,
            'addReason' => ""
        ]);
        $id = $request->id;
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 2 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");

        // addlogs to audit db
        $userId = $data1[0]->userId;
        $data2 =DB::select("select * from taxi_users where userId = '$userId'");


        $cell = $data2[0]->cell;
        $assoc = addslashes($data1[0]->name);



        // send sms
        $url = 'http://99.80.158.197/sms/sendSMSNotice.php';
        $cellFormated = substr($cell, 1, 9);
        //Initiate cURL
        $curl = curl_init($url);
        $data = array(
            'cell' => $cellFormated,
            'sms' => 'Your Association has been declined. Please contact SANTACO on 011 882 9928 for further information');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);

$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name."($role)";

        $section = 'Taxi Associations';
        $action = 'Disapproved a taxi Association - ' . $assoc;

        DB::table('auditAssoc')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'assoc'  => $assoc,
            'action' => $action,
            'userId' => $actionBy
        ]);
$data3 = DB::select("select * from taxi_assocs where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
   DB::table('taxiEvents')->insert([
            'id' => null,
            'user' => $actionBy,
            'taxiUserId' => $data3[0]->id,
            'taxiUserId2' =>  $data3[1]->id,
            'type'  => "Delined",
            'reason' => $request->dReason,
            'date' => DB::raw('now()')
        ]);
 

DB::table('taxiEvents')->insert([
            'id' => null,
            'user' => $actionBy,
            'taxiUserId' => $data3[0]->id,
            'taxiUserId2' =>  $data3[1]->id,
            'type'  => "Sms",
            'reason' => 'Sms sent to '.$cell.' Your Association has been declined. Please contact SANTACO on 011 882 9928 for further information',
            'date' => DB::raw('now()')
        ]);

        return redirect()->route('taxi_assocs_home');
    }

    public function reprove($id)
    {
//echo "hello"; die();
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $RemoveDecline = DB::select("delete from decline_reason where taxiAssocId = '$id'");
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 1 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");

        
        
        // addlogs to audit db
        $userId = $data1[0]->userId;
        $data2 =DB::select("select * from taxi_users where userId = '$userId'");


        $assoc = addslashes($data1[0]->name);
        $cell = $data2[0]->cell;
        $section = 'Taxi Associations';
        $action = 'Approved a taxi Association';
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name."($role)";

        // send sms
        $url = 'http://99.80.158.197/sms/sendSMSNotice.php';
        $cellFormated = substr($cell, 1, 9);
        //Initiate cURL
        $curl = curl_init($url);
        $data = array(
            'cell' => $cellFormated,
            'sms' => 'We are pleased to announce SANTACO has approved your Taxi Association.');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($curl);



        DB::table('auditAssoc')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'assoc'  => $assoc,
            'action' => $action,
            'userId' => $actionBy
        ]);

$data3 = DB::select("select * from taxi_assocs where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
DB::table('taxiEvents')->insert([
            'id' => null,
            'user' => $actionBy,
            'taxiUserId' => $data3[0]->id,
            'taxiUserId2' =>  $data3[1]->id,
            'type'  => "Approval",
            'reason' => "",
            'date' => DB::raw('now()')
        ]);



DB::table('taxiEvents')->insert([
            'id' => null,
            'user' => $actionBy,
            'taxiUserId' => $data3[0]->id,
            'taxiUserId2' =>  $data3[1]->id,
            'type'  => "Sms",
            'reason' => 'Sms sent to '.$cell.' We are pleased to announce SANTACO has approved your Taxi Association.',
            'date' => DB::raw('now()')
        ]);


        return redirect()->route('taxi_assocs_home');
    }

    public function deprove($id)
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $data1 = DB::select("select * from taxi_assocs where id = $id");
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("update taxi_assocs set status = 2 where province = '".$data1[0]->province."' and town = '".$data1[0]->town."' and name = '".addslashes($data1[0]->name)."'");
        
 
return redirect()->route('taxi_assocs_home');
    }

    public function main()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM `taxi_assocs` where (astatus = '1' and bstatus = '1') or  (astatus = '2' and bstatus = '2') order by id desc");
        $fas = "List of all taxi associations";
//print_r("<pre>");var_dump($data);die(); 
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
if(($getRolesId === 2) || ($getRolesId === 1) || ($getRolesId === 4) || ($getRolesId === 5))
{
        return view('taxidetaild', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
}
else
{
return redirect()->route('home');
}




    }

    public function pending()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs where status = 0 and astatus = '2' and bstatus = '2' order by id desc");
        $fas = "List of all pending associations";
        return view('taxidetaild', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }
    public function approved()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs  where status = 1 and astatus = '2' and bstatus = '2' order by id desc");
        $fas = "List of all Approved associations";
        return view('taxidetaild', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }

    public function declined()
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs  where status = 2 and astatus = '2' and bstatus = '2' order by id desc");
        $fas = "List of all declined associations";
        return view('taxidetaild', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }

public function incompleteTaxi() 
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT * FROM taxi_assocs  where bstatus = '1'  order by id desc");
        $fas = "List of all incomplete associations";
        return view('taxidetaild', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'name_link' => $fas
        ]);
    }

    public function profile($id)
    {
        $taxi_assocs = TaxiAssoc::orderBy('id', 'desc')->get();
        $taxi_user_count = TaxiUser::count();
        $member_count = TaxiMember::count();
        $taxi_count = Register::count();
        $taxi_assocs = TaxiAssoc::paginate(15);
        $data = DB::select("SELECT taxi_assocs.id as taxiassocId, taxi_users.*, taxi_users.name as meName, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_assocs.id = $id");
        //var_dump($data); die();
$taxiEvent =DB::select("select * from taxiEvents where taxiUserId = ".$data[0]->taxiassocId." or taxiUserId2 =  ".$data[0]->taxiassocId."");
//var_dump($data[0]->taxiassocId); die();
        return view('taxiProfile', ['data' => $data,
            'taxi_count' => $taxi_count,
            'taxi_user_count' => $taxi_user_count,
            'member_count' => $member_count,
            'taxiEvent' => $taxiEvent
        ]);
    }


}

?>
