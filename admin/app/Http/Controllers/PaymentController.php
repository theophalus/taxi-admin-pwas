<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser; 
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

class PaymentController extends Controller
{
/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware(['auth', 'isAdmin']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
    }
    public function index()
    {
       $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
	return view('paymentHome', ['getRolesId' => $getRolesId]);
    }
    public function import()
    {
       $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
	return view('import', ['getRolesId' => $getRolesId]);
    }
public function importForm()
    {
       //$myUser = Auth::user()->id;
	return view('importPayment', ['getRolesId' => '']);
    }

public function importFormFiles(Request $request)
    {
function cvf_ps_generate_random_code($length=10) {
$myreferences = '';

  $string = '';
  // You can define your own characters here.
  $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

  for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters)-1)];
  }

  return $string;

}
$ref = cvf_ps_generate_random_code(4);
$filename =  $_FILES['fname']['name']; 
if(pathinfo($filename)['extension'] == "xlsx")
{
$temp = explode(".", $_FILES['fname']['name']);
$newfilename = round(microtime(true)) . '.' . end($temp);
$uploaddir = './excel/';
$uploadfile = $uploaddir . $newfilename;
move_uploaded_file($_FILES['fname']['tmp_name'], $uploadfile);
$createdDate = date("Y-m-d h:i:s");

require_once ('../app/myexcel/vendor/autoload.php');

        $targetPath = $uploadfile;

        $Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $spreadSheet = $Reader->load($targetPath);
        $excelSheet = $spreadSheet->getActiveSheet();
        $spreadSheetAry = $excelSheet->toArray();
        $sheetCount = count($spreadSheetAry);
	foreach($spreadSheetAry as $val) 
{
//echo count($val); die();

if($val[0] != "ED Number") 
{
$val0 = @$val[0];
$val1 = @$val[1];
$val2 = @$val[2];
$val3 = @$val[3];
$val4 = @$val[4];
$val5 = @$val[5];
$val6 = @$val[6];
$val7 = @$val[7];
$val8 = @$val[8];
$val9 = @$val[9];
$val10 = @$val[10];
$val11 = @$val[11];
$tran = 0;
if (array_key_exists(0, $val)) {
    $tran += 1;
}
if (array_key_exists(1, $val)) {
    $tran += 1;
}
if (array_key_exists(2, $val)) {
    $tran += 1;
}
if (array_key_exists(3, $val)) {
    $tran += 1;
}
if (array_key_exists(4, $val)) {
    $tran += 1;
}
if (array_key_exists(5, $val)) {
    $tran += 1;
}
if (array_key_exists(6, $val)) {
    $tran += 1;
}
if (array_key_exists(7, $val)) {
    $tran += 1;
}
if (array_key_exists(8, $val)) {
    $tran += 1;
}
if (array_key_exists(9, $val)) {
    $tran += 1;
}
if (array_key_exists(10, $val)) {
    $tran += 1;
}
if (array_key_exists(11, $val)) {
    $tran += 1;
}
if($tran != 12)
{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Please Upload The correct File Format",

        ]);
}

$myreferences = $val4;
//echo "update csvDb set status = '$val3', 	reason = '$val6'  where ref1 = '$val4' "; die();
//echo $val4; die();
if(empty($val0) || empty($val1) || empty($val2) || empty($val3) || empty($val4) || empty($val5) || empty($val6) || empty($val7) || empty($val8)|| empty($val9) || empty($val11))
{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Please Upload The correct File Format",

        ]);
}
else
{
$refID = explode("-",$val4); 
$refID = "Ref-".$refID[1];
$dataResultPay = DB::select("SELECT * FROM `paylist` where status = 'imported' and refId = '$refID' ");
if(!empty($dataResultPay))
{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Batch File Import already exist",

        ]);

}
$dataResultPay = DB::select("SELECT * FROM `paylist` where refId = '$refID' ");
if(empty($dataResultPay))
{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Batch Number not recognised",

        ]);

}
$myref = explode("-",$val4); 
$myref = "Ref-".$myref[1];
$str = $val4;
$pacthBreak = explode("-", $str);
$patch = substr($pacthBreak[2],4);
$userId = Auth::user()->id;
$user =DB::select("select * from users where id = '$userId'");
$actionBy  = $user[0]->first_name . ' '.$user[0]->last_name;

$myUser = Auth::user()->id;
$getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = (string) $getRoles[0]->roleId;
$role_check = $getRolesId;
switch ($role_check) {
  case "1":
    $role_name = "Super admin";
    break;
  case "2":
    $role_name = "Taxi association admin";
    break;
  case "4":
    $role_name = "Query management agent";
    break;
  case "5":
    $role_name = "Fund administrator";
    break;
}
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name."($role_name)";





if($val3 == "Disputed")
{
  $message = "Payments of R $val5 has been Declined and the reason is - $val6";
$getClaimant = DB::select("SELECT * FROM `claimants` where id =  $patch");
DB::table('rejected_payments')->insert([
            'id' => null,
            'name' =>  $getClaimant[0]->first_name." ".$getClaimant[0]->last_name,
            'id_number' => $getClaimant[0]->id_number,
            'type'  => $getClaimant[0]->title,
            'contact' => $getClaimant[0]->cellphone_number,
            'application_status' => "2",
            'bank' => $val7,
            'reason' => $val6,
            'status' => "pending", 
            'ref' => $myref,          
            'date' => DB::raw('now()'), 
        ]);
}
if($val3 == "Successful") 
{
  $message = "Payments of R $val5 has been Successfully paid";
}
DB::table('claimEvents')->insert([
            'id' => null,
            'user' => $actionBy,
            'claimId' => $patch,
            'type'  => $message,
            'person' => $myref,
            'date' => DB::raw('now()'),
            'status' => 'pending'
        ]);
DB::select("update csvDb set status = '".addslashes($val3)."', 	reason = '".addslashes($val6)."'  where ref1 = '".addslashes($val4)."' ");
DB::table('ImportPayments')->insert([
            'id' => null,
            'ed_number' => "$val0", 
			'group_t' => "$val1",
			'service_type' => "$val2",
			'status' => "$val3",
			'reference1' => "$val4",
			'amount' => "$val5",
			'status_description' => "$val6",
			'account_holder' => "$val7",
			'date_created' => "$val8",
			'action_date' => "$val9",
			'c_number' => "$val10",
            'processing_date' => "$val11",
            'upload_ref' => $ref
        ]);




}


/*}
else
{

$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Please Upload The correct File Format",

        ]);

}
*/
}



}
$myreferences = explode("-",$myreferences); 
$myreferences = "Ref-".$myreferences[1];
//echo $myreferences; die();
$dataResultPay = DB::select("SELECT * FROM `payimport` where refId = '$myreferences' ");
if(!empty($dataResultPay))
{
return redirect()->route('getImportlist');

}
DB::table('payimport')->insert([ 
            'id' => null,
'name' => $_POST['name'],
'refId' => $myreferences,
'file' => $uploadfile,
'status' => 'pending',
'createdAt' => $createdDate
        ]);
$to_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Owner' and refId = '$myreferences' ");
$to_positive = (int) $to_positive[0]->mycount;
$to_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Owner' and refId = '$myreferences' ");
$to_negative = (int) $to_negative[0]->mycount;
$to_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Owner' and refId = '$myreferences' ");
$to_sum_positive = (int) $to_sum_positive[0]->mycount;
$to_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Owner' and refId = '$myreferences' ");
$to_sum_negative = (int) $to_sum_negative[0]->mycount;
$to_positive_total = $to_positive * $to_sum_positive;
$to_negative_total = $to_negative * $to_sum_negative;


$ta_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Driver' and refId = '$myreferences' ");
$ta_positive = (int) $ta_positive[0]->mycount;
$ta_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Driver' and refId = '$myreferences' ");
$ta_negative = (int) $ta_negative[0]->mycount;
$ta_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Driver' and refId = '$myreferences' ");
$ta_sum_positive = (int) $ta_sum_positive[0]->mycount;
$ta_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Driver' and refId = '$myreferences' ");
$ta_sum_negative = (int) $ta_sum_negative[0]->mycount;
$ta_positive_total = $ta_positive * $ta_sum_positive;
$ta_negative_total = $ta_negative * $ta_sum_negative;

$qm_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Queue Marshall' and refId = '$myreferences' ");
$qm_positive = (int) $qm_positive[0]->mycount;
$qm_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Queue Marshall' and refId = '$myreferences' ");
$qm_negative = (int) $qm_negative[0]->mycount;
$qm_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Queue Marshall' and refId = '$myreferences' ");
$qm_sum_positive = (int) $qm_sum_positive[0]->mycount;
$qm_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Queue Marshall' and refId = '$myreferences' ");
$qm_sum_negative = (int) $qm_sum_negative[0]->mycount;
$qm_positive_total = $qm_positive * $qm_sum_positive;
$qm_negative_total = $qm_negative * $qm_sum_negative;

$rm_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Rank Marshall' and refId = '$myreferences' ");
$rm_positive = (int) $rm_positive[0]->mycount;
$rm_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Rank Marshall' and refId = '$myreferences' ");
$rm_negative = (int) $rm_negative[0]->mycount;
$rm_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Rank Marshall' and refId = '$myreferences' ");
$rm_sum_positive = (int) $rm_sum_positive[0]->mycount;
$rm_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Rank Marshall' and refId = '$myreferences' ");
$rm_sum_negative = (int) $rm_sum_negative[0]->mycount;
$rm_positive_total = $rm_positive * $rm_sum_positive;
$rm_negative_total = $rm_negative * $rm_sum_negative;

$as_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Association Staff' and refId = '$myreferences' ");
$as_positive = (int) $as_positive[0]->mycount;
$as_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Association Staff' and refId = '$myreferences' ");
$as_negative = (int) $as_negative[0]->mycount;
$as_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Association Staff' and refId = '$myreferences' ");
$as_sum_positive = (int) $as_sum_positive[0]->mycount;
$as_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Association Staff' and refId = '$myreferences' ");
$as_sum_negative = (int) $as_sum_negative[0]->mycount;
$as_positive_total = $as_positive * $as_sum_positive;
$as_negative_total = $as_negative * $as_sum_negative;


$o_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Other' and refId = '$myreferences' ");
$o_positive = (int) $o_positive[0]->mycount;
$o_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Other' and refId = '$myreferences' ");
$o_negative = (int) $o_negative[0]->mycount;
$o_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Other' and refId = '$myreferences' ");
$o_sum_positive = (int) $o_sum_positive[0]->mycount;
$o_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Other' and refId = '$myreferences' ");
$o_sum_negative = (int) $o_sum_negative[0]->mycount;
$o_positive_total = $o_positive * $ta_sum_positive;
$o_negative_total = $o_negative * $ta_sum_negative;


$main_positive_total = $to_positive_total + $ta_positive_total + $qm_positive_total + $rm_sum_positive + $as_positive_total + $o_positive_total;
$main_negative_total = $to_negative_total + $ta_negative_total + $qm_negative_total + $rm_sum_negative + $as_negative_total + $o_negative_total;
$total_positive_count = $to_positive + $ta_positive + $as_positive + $rm_positive + $qm_positive + $o_positive;
$total_negative_count = $to_negative + $ta_negative + $qm_negative + $rm_negative + $as_negative + $o_negative;
$Authorisation_cancelled = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` = 'Authorisation cancelled' and refId = '$myreferences' ");
$Authorisation_cancelled = (int) $Authorisation_cancelled[0]->mycount;
$no_authority = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` = 'No authority to debit' and refId = '$myreferences' ");
$no_authority = (int) $no_authority[0]->mycount;
$contravention = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` IN (\"Debit in contravention of payer's authority\" , 'Debit in contravention of payer\'s authority') and refId = '$myreferences' ");
$contravention = (int) $contravention[0]->mycount;



$Authorisation_cancelled_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` = 'Authorisation cancelled' and refId = '$myreferences' ");
$Authorisation_cancelled_amount = (int) $Authorisation_cancelled_amount[0]->mycount;
$no_authority_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` = 'No authority to debit' and refId = '$myreferences' ");
$no_authority_amount = (int) $no_authority_amount[0]->mycount;
$contravention_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` IN (\"Debit in contravention of payer's authority\" , 'Debit in contravention of payer\'s authority') and refId = '$myreferences' ");
$contravention_amount = (int) $contravention_amount[0]->mycount;

 



//echo $myreferences; die();
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $data = DB::select("select * from paylist where refId = '$myreferences'");
//var_dump($myreferences); die();

        $data1 = DB::select("select * from payInsert where refId = '$myreferences'");
$userId = Auth::user()->id;
$user =DB::select("select * from users where id = '$userId'");
$actionBy  = $user[0]->first_name . ' '.$user[0]->last_name;

$myUser = Auth::user()->id;
$getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = (string) $getRoles[0]->roleId;
$role_check = $getRolesId;
switch ($role_check) {
  case "1":
    $role_name = "Super admin";
    break;
  case "2":
    $role_name = "Taxi association admin";
    break;
  case "4":
    $role_name = "Query management agent";
    break;
  case "5":
    $role_name = "Fund administrator";
    break;
}
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name."($role_name)";

        $section = 'Fund Administrator';
        $action = 'Imported Payment List status - Pending';
DB::table('auditFunds')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'batch'  => $myreferences,
            'action' => $action,
            'userId' => $actionBy
        ]);
        return view('viewbatch2', ['data' => $data,'data1' => $data1,
             'getRolesId' => $getRolesId,
             'main_positive_total' => $main_positive_total,
             'main_negative_total' => $main_negative_total,
             'total_positive_count' => $total_positive_count,
             'total_negative_count' => $total_negative_count,
             'contravention' => $contravention,
             'Authorisation_cancelled_amount' => $Authorisation_cancelled_amount,
             'no_authority_amount' => $no_authority_amount,
             'contravention_amount' => $contravention_amount,
             'no_authority' => $no_authority,
             'Authorisation_cancelled' => $Authorisation_cancelled,
                                 'to_positive' => $to_positive,
                                 'ta_positive' => $ta_positive,
                                 'rm_positive' => $rm_positive,
                                 'qm_positive' => $qm_positive,
                                 'as_positive' => $as_positive,
                                 'o_positive' => $o_positive,
                                 'to_negative' => $to_negative,
                                 'ta_negative' => $ta_negative,
                                 'rm_negative' => $rm_negative,
                                 'qm_negative' => $qm_negative,
                                 'as_negative' => $as_negative,
                                 'o_negative' => $o_negative,


        ]);
}
else
{
$myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay2', ['getRolesId' => $getRolesId,
           
'errorMessage' => "Please Upload The correct File Format",

        ]);
}
//return redirect()->route('getImportlist');


    }


public function downloadExcel($id)
{
$dataResultPay = DB::select("SELECT * FROM `payimport` where id = '$id'");
    $file = $dataResultPay[0]->file;
    header('Content-Description: File Transfer');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename=download.xlsx');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    readfile($file);
}

public function showPaylist($id)
{
$dataResultPay = DB::select("SELECT * FROM `csvDb` where refId = '$id'");
return view('showPaylist', ['data' => $dataResultPay]);

}


public function getNumRec()
{
$dataResultPay = DB::select("SELECT * FROM numberGenerate order by id desc limit 0,1 ");
if(empty($dataResultPay))
{
DB::table('numberGenerate')->insert([
            'id' => null,
             'num' => 1
        ]);
echo "started 1";
}
else
{
 $num = (int) $dataResultPay[0]->num;
$num = $num + 1;
DB::table('numberGenerate')->insert([
            'id' => null,
             'num' => $num
        ]);
}

}















    public function importTable(Request $request)
    {
$to = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Taxi Owner' ");
$to = (int) $to[0]->mycount;
$ta = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Taxi Driver' ");
$ta = (int) $ta[0]->mycount;
$qm = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Queue Marshall' ");
$qm = (int) $qm[0]->mycount;
$rm = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Rank Marshall' ");
$rm = (int) $rm[0]->mycount;
$as = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Association Staff' ");
$as = (int) $as[0]->mycount;
$o = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Other' ");
$o = (int) $o[0]->mycount;
$groupTotal = 0;
$totalMainCharges = 0;
$totalMains = 0;
$totalExpensess = 0;
if(isset($_POST['taxiOwnerAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$taxiOwnerAmount = (float) $_POST['taxiOwnerAmount'];
$taxiOwnerAmount = (float) $taxiOwnerAmount;
$payPerDay = (float) $bank_charges * $taxiOwnerAmount;
$total = (float) $bank_charges * $taxiOwnerAmount * $to;
$total = bcdiv($total, 1, 2);
$totalBankCharges = (float) $realBankCharges  * $to;
$totalBankCharges = bcdiv($totalBankCharges, 1, 2);
$totalExpense = (float) $total + $totalBankCharges;
$totalExpense = bcdiv($totalExpense, 1, 2);
$groupTotal += (float) $to;
$totalMainCharges += $totalBankCharges;
//$totalMainCharges= bcdiv($totalMainCharges, 1, 2);
$totalExpensess += (float) $totalExpense;
//$totalExpensess= bcdiv(totalExpensess, 1, 2);
$totalMains += (float) $total;
//$totalMains = bcdiv($totalMains, 1, 2);
}
 

if(isset($_POST['taxiasscAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$taxiasscAmount = $_POST['taxiasscAmount'];
$taxiasscAmount = (float) $taxiasscAmount;
$payPerDay1 = (float) $bank_charges * $taxiasscAmount;
$total1 = (float) $bank_charges * $taxiasscAmount * $ta;
$total1 = bcdiv($total1, 1, 2);
$totalBankCharges1 = (float) $realBankCharges * $ta;
$totalBankCharges1 = bcdiv($totalBankCharges1, 1, 2);
$totalExpense1 = (float) $total1 + $totalBankCharges1;
$totalExpense1 = bcdiv($totalExpense1, 1, 2);
$groupTotal += (float) $ta;
$totalMainCharges += (float) $totalBankCharges1;
$totalMains += (float) $total1;
$totalExpensess += (float) bcdiv($totalExpense1, 2);
}


if(isset($_POST['qeueMarshalAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$qeueMarshalAmount = (float) $_POST['qeueMarshalAmount'];
$payPerDay2 = bcdiv($bank_charges * $qeueMarshalAmount, 1, 2); 
$total2 = bcdiv($bank_charges * $qeueMarshalAmount * $qm, 1, 2);
$totalBankCharges2 = bcdiv($realBankCharges * $qm, 1, 2);
$totalExpense2 = bcdiv($total2 + $totalBankCharges2, 1, 2);
$groupTotal += (float) $qm;
$totalMainCharges += (float) $totalBankCharges2;
$totalMains += (float) $total2;
$totalExpensess += (float) $totalExpense2;
}


if(isset($_POST['assocStaffAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$assocStaffAmount = (float) $_POST['assocStaffAmount'];
$assocStaffAmount = (float) $assocStaffAmount;
$payPerDay3 = (float) $bank_charges * $assocStaffAmount;
$payPerDay3 = bcdiv($payPerDay3, 1, 2);
$total3 = (float) $bank_charges * $assocStaffAmount * $as;
$total3 = bcdiv($total3, 1, 2);
$totalBankCharges3 = (float) $realBankCharges * $as;
$totalBankCharges3 = bcdiv($totalBankCharges3, 1, 2);
$totalExpense3 = (float) $total3 + $totalBankCharges3;
$totalExpense3 = bcdiv($totalExpense3, 1, 2);
$groupTotal += $as;
$totalMainCharges += (float) $totalBankCharges3;
$totalMains += (float) $total3;
$totalExpensess += (float) bcdiv($totalExpense3, 1, 2);
}

if(isset($_POST['rankMarshallAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$rankMarshallAmount = (float) $_POST['rankMarshallAmount'];
$rankMarshallAmount = (float) $rankMarshallAmount;
$payPerDay4 = (float) $bank_charges * $rankMarshallAmount;
$payPerDay4 = bcdiv($payPerDay4, 1, 2);
$total4 = (float) $bank_charges * $rankMarshallAmount * $rm;
$total4 = bcdiv($total4, 1, 2); 
$totalBankCharges4 = (float) $realBankCharges * $rm;
$totalBankCharges4 = bcdiv($totalBankCharges4, 1, 2); 
$totalExpense4 = (float) $total4 + $totalBankCharges4;
$totalExpense4 = bcdiv($totalExpense4, 1, 2);
$groupTotal += (float) $rm;
$totalMainCharges += (float) $totalBankCharges4;
$totalMains += (float) $total4;
$totalExpensess += (float) $totalExpense4;
}
//print_r("<pre>");  var_dump($_POST['otherAmount]);

if(isset($_POST['otherAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$realBankCharges = (float) $_POST['bankCharges'];

$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$otherAmount = (float) $_POST['otherAmount'];
$otherAmount = (float) $otherAmount;
$payPerDay5 = bcdiv($bank_charges * $otherAmount, 1,2);
$total5 = (float) $bank_charges * $otherAmount * $o;
$total5 = bcdiv($total5, 1,2);
$totalBankCharges5 = bcdiv($realBankCharges * $o, 1,2);
$totalExpense5 = bcdiv($total5 + $totalBankCharges5, 1, 2);
$groupTotal += (float) $o;
$totalMainCharges += $totalBankCharges5;
$totalMainCharges =  bcdiv($totalMainCharges, 1, 2);
//$totalMains += $total5;
//$totalExpensess += $totalExpense5;



$totalMains += $total5;
$totalMains =  bcdiv($totalMains, 1, 2);
$totalExpensess += $totalExpense5;
$totalExpensess = bcdiv($totalExpensess, 1, 2);
}





$table = "<table border='1'>
<tr>
		<td><b>Claimant Roles</b></td>
		<td><b>Number of Verified Claimants</b></td>
		<td><b>Amount per day</b></td>
		<td><b>Number of Days</b></td>
		<td><b>Total Pay-Away Amount per Verified Claimant</b></td>
		<td><b>Bank Charges</b></td>
		<td><b>Total Bank Charges</b></td>
		<td><b>Total per Claimant Role</b></td>
		<td><b>Total Exspense</b></td>
	</tr>
";
if(isset($_POST['taxiOwnerAmount']))
{
$table .= "
<tr>
		<td>Taxi Owner</td>
		<td>$to</td>
		<td>R   ".$_POST['taxiOwnerAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges</td>
		<td>R $total</td>
		<td>R $totalExpense</td>
	</tr>
";
}
if(isset($_POST['taxiasscAmount']))
{
$table .= "
<tr>
		<td>Taxi Association</td>
		<td>$ta</td>
		<td>R   ".$_POST['taxiasscAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay1</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges1</td>
		<td>R $total1</td>
		<td>R $totalExpense1</td>
	</tr>
";
}

if(isset($_POST['qeueMarshalAmount']))
{
$table .= "
<tr>
		<td>Qeue Marshall</td>
		<td>$qm</td>
		<td>R   ".$_POST['qeueMarshalAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay2</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges2</td>
		<td>R $total2</td>
		<td>R $totalExpense2</td>
	</tr>
";
}


if(isset($_POST['assocStaffAmount']))
{
$table .= "
<tr>
		<td>Association Staff</td>
		<td>$as</td>
		<td>R   ".$_POST['assocStaffAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay3</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges3</td>
		<td>R $total3</td>
		<td>R $totalExpense3</td>
	</tr>
";
}

if(isset($_POST['rankMarshallAmount']))
{
$table .= "
<tr>
		<td>Rank Marshall</td>
		<td>$rm</td>
		<td>R   ".$_POST['rankMarshallAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay4</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges4</td>
		<td>R $total4</td>
		<td>R $totalExpense4</td>
	</tr>
";
}




if(isset($_POST['otherAmount']))
{
$table .= "
<tr>
		<td>Other</td>
		<td>$o</td>
		<td>R   ".$_POST['otherAmount']."</td>
		<td>$bank_charges</td>
		<td>R  $payPerDay5</td>
		<td>R ".$_POST['bankCharges']."</td>
		<td>R $totalBankCharges5</td>
		<td>R $total5</td>
		<td>R $totalExpense5</td>
	</tr>
";
}

$table .= "
<tr>
		<td><b>Total Amount:</b></td>
		<td><b>$groupTotal</b></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td><b>R $totalMainCharges</b></td>
		<td><b>R $totalMains</b></td>
		<td><b>R $totalExpensess</b></td>
	</tr>
";

$table .= "</table>";
	return $table;

    
}




public function importTableinsert(Request $request)
    {
$to = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Taxi Owner' ");
$to = (int) $to[0]->mycount;
$ta = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Taxi Driver' ");
$ta = (int) $ta[0]->mycount;
$qm = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Queue Marshall' ");
$qm = (int) $qm[0]->mycount;
$rm = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Rank Marshall' ");
$rm = (int) $rm[0]->mycount;
$as = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Association Staff' ");
$as = (int) $as[0]->mycount;
$o = DB::select("SELECT count(*) as mycount FROM claimants where `application_status` = 2 and `title` = 'Other' ");
$o = (int) $o[0]->mycount;
function cvf_ps_generate_random_code($length=10) {

  $string = '';
  // You can define your own characters here.
  $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

  for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters)-1)];
  }

  return $string;

}
$refnew = 'Ref-100';
$dataResultPay = DB::select("SELECT * FROM numberGenerate order by id desc limit 0,1 ");
if(empty($dataResultPay))
{
DB::table('numberGenerate')->insert([
            'id' => null,
             'num' => 1
        ]);
$refnew = $refnew."1";
}
else
{
 $num = (int) $dataResultPay[0]->num;
$num = $num + 1;
DB::table('numberGenerate')->insert([
            'id' => null,
             'num' => $num
        ]);
$refnew = $refnew.$num;
}
$refId = $refnew;
$createdDate = date("Y-m-d h:i:s");
$bankCharges24 = (float) $_POST['bankCharges'];
$bankCharges24 = bcdiv($bankCharges24, 1, 2);
$taxiOwnerAmount24 = (float) $_POST['taxiOwnerAmount'];
$taxiOwnerAmount24 = bcdiv($taxiOwnerAmount24, 1, 2); 
$taxiasscAmount24 = (float) $_POST['taxiasscAmount'];
$taxiasscAmount24 = bcdiv($taxiasscAmount24, 1, 2);
$qeueMarshalAmount24 = (float) $_POST['qeueMarshalAmount'];
$qeueMarshalAmount24 = bcdiv($qeueMarshalAmount24, 1, 2); 
$assocStaffAmount24 = (float) $_POST['assocStaffAmount'];
$assocStaffAmount24 = bcdiv($qeueMarshalAmount24, 1, 2);  
$rankMarshallAmount24 = (float) $_POST['rankMarshallAmount'];
$rankMarshallAmount24 = bcdiv($rankMarshallAmount24, 1, 2); 
$assocStaffAmount24 = (float) $_POST['assocStaffAmount'];
$assocStaffAmount24 = bcdiv($assocStaffAmount24, 1, 2); 
$otherAmount24 = (float) $_POST['otherAmount'];
$otherAmount24 = bcdiv($otherAmount24, 1, 2);  
DB::table('paylist')->insert([
            'id' => null,
            'bankCharges' => $bankCharges24, 
			'taxiOwner' => $taxiOwnerAmount24,
			'taxiDriver' => $taxiasscAmount24,
			'qeueMarshal' => $qeueMarshalAmount24,
			'rankMarshal' => $rankMarshallAmount24,
			'assocStaff' => $assocStaffAmount24,
			'other' => $otherAmount24,
			'fromDate' => $_POST['fromDate'],
			'toDate' => $_POST['toDate'],
			'releaseDate' => $_POST['actionedDate'],
			'status' => 'pending',
			'refId' => $refId,
            'createdAt' => $createdDate
        ]);
$groupTotal = 0;
$totalMainCharges = 0;
$totalMains = 0;
$totalExpensess = 0;
if(isset($_POST['taxiOwnerAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$taxiOwnerAmount = (float) $_POST['taxiOwnerAmount'];
$taxiOwnerAmount = (float) $taxiOwnerAmount;
$payPerDay = (float) $bank_charges * $taxiOwnerAmount;
$payPerDay = bcdiv($payPerDay, 1, 2);
$total = (float) $bank_charges * $taxiOwnerAmount * $to;
$total = bcdiv($total, 1, 2);
$totalBankCharges = (float) $realBankCharges  * $to;
$totalBankCharges = bcdiv($totalBankCharges, 1, 2);
$totalExpense = (float) $total + $totalBankCharges;
$totalExpense = bcdiv($totalExpense, 1, 2);
$groupTotal += (float) $to;
$totalMainCharges += $totalBankCharges;
//$totalMainCharges= bcdiv($totalMainCharges, 1, 2);
$totalExpensess += (float) $totalExpense;
//$totalExpensess= bcdiv(totalExpensess, 1, 2);
$totalMains += (float) $total;
//$totalMains = bcdiv($totalMains, 1, 2);
}
 

if(isset($_POST['taxiasscAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$taxiasscAmount = $_POST['taxiasscAmount'];
$taxiasscAmount = (float) $taxiasscAmount;
$payPerDay1 = (float) $bank_charges * $taxiasscAmount;
$payPerDay1 = bcdiv($payPerDay1, 1, 2);
$total1 = (float) $bank_charges * $taxiasscAmount * $ta;
$total1 = bcdiv($total1, 1, 2);
$totalBankCharges1 = (float) $realBankCharges * $ta;
$totalBankCharges1 = bcdiv($totalBankCharges1, 1, 2);
$totalExpense1 = (float) $total1 + $totalBankCharges1;
$totalExpense1 = bcdiv($totalExpense1, 1, 2);
$groupTotal += (float) $ta;
$totalMainCharges += (float) $totalBankCharges1;
$totalMains += (float) $total1;
$totalExpensess += (float) bcdiv($totalExpense1, 2);
}


if(isset($_POST['qeueMarshalAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$qeueMarshalAmount = (float) $_POST['qeueMarshalAmount'];
$payPerDay2 = bcdiv($bank_charges * $qeueMarshalAmount, 1, 2);
$payPerDay2 = bcdiv($payPerDay2, 1, 2); 
$total2 = bcdiv($bank_charges * $qeueMarshalAmount * $qm, 1, 2);
$totalBankCharges2 = bcdiv($realBankCharges * $qm, 1, 2);
$totalExpense2 = bcdiv($total2 + $totalBankCharges2, 1, 2);
$groupTotal += (float) $qm;
$totalMainCharges += (float) $totalBankCharges2;
$totalMains += (float) $total2;
$totalExpensess += (float) $totalExpense2;
}


if(isset($_POST['assocStaffAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$assocStaffAmount = (float) $_POST['assocStaffAmount'];
$assocStaffAmount = (float) $assocStaffAmount;
$payPerDay3 = (float) $bank_charges * $assocStaffAmount;
$payPerDay3 = bcdiv($payPerDay3, 1, 2);
$total3 = (float) $bank_charges * $assocStaffAmount * $as;
$total3 = bcdiv($total3, 1, 2);
$totalBankCharges3 = (float) $realBankCharges * $as;
$totalBankCharges3 = bcdiv($totalBankCharges3, 1, 2);
$totalExpense3 = (float) $total3 + $totalBankCharges3;
$totalExpense3 = bcdiv($totalExpense3, 1, 2);
$groupTotal += $as;
$totalMainCharges += (float) $totalBankCharges3;
$totalMains += (float) $total3;
$totalExpensess += (float) bcdiv($totalExpense3, 1, 2);
}

if(isset($_POST['rankMarshallAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$realBankCharges = (float) $_POST['bankCharges'];
$rankMarshallAmount = (float) $_POST['rankMarshallAmount'];
$rankMarshallAmount = (float) $rankMarshallAmount;
$payPerDay4 = (float) $bank_charges * $rankMarshallAmount;
$payPerDay4 = bcdiv($payPerDay4, 1, 2);
$total4 = (float) $bank_charges * $rankMarshallAmount * $rm;
$total4 = bcdiv($total4, 1, 2); 
$totalBankCharges4 = (float) $realBankCharges * $rm;
$totalBankCharges4 = bcdiv($totalBankCharges4, 1, 2); 
$totalExpense4 = (float) $total4 + $totalBankCharges4;
$totalExpense4 = bcdiv($totalExpense4, 1, 2);
$groupTotal += (float) $rm;
$totalMainCharges += (float) $totalBankCharges4;
$totalMains += (float) $total4;
$totalExpensess += (float) $totalExpense4;
}
//print_r("<pre>");  var_dump($_POST['otherAmount]);

if(isset($_POST['otherAmount']))
{
$date1=date_create($_POST['fromDate']);
$date2=date_create($_POST['toDate']);
$diff=date_diff($date1,$date2); 
$realBankCharges = (float) $_POST['bankCharges'];

$bank_charges = $diff->format("%R%a"); 
$bank_charges = (int) $bank_charges;
$otherAmount = (float) $_POST['otherAmount'];
$otherAmount = (float) $otherAmount;
$payPerDay5 = bcdiv($bank_charges * $otherAmount, 1,2);
$total5 = (float) $bank_charges * $otherAmount * $o;
$total5 = bcdiv($total5, 1,2);
$totalBankCharges5 = bcdiv($realBankCharges * $o, 1,2);
$totalExpense5 = bcdiv($total5 + $totalBankCharges5, 1, 2);
$groupTotal += (float) $o;
$totalMainCharges += $totalBankCharges5;
$totalMainCharges =  bcdiv($totalMainCharges, 1, 2);
$totalMains += $total5;
$totalMains =  bcdiv($totalMains, 1, 2);
$totalExpensess += $totalExpense5;
$totalExpensess = bcdiv($totalExpensess, 1, 2);
}
DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Taxi Owner",
			'claimantNo' => $to,
			'noPerDays' => $_POST['taxiOwnerAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges,
			'totalPerRole' => $total,
			'totalExpense' => $totalExpense,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Taxi Driver",
			'claimantNo' => $ta,
			'noPerDays' => $_POST['taxiasscAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay1,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges1,
			'totalPerRole' => $total1,
			'totalExpense' => $totalExpense1,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Qeue Marshall",
			'claimantNo' => $qm,
			'noPerDays' => $_POST['qeueMarshalAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay2,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges2,
			'totalPerRole' => $total2,
			'totalExpense' => $totalExpense2,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Association Staff",
			'claimantNo' => $as,
			'noPerDays' => $_POST['assocStaffAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay3,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges3,
			'totalPerRole' => $total3,
			'totalExpense' => $totalExpense3,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Rank Marshall",
			'claimantNo' => $rm,
			'noPerDays' => $_POST['rankMarshallAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay4,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges4,
			'totalPerRole' => $total4,
			'totalExpense' => $totalExpense4,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

DB::table('payInsert')->insert([
            'id' => null,
            'refId' => $refId, 
			'role' => "Other",
			'claimantNo' => $o,
			'noPerDays' => $_POST['otherAmount'],
			'days' => $bank_charges,
			'amountPer' => $payPerDay5,
			'bankCharges' => $_POST['bankCharges'],
			'totalBankCharges' => $totalBankCharges5,
			'totalPerRole' => $total5,
			'totalExpense' => $totalExpense5,
                        'groupTotal' => $groupTotal,
                        'totalMainCharges' => $totalMainCharges,
                        'totalMains' => $totalMains,
                        'totalExpensess' => $totalExpensess,
            'createdAt' => $createdDate
        ]);

$userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
$myUser = Auth::user()->id;
$getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = (string) $getRoles[0]->roleId;
$role_check = $getRolesId;
switch ($role_check) {
  case "1":
    $role_name = "Super admin";
    break;
  case "2":
    $role_name = "Taxi association admin";
    break;
  case "4":
    $role_name = "Query management agent";
    break;
  case "5":
    $role_name = "Fund administrator";
    break;
}
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name."($role_name)";

        $section = 'Fund Administrator';
        $action = 'Batch created';
DB::table('auditFunds')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'batch'  => $refId,
            'action' => $action,
            'userId' => $actionBy 
        ]);
	return "inserted".$actionBy;

    
}


function importTableTest($id)
{
//echo $id; die();
$myreference = $id;
$dataResult = DB::select("SELECT * FROM `payInsert` where `refId` = '$id'");
$dataResultList = DB::select("SELECT * FROM `paylist` where `refId` = '$id'");
//var_dump($dataResultList); die();
$actionDate = $dataResultList[0]->releaseDate;
//$getar = array();
$CheckIndB = DB::select("SELECT count(*) as mycount FROM csvDb where refId = '$id'  ");
//echo gettype($CheckIndB[0]->mycount); die();
if($CheckIndB[0]->mycount === 0)
{
foreach($dataResult as $datas)
{
$getar[] = $datas->role;
$favcolor = $datas->role;  
switch ($favcolor) {
  case "Taxi Owner":
    $to = $datas->amountPer; 
    break;
  case "Taxi Driver":
    $ta = $datas->amountPer;
    break;
  case "Qeue Marshall":
    $qm = $datas->amountPer;
    break;
case "Queue Marshall":
    $amount = $qm;
    break;
case "Rank Marshall":
    $rm = $datas->amountPer;
    break;
case "Other":
    $qo = $datas->amountPer;
    break;
}
}
//echo getcwd();die(); 
//print_r("<pre>"); var_dump($getar); die();
$createdDate = date("Y-m-d h:i:s");
$data = DB::select("SELECT claimants.id as claimIdNo, claimants.*, bank_accounts.* FROM claimants INNER JOIN  bank_accounts on bank_accounts.`claimant_id` = claimants.id where  application_status = 2");
foreach($data as $datas)
{


$favcolor = $datas->title;  
switch ($favcolor) {
  case "Taxi Owner":
    $amount = $to; 
    break;
  case "Taxi Driver":
    $amount = $ta;
    break;
  case "Qeue Marshall":
    $amount = $qm;
    break;
 case "Queue Marshall":
    $amount = $qm;
    break;
case "Rank Marshall":
    $amount = $rm;
    break;
case "Other":
    $amount =  $qo;
    break;
}

DB::table('csvDb')->insert([
            'id' => null,
            'ref1' => $myreference.'-1000'.$datas->claimIdNo, 
			'ref2' => "INT-REF-1DC",
			'idNum' => 'SID',
			'idType' => $datas->id_number,
			'branchCode' => $datas->branch_code,
			'account' => $datas->account_number,
			'holder' => $datas->account_name,
			'accType' => '1',
			'serviceType' => 'PCF',
			'type' => '1DC',
            'amount' => $amount,
			'actionDate' => $actionDate,
			'refId' => $id,
			'createdAt' => $createdDate,
                        'status' => 'pending',
                        'reason' => '',
                        'taxiType' => $datas->title,

        ]);

}

$data = DB::select("SELECT claimants.id as claimIdNo, claimants.*, receiving_account.* FROM claimants INNER JOIN  receiving_account on receiving_account.`claimant_id` = claimants.id where application_status = 2");
foreach($data as $datas)
{


$favcolor = $datas->title;  
switch ($favcolor) {
  case "Taxi Owner":
    $amount = $to; 
    break;
  case "Taxi Driver":
    $amount = $ta;
    break;
  case "Qeue Marshall":
    $amount = $qm;
    break;
 case "Queue Marshall":
    $amount = $qm;
    break;
case "Rank Marshall":
    $amount = $rm;
    break;
case "Other":
    $amount =  $qo;
    break;
}

DB::table('csvDb')->insert([
            'id' => null,
            'ref1' => $myreference.'-1000'.$datas->claimIdNo, 
			'ref2' => "INT-REF-1DC",
			'idNum' => 'SID',
			'idType' => $datas->receiver_id_number,
			'branchCode' => $datas->branch,
			'account' => $datas->bank_account_number,
			'holder' => $datas->receiver_name." ".$datas->receiver_surname,
			'accType' => '1',
			'serviceType' => 'PCF',
			'type' => '1DC',
            'amount' => $amount,
			'actionDate' => $actionDate,
			'refId' => $id,
			'createdAt' => $createdDate,
                        'status' => 'pending',
                        'reason' => '',
                        'taxiType' => $datas->title,
        ]);


}
}
//echo "SELECT ref1, ref2, idNum, idType, branchCode, account, holder, accType, serviceType, type, amount, actionDate FROM `csvDb` where refId = '$myreference'"; die();
$dataResultPay = DB::select("SELECT ref1, ref2, idNum, idType, branchCode, account, holder, accType, serviceType, type, amount, actionDate FROM `csvDb` where refId = '$myreference'");
$result = array();
$result[] = array("Reference 1","Reference 2","Identification Type","Identification Number","Branch Code","Account Number","Account Holder","Account Type","Service Type","Type","Payment Amount","Action Date");
foreach($dataResultPay as $datas)
{
$result[] = (array) $datas;
}
//print_r("<pre>"); var_dump($result); die();
header('Content-Type: application/download');
header("Content-Type: text/csv; charset=utf-8");
header('Content-Disposition: attachment; filename="batchFile.csv"');
header("Content-Length: " . filesize("data.csv"));
$fp = fopen('php://output', 'w');
foreach($result as $line){
    //$val = explode(",",$line);
    fputcsv($fp, $line);
}
fclose($fp);
// DB::select("DELETE FROM `csvDb` where `refId` = '$id'");
//return "inserted";
 
}

function deleteImport($id)
{
         DB::select("DELETE FROM `payimport` where `refId` = '$id'");
        DB::select("DELETE FROM `csvDb` where `refId` = '$id'");
        DB::select("DELETE FROM `ImportPayments` where `reference1` like '$id%'");
        DB::select("update `paylist` set status = 'pending' where `refId` = '$id'");
return redirect()->route('getImportlist');
}

function batchlist()
{
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $data = DB::select("select * from paylist order by id desc");
        return view('batchlist', ['data' => $data,
             'getRolesId' => $getRolesId


        ]);
}


function getImportlist()
{
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $data = DB::select("select * from payimport where status = 'approved' order by id desc");
        return view('payimportList', ['data' => $data,
             'getRolesId' => $getRolesId


        ]);
}

function updateImportStatus($id)
{
//echo $id;
DB::table('payimport')
            ->where('refId', $id)
            ->update([
                'status' => 'approved'
                ]); 
DB::select("update paylist set status = 'imported' where refId = '".$id."' ");
DB::select("update claimEvents set status = 'Success' where person = '".$id."' ");
DB::select("update rejected_payments set status = 'Success' where ref = '".$id."' ");

$userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $actionBy  = $user[0]->first_name . ' '.$user[0]->last_name;

        $section = 'Fund Administrator';
        $action = 'Imported Payment List status - Imported';
DB::table('auditFunds')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'batch'  => $id,
            'action' => $action,
            'userId' => $actionBy
        ]);
return redirect()->route('getImportlist');
}


function importPay()
{
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        return view('importPay', ['getRolesId' => $getRolesId


        ]);
}




function viewBatch($id)
{
        $myUser = Auth::user()->id;
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $getRolesId = $getRoles[0]->roleId;
        $data = DB::select("select * from paylist where refId = '$id'");
        $data1 = DB::select("select * from payInsert where refId = '$id'");
        return view('viewBatch', ['data' => $data,'data1' => $data1,
             'getRolesId' => $getRolesId


        ]);
}
















	function viewImportBatch($id)
	{

	$myreferences = $id; 
	$to_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Owner' and refId = '$id' ");
	$to_positive = (int) $to_positive[0]->mycount;
	$to_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Owner' and refId = '$id' ");
	$to_negative = (int) $to_negative[0]->mycount;
	$to_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Owner' and refId = '$id' ");
	$to_sum_positive = (int) $to_sum_positive[0]->mycount;
	$to_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Owner' and refId = '$id' ");
	$to_sum_negative = (int) $to_sum_negative[0]->mycount;
	$to_positive_total = $to_positive * $to_sum_positive;
	$to_negative_total = $to_negative * $to_sum_negative;


	$ta_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Driver' and refId = '$id' ");
	$ta_positive = (int) $ta_positive[0]->mycount;
	$ta_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Driver' and refId = '$id' ");
	$ta_negative = (int) $ta_negative[0]->mycount;
	$ta_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Taxi Driver' and refId = '$id' ");
	$ta_sum_positive = (int) $ta_sum_positive[0]->mycount;
	$ta_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Taxi Driver' and refId = '$id' ");
	$ta_sum_negative = (int) $ta_sum_negative[0]->mycount;
	$ta_positive_total = $ta_positive * $ta_sum_positive;
	$ta_negative_total = $ta_negative * $ta_sum_negative;

	$qm_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Queue Marshall' and refId = '$id' ");
	$qm_positive = (int) $qm_positive[0]->mycount;
	$qm_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Queue Marshall' and refId = '$id' ");
	$qm_negative = (int) $qm_negative[0]->mycount;
	$qm_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Queue Marshall' and refId = '$id' ");
	$qm_sum_positive = (int) $qm_sum_positive[0]->mycount;
	$qm_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Queue Marshall' and refId = '$id' ");
	$qm_sum_negative = (int) $qm_sum_negative[0]->mycount;
	$qm_positive_total = $qm_positive * $qm_sum_positive;
	$qm_negative_total = $qm_negative * $qm_sum_negative;

	$rm_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Rank Marshall' and refId = '$id' ");
	$rm_positive = (int) $rm_positive[0]->mycount;
	$rm_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Rank Marshall' and refId = '$id' ");
	$rm_negative = (int) $rm_negative[0]->mycount;
	$rm_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Rank Marshall' and refId = '$id' ");
	$rm_sum_positive = (int) $rm_sum_positive[0]->mycount;
	$rm_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Rank Marshall' and refId = '$id' ");
	$rm_sum_negative = (int) $rm_sum_negative[0]->mycount;
	$rm_positive_total = $rm_positive * $rm_sum_positive;
	$rm_negative_total = $rm_negative * $rm_sum_negative;

	$as_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Association Staff' and refId = '$id' ");
	$as_positive = (int) $as_positive[0]->mycount;
	$as_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Association Staff' and refId = '$id' ");
	$as_negative = (int) $as_negative[0]->mycount;
	$as_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Association Staff' and refId = '$id' ");
	$as_sum_positive = (int) $as_sum_positive[0]->mycount;
	$as_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Association Staff' and refId = '$id' ");
	$as_sum_negative = (int) $as_sum_negative[0]->mycount;
	$as_positive_total = $as_positive * $as_sum_positive;
	$as_negative_total = $as_negative * $as_sum_negative;


	$o_positive = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Other' and refId = '$id' ");
	$o_positive = (int) $o_positive[0]->mycount;
	$o_negative = DB::select("SELECT count(*) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Other' and refId = '$id' ");
	$o_negative = (int) $o_negative[0]->mycount;
	$o_sum_positive = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Successful' and `taxiType` = 'Other' and refId = '$id' ");
	$o_sum_positive = (int) $o_sum_positive[0]->mycount;
	$o_sum_negative = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `status` = 'Disputed' and `taxiType` = 'Other' and refId = '$id' ");
	$o_sum_negative = (int) $o_sum_negative[0]->mycount;
	$o_positive_total = $o_positive * $ta_sum_positive;
	$o_negative_total = $o_negative * $ta_sum_negative;


	$main_positive_total = $to_positive_total + $ta_positive_total + $qm_positive_total + $rm_sum_positive + $as_positive_total + $o_positive_total;
	$main_negative_total = $to_negative_total + $ta_negative_total + $qm_negative_total + $rm_sum_negative + $as_negative_total + $o_negative_total;
	$total_positive_count = $to_positive + $ta_positive + $as_positive + $rm_positive + $qm_positive + $o_positive;
	$total_negative_count = $to_negative + $ta_negative + $qm_negative + $rm_negative + $as_negative + $o_negative;
	$Authorisation_cancelled = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` = 'Authorisation cancelled' and refId = '$id' ");
	$Authorisation_cancelled = (int) $Authorisation_cancelled[0]->mycount;
	$no_authority = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` = 'No authority to debit' and refId = '$id' ");
	$no_authority = (int) $no_authority[0]->mycount;
	$contravention = DB::select("SELECT count(*) as mycount FROM csvDb where `reason` IN (\"Debit in contravention of payer's authority\" , 'Debit in contravention of payer\'s authority') and refId = '$id' ");
	$contravention = (int) $contravention[0]->mycount;



	$Authorisation_cancelled_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` = 'Authorisation cancelled' and refId = '$id' ");
	$Authorisation_cancelled_amount = (int) $Authorisation_cancelled_amount[0]->mycount;
	$no_authority_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` = 'No authority to debit' and refId = '$myreferences' ");
	$no_authority_amount = (int) $no_authority_amount[0]->mycount;
	$contravention_amount = DB::select("SELECT SUM(amount) as mycount FROM csvDb where `reason` IN (\"Debit in contravention of payer's authority\" , 'Debit in contravention of payer\'s authority') and refId = '$id' ");
	$contravention_amount = (int) $contravention_amount[0]->mycount;

	 



	//echo $myreferences; die();
	$myUser = Auth::user()->id;
			$getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
			$getRolesId = $getRoles[0]->roleId;
			$data = DB::select("select * from paylist where refId = '$id'");
	//var_dump($myreferences); die();
			$data1 = DB::select("select * from payInsert where refId = '$id'");
			return view('viewbatch3', ['data' => $data,'data1' => $data1,
				 'getRolesId' => $getRolesId,
				 'main_positive_total' => $main_positive_total,
				 'main_negative_total' => $main_negative_total,
				 'total_positive_count' => $total_positive_count,
				 'total_negative_count' => $total_negative_count,
				 'contravention' => $contravention,
				 'Authorisation_cancelled_amount' => $Authorisation_cancelled_amount,
				 'no_authority_amount' => $no_authority_amount,
				 'contravention_amount' => $contravention_amount,
				 'no_authority' => $no_authority,
				 'Authorisation_cancelled' => $Authorisation_cancelled,
                                 'to_positive' => $to_positive,
                                 'ta_positive' => $ta_positive,
                                 'rm_positive' => $rm_positive,
                                 'qm_positive' => $qm_positive,
                                 'as_positive' => $as_positive,
                                 'o_positive' => $o_positive,
                                 'to_negative' => $to_negative,
                                 'ta_negative' => $ta_negative,
                                 'rm_negative' => $rm_negative,
                                 'qm_negative' => $qm_negative,
                                 'as_negative' => $as_negative,
                                 'o_negative' => $o_negative,




			]);
	}



}
?>