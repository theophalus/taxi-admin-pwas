<?php

namespace App\Http\Controllers;

use App\ApprovalStatus;
use App\BankAccount;
use App\Claimant;
use App\Payment;
use App\Register;
use App\TaxiAssoc;
use App\TaxiAssociationMember;
use App\TaxiBeneficiary;
use App\TaxiMember;
use App\TaxiUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use DB;

class LogsinController extends Controller
{
    public function index()
    {
session_start();
if(isset($_SESSION['type']))
{
$myUser = Auth::user()->id;
        $userId = Auth::user()->id;
        $user =DB::select("select * from users where id = '$userId'");
        $getRoles = DB::select("select * from dummy_permissions where userId = '$myUser'");
        $roleId = $getRoles[0]->roleId;
        $getRoleName = DB::select("select * from roles where id = '$roleId'");
        $role = $getRoleName[0]->name;

         // add login logs to audit db
        $section = 'Logout';
        $action = 'Logged out';
        DB::table('audit')->insert([
            'id' => null,
            'date' => DB::raw('now()'),
            'section' => $section,
            'action' => $action . ' ('.$_SESSION['type'].' - '.$_SESSION['taxiName'].')',
            'userId' => Auth::user()->id,
            'full_name' => $_SESSION['taxiMan'],
]);
	}
Auth::logout();
		$data = "nothng";
		return view('auth.claimId', ['data' => $data]);
	}
 
	public function idLogin(Request $request)
	{
//Auth::logout();
if(!isset($_POST['idNumber']))
{
return redirect()->route('claimantlogin');
}
		$idNumber = $request->idNumber;
		$datas = DB::select("SELECT taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_users.idNum = '$idNumber' and taxi_assocs.status = 1 order by taxi_assocs.date desc limit 0,1");
		if(!empty($datas))
		{
			$cell = $datas[0]->cell;
			$url = 'https://app.ticrf.co/sms/sendSMS.php';
			$cellFormated = substr($cell, 1, 9);
			//Initiate cURL
			$curl = curl_init($url); 
			$data = array(
				'cell' => $cellFormated
			);
			//Set the Content-Type to text/xml.
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$result = curl_exec($curl);
			$otp = $result;
                        $myIdNumber = $datas[0]->idNum;
                        DB::select("delete from claimOtp where idNum = '$myIdNumber'");
			DB::table('claimOtp')->insert(['id' => null,'idNum' => $myIdNumber, 'otp' => $otp]);
			return view('auth.claimOtp', ['idNum' => $datas[0]->idNum]);
		}
		else
		{
			return redirect()->route('claimantlogin'); //return view('auth.claimId', ['idError' => 'wrong']);
		}

	}

 

	public function otpLogin(Request $request)
	{

if(!isset($_POST['idNum']))
{
return redirect()->route('claimantlogin'); 
}
$otp = $request->otp;
                $idNum = $request->idNum;
//print_r("<pre"); var_dump($_POST); die();

		
		$data = DB::select("SELECT * from claimOtp where idNum = '$idNum' and otp = $otp");
		//print_r("<pre"); var_dump($data); die();
		 
                 
		if(!empty($data))
		{
                 $datas = DB::select("SELECT taxi_assocs.name as taxiName, taxi_users.name as personName,taxi_assocs.id as taxiassocId, taxi_users.*, taxi_assocs.*, taxi_members.* FROM taxi_users INNER JOIN taxi_assocs ON taxi_users.userId = taxi_assocs.userId INNER JOIN taxi_members ON taxi_users.userId = taxi_members.userId where taxi_users.idNum = '$idNum' and taxi_assocs.status = 1 order by taxi_assocs.date desc limit 0,1");
		session_start();
                
		$_SESSION['taxiMan'] = $datas[0]->personName." ".$datas[0]->surname;
                $_SESSION['taxiName'] = $datas[0]->taxiName;
                $_SESSION['province'] = $datas[0]->province;
                $_SESSION['town'] = $datas[0]->town;
                $_SESSION['type'] = $datas[0]->type;

if (Auth::attempt([ 'email' => 'taxiuser@gmail.com', 'password' => 'password'
])) {
            // Authentication passed...
           return redirect()->route('home');
        }

		//return view('auth.enterDen', ['idError' => 'wrong']);		
		}
               else
               {
Auth::logout();
			//return view('auth.claimOtpCheck', ['idNum' => $request->idNum]);
$data = "nothng";
		return view('auth.claimId', ['data' => $data]);
               }
 

	}

	public function enterDen()
	{
		session_start();

		$_SESSION['taxiMan'] = "yebo";
		return view('auth.enterDen', ['idError' => 'wrong']);

	}
}
?>