<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxiMember extends Model
{
    protected $fillable = [
        'user_id', 'licenseNo', 'pnrLicense',
    ];

    protected $casts = [
        'pnrLicenseExpiryDate' => 'datetime',
        'licenseExpiryDate' => 'datetime',
    ];
}
