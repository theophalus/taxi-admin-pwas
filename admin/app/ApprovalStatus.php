<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovalStatus extends Model
{
    protected $fillable = [
        'user_id', 'taxi_user_id', 'claimant_id', 'register_id', 'taxi_assoc_id', 'taxi_member_id',
        'beneficiary_id', 'status_reason', 'status',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
}
