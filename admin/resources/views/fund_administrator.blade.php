<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        .text-shw{
            color: white;
            font-weight: bold;
            text-shadow: 2px 2px 4px #000000;
        }

        /*END OF Side Menu*/

        .custom-box {
            box-shadow: 6px 10px 8px #888888;
        }

        .bg-dark:hover{
            background-color: green;
        }
    </style>

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li><!--Lougout -->
                    <div class="" >
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div></li>
            </ul>
        </nav>
        <!-- /.navbar -->
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
        <!-- Brand Logo -->
        <a href="" class="brand-link side-nav-logo">
            <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
            <span class="brand-text font-weight-light text-center">

                <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
                <h4 class="text-shw">{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
            </span>
            <br/>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                </div>
                <div class="info text-bold">
                    <a href="#" class="d-block">
                        {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
        @include('layouts.nav')
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h3><strong>Fund Administrator</strong></h3>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="https://app.ticrf.co/admin/public/home">Home</a></li>
                            <li class="breadcrumb-item active">Fund Administrator</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                        @endif
                        <!-- Posts list -->
                            <div class="row">

                            </div>
                            <!-- Small boxes (Stat box) -->
                            <div class="row">
                                <div class="col-lg-2 col-6"></div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules custom-box">
                                            <h4 class="text-center">All Taxi Associations</h4>
                                            <h4 class="text-center">{{ $taxi_assoc_count }}</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules custom-box">
                                            <h4 class="text-center">Approved Associations</h4>
                                            <h4 class="text-center">{{ $taxi_assoc_count }}</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-2 col-6"></div>
                            </div>
                            <!-- /.row -->

                            <!-- Small boxes (Stat box) -->
                            <div class="row">
                                <div class="col-lg-2 col-6"></div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules custom-box">
                                            <h4 class="text-center">Pending Associations</h4>
                                            <h4 class="text-center">{{ $taxi_assoc_count }}</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules custom-box">
                                            <h4 class="text-center">Declined Associations</h4>
                                            <h4 class="text-center">{{ $taxi_assoc_count }}</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-6"></div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card -->

                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->

            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript">
    $("document").ready(function(){
        setTimeout(function(){
            $("div.alert").fadeOut();
        }, 5000 ); // 5 secs

    });
</script>
</body>
</html>
