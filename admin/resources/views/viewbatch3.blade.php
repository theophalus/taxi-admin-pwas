<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    {{--<link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">--}}
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        .text-shw{
            color: white;
            font-weight: bold;
            text-shadow: 2px 2px 4px #000000;
        }

        /*END OF Side Menu*/

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
		#calc
		{
			display: none;
		}
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed" style="padding: 0px !important; margin: 0px !important;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <a class="dropdown-item" href="{{ route('taxi_assocs_home') }}">
                        Home
                    </a>
                </li>
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->




        <!-- Navbar
<nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
 Left navbar links
    <ul class="navbar-nav">
        <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Payments</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Transactions</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Audit Trails</a>
        </li>
    </ul>
-->
        <!-- Right navbar links
    <ul class="navbar-nav ml-auto">

    </ul>
</nav>
s/.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link side-nav-logo">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
  <?php
session_start();
?>   
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;"><?php if(isset($_SESSION['taxiMan'])){ echo $_SESSION['taxiMan']; }else{?> {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <?php }?></h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
            @include('layouts.nav')
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="background-color: #ffffff;">


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach()
                    </div>
                    @endif



                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">
<a class="btn btn-default" onclick="goBack()">
                                Back
                            </a>
                            <div class="col-lg-12 mt-2 main-content" style="font-size: 12px;">
<br/>
        


								 
							  <div id="calc" style="font-size: 11px;display:block;">

<h2>Batch Number: <?php echo $data[0]->refId; ?></h2>
<table border='0'>
<tr>
		<td><b>Date selection</b></td>
		<td><?php echo $data[0]->fromDate; ?></td>
                <td><?php echo $data[0]->toDate; ?></td>
</tr>
<tr>
		<td  width="220"><b>Number of transactions</b></td>
		<td width="120"><?php $totalsAmount = $total_positive_count + $total_negative_count; echo $totalsAmount; ?></td>
                <td>&nbsp;</td>
</tr>
<tr>
		<td><b>Total value of payments</b></td>
		<td>R <?php $totalsAmount1 = $main_positive_total + $main_negative_total; echo $totalsAmount1; ?></td>
                <td></td>
</tr>
</table>
<br/>
<table border='0'>
<tr>
		<td></td>
		<td><b>Volume</b></td>
                <td><b>Value<b></td>
</tr>
<tr>
		<td width="220"><b>Authorisation cancelled</b></td>
		<td width="120"><?php echo $Authorisation_cancelled; ?></td>
                <td>R <?php echo $Authorisation_cancelled_amount; ?></td>
</tr>
<tr>
		<td><b>No authority to debit</b></td>
		<td><?php echo $no_authority; ?></td>
                <td>R <?php echo $no_authority_amount; ?></td>
</tr>
<tr>
		<td><b>Debit in contravention of payer's authority</b></td>
		<td><?php echo $contravention; ?></td>
                <td>R <?php echo $contravention_amount; ?></td>
</tr>
</table>
<br/>
<table border='0'>
<tr>
		<td></td>
		<td><b>Volume</b></td>
                <td><b>Value</b></td>
</tr>
<tr>
		<td width="220"><b>Successful Payments</b></td>
		<td width="120"><?php echo $total_positive_count; ?></td>
                <td>R <?php echo $main_positive_total; ?></td>
</tr>
<tr>
		<td><b>Failed Payments</b></td>
		<td><?php echo $total_negative_count; ?></td>
                <td>R <?php echo $main_negative_total; ?></td>
</tr>
</table>
<br/>
<table border='1' style="box-shadow: 1px 1px 10px #000;">
<tr>
		<td align="center" width="100"><b>Claimant Roles</b></td>
		<td align="center"><b>Number of Verified Claimants</b></td>
		<td align="center"><b>Amount per day</b></td>
		<td align="center"><b>Number of Days</b></td>
		<td align="center"><b>Total Pay-Away Amount per Verified Claimant</b></td>
		<td align="center"><b>Total per Claimant Role (Successful)</b></td>
		<td align="center"><b>Total per Claimant Role (Failed)</b></td>
		<td align="center"><b>Over all total per Claimant Role</b></td>
	</tr>
<?php
$ar_positive = array();
$ar_positive[] = $to_positive;
$ar_positive[] = $ta_positive;
$ar_positive[] = $qm_positive;
$ar_positive[] = $as_positive;
$ar_positive[] = $rm_positive;
$ar_positive[] = $o_positive;

$ar_negative = array();
$ar_negative[] = $to_negative;
$ar_negative[] = $ta_negative;
$ar_negative[] = $qm_negative;
$ar_negative[] = $as_negative;
$ar_negative[] = $rm_negative;
$ar_negative[] = $o_negative;
$groupTotal = "";
$totalMainCharges = "";
$totalMains = "";
$totalMains_positive = 0;
$totalMains_negative = 0;
$count = 0;
$had = array();
foreach($data1 as $datas)
{
$had[] = $datas->amountPer;
?>

<tr>
		<td align="center"><?php echo $datas->role; ?></td>
		<td align="center"><?php echo $datas->claimantNo; ?></td>
		<td align="center"><?php echo $datas->noPerDays; ?></td>
		<td align="center"><?php echo $datas->days; ?></td>
		<td align="center">R  <?php echo $datas->amountPer; ?></td>
		<td align="center">R <?php $amount = (float) $datas->amountPer; $mycount = (int) $ar_positive[$count]; $getTotals = $amount * $mycount; echo $getTotals; ?></td>
		<td align="center">R <?php $amount1 = (float) $datas->amountPer; $mycount1 = (int) $ar_negative[$count]; $getTotals1 = $amount1 * $mycount1; echo $getTotals1; ?></td>
		<td align="center">R <?php echo $datas->totalPerRole; ?></td>
	</tr>
<?php
$count++;
$groupTotal = $datas->groupTotal;
$totalMains_positive += $getTotals;
$totalMains_negative += $getTotals1;
$totalMains = $datas->totalMains;
}

?>
<tr>
		<td align="center"><b>Total Amount:</b></td>
		<td align="center"><b><?php echo $groupTotal; ?></b></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center"><b>R  <?php echo $totalMains_positive; ?></b></td>
		<td align="center"><b>R  <?php echo $totalMains_negative; ?></b></td>
		<td align="center"><b>R <?php echo $totalMains; ?></b></td>
	</tr>
	</table><br><br><br>						  
</div>
                            </div>
                            <!-- /.card -->
<br><br><br>
                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->


        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>


    <script>
        // data tables with export buttons
        $(document).ready(function() {
            $('#myTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "order": [[ 0, 'desc' ]],
                "info": true,
                "autoWidth": true,
                "pageLength": 25
            });
        });
    </script>


    <!-- PAGE SCRIPTS -->
    {{--<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>--}}
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
	/*
	    var xhr = new XMLHttpRequest(); 
        var data = new FormData(); 
		var tokens = $('meta[name="csrf-token"]').attr('content');
        data.append('_token', tokens);
        xhr.open('POST', 'https://app.ticrf.co/admin/public/importTable', true);
        xhr.onprogress = function () {
        };
        xhr.onload = function () {
            //alert(SessionID); 
            document.getElementById("calc").innerHTML = this.responseText;				
        };
        xhr.send(data);
		*/
        function goBack() {
            window.history.back();
        }
		function generateSummary()
	{
	    document.getElementById('calc').style.display = "block";
		var fromDate = document.getElementById('fromDate').value;
		var toDate = document.getElementById('toDate').value;
		var xhr = new XMLHttpRequest(); 
        var data = new FormData(); 
		var tokens = $('meta[name="csrf-token"]').attr('content');
        data.append('_token', tokens);
        data.append('fromDate', fromDate);
		data.append('toDate', toDate);
		data.append('bankCharges', document.getElementById('bankCharges').value);
		if(document.getElementById('taxiOwnerAmountCheck').checked)
		{
		 data.append('taxiOwnerAmount', document.getElementById('taxiOwnerAmount').value);
		}
		if(document.getElementById('taxiasscAmountCheck').checked)
		{
		 data.append('taxiasscAmount', document.getElementById('taxiasscAmount').value);
		}
		if(document.getElementById('qeueMarshalAmountCheck').checked)
		{
		 data.append('qeueMarshalAmount', document.getElementById('qeueMarshalAmount').value);
		}
		if(document.getElementById('qeueMarshalAmountCheck').checked)
		{
		 data.append('qeueMarshalAmount', document.getElementById('qeueMarshalAmount').value);
		}
		if(document.getElementById('assocStaffAmountCheck').checked)
		{
		 data.append('assocStaffAmount', document.getElementById('assocStaffAmount').value);
		}
		if(document.getElementById('rankMarshallAmountCheck').checked)
		{
		 data.append('rankMarshallAmount', document.getElementById('rankMarshallAmount').value);
		}
		if(document.getElementById('otherAmountCheck').checked)
		{
		 data.append('otherAmount', document.getElementById('otherAmount').value);
		}
		xhr.open('POST', 'https://app.ticrf.co/admin/public/importTable', true);
        xhr.onprogress = function () {
        };
        xhr.onload = function () {
            //alert(SessionID); 
            document.getElementById("calc").innerHTML = this.responseText;				
        };
        xhr.send(data);
		
	}
	
	
	
	
	
	
	function generateExcel()
	{
	alert();
	    document.getElementById('calc').style.display = "block";
		var fromDate = document.getElementById('fromDate').value;
		var toDate = document.getElementById('toDate').value;
		var xhr = new XMLHttpRequest(); 
        var data = new FormData(); 
		var tokens = $('meta[name="csrf-token"]').attr('content');
        data.append('_token', tokens);
        data.append('fromDate', fromDate);
		data.append('toDate', toDate);
		data.append('bankCharges', document.getElementById('bankCharges').value);
		data.append('taxiOwnerAmount', document.getElementById('taxiOwnerAmount').value);
		data.append('taxiasscAmount', document.getElementById('taxiasscAmount').value);
		data.append('qeueMarshalAmount', document.getElementById('qeueMarshalAmount').value);
		data.append('qeueMarshalAmount', document.getElementById('qeueMarshalAmount').value);
		data.append('assocStaffAmount', document.getElementById('assocStaffAmount').value);
		data.append('rankMarshallAmount', document.getElementById('rankMarshallAmount').value);
		data.append('otherAmount', document.getElementById('otherAmount').value);
		data.append('actionedDate', document.getElementById('actionedDate').value);
		xhr.open('POST', 'https://app.ticrf.co/admin/public/importTableinsert', true);
        xhr.onprogress = function () {
		   document.getElementById("excelSend").innerHTML = "<h1>Loading....</h1>";
        };
        xhr.onload = function () {
            //alert(SessionID); 
            location.href = "https://app.ticrf.co/admin/public/batchlist";				
        };
        xhr.send(data);
		
	}
    </script>
</body>

</html>