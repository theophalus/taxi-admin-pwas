<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
     with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview" style="padding-top: 12px;">
            <br /><br />
            <?php if(($getRolesId === 2) || ($getRolesId === 1) || ($getRolesId === 5) || ($getRolesId === 4)){?>
            <a href="{{ route('taxi_assocs_home')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-bus"></i>
                <p class="menu-items">
                    Taxi Associations
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>

            <?php if(($getRolesId === 3) || ($getRolesId === 1) || ($getRolesId === 4) || ($getRolesId === 5)){?>
            <a href="{{ route('claimants')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-users"></i>
                <p class="menu-items">
                    Claimants
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>
            <?php if(($getRolesId === 5) || ($getRolesId === 1)){?>
            <a href="{{ route('paymentHome')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-hand-holding-usd"></i>
                <p class="menu-items">
                    Payments
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>
            <?php if (($getRolesId === 4) || ($getRolesId === 1) || ($getRolesId === 5)) { ?>
        <!-- <a href="{{ route('query_management')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Query Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a> -->
            <?php } ?>

            <?php if ($getRolesId === 1) { ?>
            <a href="{{ route('user_management')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-user-cog"></i>
                <p class="menu-items">
                    User Management
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>
            <?php if ($getRolesId === 1) { ?>
            <a href="{{ route('users')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-user-cog"></i>
                <p class="menu-items">
                    View Users
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>
            <?php if ($getRolesId === 1 || $getRolesId === 5) { ?>
            <a href="{{ route('audit')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-scroll"></i>
                <p class="menu-items">
                    Audit Trail
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <a href="{{ route('report')  }}" class="nav-link active side-menu-items">
                <i class="nav-icon fas fa-sticky-note"></i>
                <p class="menu-items">
                    Reporting
                    <i class="right fas fa-angle-right"></i>
                </p>
            </a>
            <?php } ?>
            <br /><br /><br />
            <ul class="nav nav-treeview">

                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Home
                        </p>
                    </a>
                </li>
            </ul>
</nav>