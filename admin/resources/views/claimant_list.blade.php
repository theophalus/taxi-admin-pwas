<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>  <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    {{--<link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">--}}
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        /*END OF Side Menu*/

        .custom-box {
            box-shadow: 6px 10px 8px #888888;
        }

        .brand-link-logo {
            display: block;
            height: calc(5.5rem + 1px) !important;
            margin-top: 15px;
            margin-bottom: 35px;
            font-size: 1.25rem;
            line-height: 1.5;
            padding: 0.8125rem 0.5rem;
            transition: width 0.3s ease-in-out;
            white-space: nowrap;
        }

        .brand-link-logo ul li {
            display: inline-block;
            list-style: none;
        }

        .brand-link-logo img {
            width: 125px;
            margin-top: -5px;
            margin-left: 25px;
        }

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Payments</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Transactions</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Audit Trails</a>
                </li>
            </ul>

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="" class="nav-link">Admin Portal</a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li>
                        <!--Lougout -->
                        <div class="">
<?php if(isset($_SESSION['taxiMan'])){?>
                            <a class="dropdown-item" href="{{ route('claimantlogin') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('claimantlogin') }}" method="get" style="display: none;">
                            </form>
<?php } else{?>

  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
<?php } ?>
                        </div>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link side-nav-logo">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
 
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;"><?php if(isset($_SESSION['taxiMan'])){ echo $_SESSION['taxiMan']; }else{?> {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <?php }?></h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
            @include('layouts.nav')
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-6">
                            <a class="btn btn-default" onclick="goBack()">
                                Back
                            </a>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('claimants') }}">Claimants</a></li>
                                <li class="breadcrumb-item active">Claimants</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-12">
                                    <h3><strong>{{ $label }} <?php if(isset($_SESSION['taxiMan'])){ echo $_SESSION['taxiName']." Association"; }?></strong></h3>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2 main-content">
                                @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                                @endif
                                <!-- Posts list -->
                                <div class="row" style="background-color: #ffffff;">

                                </div>
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-12 col-6">
                                        <!-- CHECK IF CLAIMANTS EXIST -->

                                        <!-- ROW -->
                                        <div class="row bx-shw">
                                            <!-- COL -->
                                            <div class="col-xs-12 col-sm-12 col-md-12" style="padding-right: 0px; padding-left: 0px;">
                                                <!-- TABLE -->
                                                <table class="table table-striped task-table" id="myTable" style="width: 100%;">
                                                    <thead class="text-center cust-table-theads">
                                                        <th width="14.11%">Date Created</th>
                                                        <th width="11.11%">Association</th>
                                                        <th width="11.11%">Role</th>
                                                        <th width="11.11%">First Name</th>
                                                        <th width="11.11%">Surname</th>
                                                        <th width="11.11%">Cell Number</th>
                                                        <!-- <th width="14.11%">Email</th> -->
                                                        <!-- <th width="9.11%">Gender</th> -->
                                                        <th width="9.11%">Status</th>
                                                        <th width="9.11%">Action</th>
                                                    </thead>
                                                    <!-- END TABLE HEADING -->

                                                    <!-- TABLE BODY --> 
                                                   
                                                    <tbody class="text-center">

                                                        <!-- CLAIMANTS EXIST THEN TRAVERSE THE LOOP AND LIST CLAIMANTS -->
                                                        <?php foreach($claimants as $claimant){ ?>

                                                        <!-- TABLE ROW -->
                                                        <tr>
                                                            <td class="table-text">
                                                                <div>{{$claimant->created_at}}</div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div>{{ ucwords($claimant->taxiName) }}</div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div>{{$claimant->title}}</div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div><?php echo ucwords($claimant->first_name); ?></div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div><?php echo ucwords($claimant->last_name); ?></div>
                                                            </td>
                                                            <td class="table-text">
                                                                <div>{{$claimant->cellphone_number}}</div>
                                                            </td>
                                                           <!-- <td class="table-text">
                                                                <div>{{$claimant->email}}</div>
                                                            </td> 
                                                            <td class="table-text">
                                                                <div>{{$claimant->gender}}</div>
                                                            </td>-->
                                                            <td>
                                                               <?php if($claimant->application_status == 0){?>
                                                                    <i class="far fa-question-circle  text-warning" style="margin-left: 15px; font-size: 1.3rem"></i>
<i class="far fa-question-circle  text-warning" style="margin-left: 15px; font-size: 1.3rem"></i>
   <?php }?>                                                             
<?php if($claimant->application_status == 1){?>
<i class="fas fa-check text-success" style="margin-left: 15px; font-size: 1.3rem"></i>
<i class="far fa-question-circle  text-warning" style="margin-left: 15px; font-size: 1.3rem"></i>
                                                              <?php }?>      
                                                               <?php if($claimant->application_status == 2){?>
                                                                    <!-- <i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i> -->
<i class="fas fa-check text-success" style="margin-left: 15px; font-size: 1.3rem"></i>
<i class="fas fa-check text-success" style="margin-left: 15px; font-size: 1.3rem"></i>
                                                               <?php }?>
<?php if($claimant->application_status == 3){?>
                                                                    <!-- <i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i> -->
<i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i>
<i class="far fa-question-circle  text-warning" style="margin-left: 15px; font-size: 1.3rem"></i>
                                                              <?php }?>
<?php if($claimant->application_status == 4){?>
                                                                    <!-- <i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i> -->
<i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i>
<i class="fas fa-times text-danger" style="margin-left: 15px; font-size: 1.3rem"></i>
                                                                <?php }?>
                                                            </td>
                                                            <td>
                                                                <?php if($claimant->application_status == 0){?>
                                                                    <a href="{{ route('claimant_pending_detail', $claimant->claimantId) }}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                        <i class="fas fa-eye"></i>
                                                                    </a>
<?php }?>
                                                                <?php if($claimant->application_status == 1){?>
                                                                     <a href="{{ route('claimant_half_approved_detail', $claimant->claimantId) }}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                        <i class="fas fa-eye"></i>
                                                                    </a>
<?php }?>
<?php if($claimant->application_status == 2){?>
                                                                    <a href="{{ route('claimant_full_approved', $claimant->claimantId) }}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                        <i class="fas fa-eye"></i>
                                                                    </a>
<?php }?>
                                                                <?php if($claimant->application_status == 3){?>
                                                                    <a href="{{ route('claimant_half_declined_detail', $claimant->claimantId) }}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                        <i class="fas fa-eye"></i>
                                                                    </a>
                                                                <?php }?>
<?php if($claimant->application_status == 4){?>
                                                                    <a href="{{ route('claimant_full_declined_detail', $claimant->claimantId) }}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
                                                                        <i class="fas fa-eye"></i>
                                                                    </a>
<?php }?>
                                                              
                                                            </td>
                                                        </tr>
                                                        <!-- TABLE ROW -->

                                                        <?php } ?>

                                                    </tbody>
                                                    <!-- TABLE BODY -->

                                                   
                                                    <!-- END IF -->
                                                </table>
                                                <!-- TABLE -->

                                            </div>
                                            <!-- END COL -->

                                        </div>
                                        <!-- END ROW -->

                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card -->

                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->


    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>


    <script>
        // data tables with export buttons
        $(document).ready(function() {
            $('#myTable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "order": [[ 0, 'desc' ]],
                "info": true,
                "pageLength": 25
            });
        });
    </script>


    <!-- PAGE SCRIPTS -->
    {{--<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>--}}
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>

</html>
