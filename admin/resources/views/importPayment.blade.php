<!DOCTYPE html>
<html>
<body>

<h2>Text input fields</h2>

<form action="https://app.ticrf.co/admin/public/importFormFiles" method="post" enctype="multipart/form-data">
@csrf
  <label for="fname">Upload File:</label><br>
  <input type="file" id="fname" name="fname"><br>
  <input type="submit"value="Submit File">
</form>

<p>Note that the form itself is not visible.</p>

<p>Also note that the default width of text input fields is 20 characters.</p>

</body>
</html>