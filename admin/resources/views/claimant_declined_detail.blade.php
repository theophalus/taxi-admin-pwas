<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        /*END OF Side Menu*/

        .custom-box {
            box-shadow: 6px 10px 8px #888888;
        }

        .brand-link-logo {
            display: block;
            height: calc(5.5rem + 1px) !important;
            margin-top: 15px;
            margin-bottom: 35px;
            font-size: 1.25rem;
            line-height: 1.5;
            padding: 0.8125rem 0.5rem;
            transition: width 0.3s ease-in-out;
            white-space: nowrap;
        }

        .brand-link-logo ul li {
            display: inline-block;
            list-style: none;
        }

        .brand-link-logo img {
            width: 125px;
            margin-top: -5px;
            margin-left: 25px;
        }

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }

        table {
            width: 100%;
        }

        table th {
            width: 25%;
        }

        table td {
            width: 75%;
        }

        .user-details {
            margin-left: 25px;
        }

        .rec-account-info {
            background-color: #ddd;
        }

        .rec-account-info h4 {
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            </ul>

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="" class="nav-link">Admin Portal</a>
                    </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li>
                        <!--Lougout -->
                        <div class="">
                        <?php if(isset($_SESSION['taxiMan'])){?>
                            <a class="dropdown-item" href="{{ route('claimantlogin') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('claimantlogin') }}" method="get" style="display: none;">
                            </form>
<?php } else{?>

  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
<?php } ?>
                        </div>
                    </li>
                </ul>
            </nav>
            <!-- /.navbar -->
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->

        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link side-nav-logo">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
     style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;">{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style="padding-top: 12px;">
                            <br /><br />
                            <a href="{{ route('taxi_assocs_home')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Taxi Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>

                            <a href="{{ route('claimants')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Claimants
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>


                            <a href="{{ route('fund_administrator')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                                <p class="menu-items">
                                    Fund Administrator
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>

                            <a href="{{ route('user_management')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p class="menu-items">
                                    User Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>

                            <a href="{{ route('users')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p class="menu-items">
                                    View Users
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>

                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-lg-6">
                            <a class="btn btn-default" onclick="goBack()">
                                Back
                            </a>
                            <a class="btn btn-success text-white">
                                Approve
                            </a>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="https://app.ticrf.co/admin/public/home">Home</a></li>
                                <li class="breadcrumb-item"><a href="https://app.ticrf.co/admin/public/claimants">Claimants</a></li>
                                <li class="breadcrumb-item active">Declined Claimant</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <!-- Main content -->
                    @if(!empty($declined))
                    <section class="content">

                        @foreach($declined as $claimant)
                        <div class="container-fluid">
                            <div class="row mb-2">
                                <div class="col-lg-12">
                                    <h3><strong>{{ $claimant->first_name }} {{ $claimant->last_name }}</strong></h3>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2">
                                @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                                @endif
                                <!-- Posts list -->
                                <div class="row">

                                </div>
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-12 col-6">
                                        <!-- CHECK IF CLAIMANTS EXIST -->

                                        <!-- ROW -->
                                        <div class="row">
                                            <!-- COL -->
                                            <div class="col-xs-12 col-sm-4 col-md-4" style="padding-right: 0px; padding-left: 0px;">
                                                <div class="card bx-shw">
                                                    <img src="{{ asset($claimant->photo_location) }}" style="width:100%">
                                                </div>
                                            </div>
                                            <!-- END COL -->
                                            <!-- COL -->
                                            <div class="col-xs-12 col-sm-8 col-md-8" style="padding-right: 0px; padding-left: 0px;">
                                                <div class="card user-details bx-shw">
                                                    <!-- /.card-header -->
                                                    <div class="card-body table-responsive p-0">
                                                        <table class="table table-hover">
                                                            <tbody>
                                                                <tr>
                                                                    <th><span class="tag tag-success">First Name</span></th>
                                                                    <td>{{ $claimant->first_name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Surname</span></th>
                                                                    <td>{{ $claimant->last_name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Title</span></th>
                                                                    <td>{{ $claimant->title }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">ID Number</span></th>
                                                                    <td>{{ $claimant->id_number }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Email Address</span></th>
                                                                    <td>{{ $claimant->email }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Cellphone Number</span></th>
                                                                    <td>{{ $claimant->cellphone_number }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Gender</span></th>
                                                                    <td>{{ $claimant->gender }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">License Number</span></th>
                                                                    <td>{{ $claimant->license_number }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">PNR Number</span></th>
                                                                    <td>{{ $claimant->pnr_number }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">City</span></th>
                                                                    <td>{{ $claimant->city }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Province</span></th>
                                                                    <td>{{ $claimant->province }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th><span class="tag tag-success">Account Used</span></th>
                                                                    <td>{{ $claimant->account_used }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <!-- /.card-body -->
                                                </div>
                                                <!-- /.card -->
                                            </div>
                                            <!-- END COL -->
                                        </div>
                                        <!-- END ROW -->

                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <!-- COL -->
                                    <div class="col-xs-12 col-sm-4 col-md-4" style="padding-right: 0px; padding-left: 0px;">
                                        <div class="card"></div>
                                    </div>
                                    <!-- END COL -->
                                    <!-- COL -->
                                    <div class="col-xs-12 col-sm-8 col-md-8" style="padding-right: 0px; padding-left: 0px;">
                                        <div class="card user-details bx-shw">
                                            <!-- /.card-header -->
                                            <div class="card-body table-responsive p-0">
                                                <table class="table table-hover">
                                                    <tbody>
                                                        <tr class="rec-account-info">
                                                            <td colspan="2">
                                                                <h4>Receiving Account Information</h4>
                                                            </td>
                                                        </tr>
                                                        <!--HERE IS WHERE TO HIDE  -->
                                                        <tr>
                                                            <th><span class="tag tag-success">Name</span></th>
                                                            <td>{{ $claimant->receiver_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Surame</span></th>
                                                            <td>{{ $claimant->receiver_surname }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Email Address</span></th>
                                                            <td>{{ $claimant->receiver_email }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Phone Number</span></th>
                                                            <td>{{ $claimant->cell }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Gender</span></th>
                                                            <td>{{ $claimant->receiver_gender }}</td>
                                                        </tr>
                                                        <!--HERE IS WHERE TO HIDE  ends-->
                                                        <tr>
                                                            <th><span class="tag tag-success">Bank Name</span></th>
                                                            <td>{{ $claimant->bank_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Account Number</span></th>
                                                            <td>{{ $claimant->account_number }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Branch Code</span></th>
                                                            <td>{{ $claimant->branch_code }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Accout Type</span></th>
                                                            <td>{{ $claimant->account_type }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th><span class="tag tag-success">Application Date</span></th>
                                                            <td>{{ $claimant->created_at }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card -->

                            </div><!-- /.container-fluid -->
                            @endforeach
                    </section>
                    @endif
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>

</body>

</html>