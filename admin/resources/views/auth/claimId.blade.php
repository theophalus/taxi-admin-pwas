@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header <?php if(isset($idError)){ ?>alert alert-danger <?php } ?>" style="width: 100%;<?php if(isset($idError)){ echo "text-align:center"; } ?>"><?php if(isset($idError)){ ?> Wrong Id Number <?php }else{ ?>{{ __('Login') }} <?php } ?> </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('idLogin') }}">
                        @csrf
   

                <div class="form-group row">

                            <label for="email" class="col-md-4 col-form-label text-md-right">Id Number</label>

                            <div class="col-md-6">
                                <input id="idNumber" type="number" class="form-control" name="idNumber" value="" required>

                              
                            </div>
                        </div>




                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
