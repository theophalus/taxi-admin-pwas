@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header <?php if(isset($idError)){ ?>alert alert-danger <?php } ?>" style="width: 100%;<?php if(isset($idError)){ echo "text-align:center"; } ?>"><?php if(isset($idError)){ ?> Wrong Otp Number <?php }else{ ?>{{ __('Login') }} <?php } ?> </div>>

                <div class="card-body">
                    <form method="POST" action="{{ route('otpLogin') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Otp</label>

                            <div class="col-md-6">
                                <input id="otp" type="number" class="form-control @error('otp') is-invalid @enderror" name="otp" value="{{ old('email') }}" required autocomplete="email" autofocus>
								<input type="hidden" name="idNum" value="{{ $idNum }}" >
                                @error('otp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 




                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
