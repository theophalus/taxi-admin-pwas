<?php //var_dump($_POST); die(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
            cursor: pointer;
            transition: 0.3s;
        }

        .menu-items {
            font-size: 13px;
        }

        .text-shw {
            color: white;
            font-weight: bold;
            text-shadow: 2px 2px 4px #000000;
        }

        /*END OF Side Menu*/

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link side-nav-logo">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
<?php
session_start();
?>   
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;"><?php if(isset($_SESSION['taxiMan'])){ echo $_SESSION['taxiMan']; }else{?> {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <?php }?></h4>
                
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
            @include('layouts.nav')

            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h3><strong>User Management</strong></h3>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="https://app.ticrf.co/admin/public/home">Home</a></li>
                                <li class="breadcrumb-item active">User Management</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-8">
                                    {{--display messages--}}
                                    @if(Session::has('message'))
                                        <p class="alert alert-dismissible {{ Session::get('alert-class', 'alert-success ') }}">{{ Session::get('message') }}</p>
                                    @endif
                                </div>
                            </div>



                            <div class="row">



                                <div class="col-4">
                                    <form method="post" action="{{ route('create_user') }}">
                                        @csrf
                                        <div class="modal-content" >
                                            <div class="modal-header">
                                                <h4 class="modal-title">User Registration Information</h4>
                                            </div>
                                            <div class="modal-body">
                                            @if(!empty($roles))
                                                <!-- select -->
                                                    <div class="form-group">
                                                        <label>User Role</label>
                                                        <select class="form-control" name="role_id" id="role_id" required>
                                                            <option value="">Select User Roles</option>
                                                            @foreach($roles as $role)
                                                                <?php
                                                                /*
                                                                                                                    if ($role->name !== "Claimant admin") {
                                                                                                                    ?>
                                                                                                                        <option value="{{ $role->id }}" @if (old('role_id') == $role->id ) selected="selected" @endif>{{ $role->name }}</option>
                                                                                                                    <?php
                                                                                                                    }
                                                                */
                                                                ?>
                                                            @endforeach
                                                            <option value="1" @if (old('role_id') == "1" ) selected="selected" @endif>Super admin</option>
                                                            <option value="5" @if (old('role_id') == "5" ) selected="selected" @endif>Fund administrator</option>
                                                            <option value="2" @if (old('role_id') == "2" ) selected="selected" @endif>Taxi association admin</option>
                                                            <option value="4" @if (old('role_id') == "4" ) selected="selected" @endif>Query management agent</option>
                                                        </select>
                                                    </div>
                                                @endif
                                                <input type="text" name="first_name" class="form-control @error('first_name') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Firstname" value="{{ old('first_name') }}" required>
                                                <input type="text" name="last_name" class="form-control @error('last_name') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Surname" value="{{ old('last_name') }}" required>
                                                @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" style="margin-top: 10px;" autocomplete="off" placeholder="Email Address" value="{{ old('email') }}" rrequired>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                                <input id="password" type="password" style="margin-top: 10px;" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="off">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                @enderror
                                                <input id="password-confirm" type="password" style="margin-top: 10px;" placeholder="Re-password" class="form-control" name="password_confirmation" value="{{ old('password') }}" required autocomplete="off">
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <!--
                                            <input type="hidden" name="model_type" value="">
                                            <input type="hidden" name="model_id" value="">
                                            <input type="hidden" name="model_type" value="">
                                            -->
                                                <button type="submit" class="btn btn-primary">Add User</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>




                                <div class="col-8 bx-shw" style="background-color: #ffffff">
                                    <table class="table table-striped task-table">
                                        <!-- Table Headings -->
                                        <thead class=" cust-table-theads">
                                        <th>User Role</th>
                                        <th>Access</th>
                                        <th>Permissions</th>
                                        </thead>

                                        <!-- Table Body -->
                                        <tbody class="">
                                        <tr>
                                            <td class="table-text">
                                                <div>Super Admin</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Full System All Modules</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Edit All Fields</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-text">
                                                <div>Taxi Association Admin</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Full Taxi Association Module Access</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Edit All Taxi Association and Related Fields</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-text">
                                                <div>Claimant Admin</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Full Claimant Module Access</div>
                                            </td>
                                            <td class="table-text">
                                                <div>No Edit Function, Only Approve and Disapprove Functionality</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-text">
                                                <div>Query Management Agent</div>
                                            </td>
                                            <td class="table-text">
                                                <div>View Access Only Module</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Only View Access</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-text">
                                                <div>Fund Administer</div>
                                            </td>
                                            <td class="table-text">
                                                <div>Fund Administer Module Access</div>
                                            </td>
                                            <td class="table-text">
                                                <div>View All and Payments</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <!-- /.card -->





                            </div>




                        </div><!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>

    <script>
        // boot strap notification timer
        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 3000);
    </script>

</body>

</html>