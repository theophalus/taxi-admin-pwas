<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Payments</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Transactions</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Audit Trails</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-primary elevation-4">
        <!-- Brand Logo -->
        <a href="" class="brand-link">
            <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
            <span class="brand-text font-weight-light text-center">
                <h4 class="text-center">SANTACO </h4>
            </span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                </div>
                <div class="info text-bold">
                    <a href="#" class="d-block">
                        {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="{{ route('taxi_assocs_home')  }}" class="nav-link active bg-dark">
                            <i class="nav-icon fas fa-bus"></i>
                            <p>
                                Taxi Associations
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{ route('claimants')  }}" class="nav-link active bg-dark">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Claimants
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link active bg-dark">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Query Management
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link active bg-dark">
                            <i class="nav-icon fas fa-hand-holding-usd"></i>
                            <p>
                                Fund Administrator
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link active bg-dark">
                            <i class="nav-icon fas fa-user-cog"></i>
                            <p>
                                User Management
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">SANTACO | Admin Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach()
                    </div>
            @endif

            <!-- Info boxes -->
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="info-box">
                            <div class="info-box-content">
                                <h4 class="info-box-text">
                                    Registered Taxi Associations
                                </h4>
                                <span class="info-box-number">
                                    {{ $taxi_assoc_count }}
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="info-box mb-6">
                            <div class="info-box-content">
                                <h4 class="info-box-text">
                                    Registered Claimants
                                </h4>
                                <span class="info-box-number">
                                        {{ $claimant_count }}
                                    </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix hidden-md-up"></div>

                </div>
                <!-- /.row -->

                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="col-lg-12">
                            @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                        @endif
                        <!-- Posts list -->
                            <div class="row">
                                <div class="col-lg-12 margin-tb">
                                    <div class="pull-left">
                                        <h2></h2>
                                    </div>
                                </div>
                            </div>
                            <!-- Small boxes (Stat box) -->
                            <div class="row">
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules">
                                            <h4>Taxi Associations Module</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules">
                                            <h4>Claimants Module</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules">
                                            <h4>Query Management Module</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /.row -->

                            <!-- Small boxes (Stat box) -->
                            <div class="row">
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules">
                                            <h4>Fund Administratot Module</h4>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <!-- small box -->
                                    <a href="">
                                        <div class="small-box bg-dark small-box-modules">
                                            <h4>User Management Module</h4>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.card -->

                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->

            </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <strong><a href="http://applord.co">Applord (Pty) Ltd</a>.</strong>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript">
    $("document").ready(function(){
        setTimeout(function(){
            $("div.alert").fadeOut();
        }, 5000 ); // 5 secs

    });
</script>
</body>
</html>
