@role('Taxi association admin')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.min.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        .text-shw{
            color: white;
            font-weight: bold;
            text-shadow: 2px 2px 4px #000000;
        }

        /*END OF Side Menu*/

        .main-content {
            background-color: #ffffff;
        }

        .bx-shw {
            box-shadow: 0px 4px 8px #888888;
            border-radius: 10px;
            border: 0.7px solid #e9ecef;
        }

        .reason-4-decline {
            font-weight: bold;
            font-size: 1.3rem;
        }

        .reason-declined {
            color: #ff2200;
            font-weight: bold;
        }

        .cust-table-theads {
            font-size: 0.95rem;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed" style="padding: 0px !important; margin: 0px !important;">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="" class="nav-link">Admin Portal</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li>
                <a class="dropdown-item" href="http://54.246.148.187/admin/public/taxi_assocs_home">
                    Home
                </a></li>
            <li>
                <!--Lougout -->
                <div class="">
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Navbar
<nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
 Left navbar links
    <ul class="navbar-nav">
        <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Payments</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Transactions</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Audit Trails</a>
        </li>
    </ul>
-->
    <!-- Right navbar links
    <ul class="navbar-nav ml-auto">

    </ul>
</nav>
s/.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link side-nav-logo">
            <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">-->
            <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;">{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
            <br />
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                </div>
                <div class="info text-bold">
                    <a href="#" class="d-block">
                        {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                    </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview" style="padding-top: 10px;">
                        <br /><br />
                        <a href="http://54.246.148.187/admin/public/taxiMain" class="nav-link active side-menu-items">
                            <i class="nav-icon fas fa-bus"></i>
                            <p class="menu-items">
                                All taxi assciations
                                <i class="right fas fa-angle-right"></i>
                            </p>
                        </a>
                        
                        <a href="http://54.246.148.187/admin/public/taxiApproved" class="nav-link active side-menu-items">
                            <i class="nav-icon fas fa-bus"></i>
                            <p class="menu-items">
                                Approved associations
                                <i class="right fas fa-angle-right"></i>
                            </p>
                        </a>
                        
                        <a href="http://54.246.148.187/admin/public/taxiPending" class="nav-link active side-menu-items">
                            <i class="nav-icon fas fa-bus"></i>
                            <p class="menu-items">
                                Pending Associations
                                <i class="right fas fa-angle-right"></i>
                            </p>
                        </a>
                        
                        <a href="http://54.246.148.187/admin/public/taxiDeclined" class="nav-link active side-menu-items">
                            <i class="nav-icon fas fa-bus"></i>
                            <p class="menu-items">
                                Declined Associations
                                <i class="right fas fa-angle-right"></i>
                            </p>
                        </a>
                        <br /><br /><br />
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="nav-icon fas fa-th"></i>
                                    <p>
                                        Home
                                    </p>
                                </a>
                            </li>
                        </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: #ffffff;">


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach()
                    </div>
            @endif



            <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">

                        <div class="col-lg-12 mt-2 main-content">
<br />
<a class="btn btn-default" onclick="goBack()">
                                Back
                            </a>


<?php
$countAr = count($data);
//echo $countAr$data[0]->status; die(); 
if($countAr > 1)
{

	if($data[0]->status === 0)
	{

		?>
@role('Taxi association admin')
			<a class="btn btn-success" href="http://54.246.148.187/admin/public/reprove/<?php echo $id; ?>">
				Approve
			</a>
			<a class="btn btn-success" href="#" data-toggle="modal" data-target="#rejectModal" style="background-color:#ff2200;border: 1px solid #ff2200;">
				Decline
			</a>

@endrole
		<?php
	}

	if($data[0]->status === 2)
	{
		?>
@role('Taxi association admin')
			<a class="btn btn-success" href="http://54.246.148.187/admin/public/reprove/<?php echo $id; ?>">
				Approve
			</a>
@endrole
		<?php
	}
	if($data[0]->status === 1)
	{
		?>
@role('Taxi association admin')
			<a class="btn btn-success" href="#" data-toggle="modal" data-target="#rejectModal" style="background-color:#ff2200;border: 1px solid #ff2200;">
				Decline
			</a>
@endrole
		<?php
	}
}

?>
                        <!-- Posts list -->

                            <div class="row" style="background-color: #ffffff;">
                                <div class="col-lg-12 margin-tb">

                                    <div>
                                        <br />
                                        <input type="text" id="myInput" style="width: 30%;" class="form-control" onkeyup="myFunction()" placeholder="Search for names..">
                                        <br />
                                    </div>
                                </div>
                            </div>

                            <div class="bx-shw">
                                <div class="row text-center ">
                                    <div class="col-xs col-sm col-md">
                                        <h3><?php echo $data[0]->name; ?></h3>
                                    </div>
                                </div>

                                <?php if ($data[0]->status === 2) { ?>
                                <div class="row text-center">
                                    <div class="col-xs col-sm col-md">
                                        <p>
                                            <span class="reason-4-decline">Reason for decline:</span>
                                            <span class="reason-declined"><?php echo $addReason[0]->reason; ?> </span>
                                        </p>
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <table class="table table-striped task-table" id="myTable">
                                            <!-- Table Headings -->
                                            <thead class="text-center cust-table-theads">
                                            <th>Role</th>
                                            <th>ID Number</th>
                                            <th width="12.25%">Name</th>
                                            <th width="12.25%">Surname</th>
                                            <th width="12.25%">Cell Number</th>
                                            <th width="12.25%">Email</th>
                                            <th width="12.25%">Gender</th>
                                            <th width="12.25%">Action</th>
                                            </thead>
                                        <?php
                                        //var_dump($data);
                                        //die();
                                        ?>
                                        <!-- Table Body -->
                                            <tbody class="text-center">
                                            @foreach($data as $datas)
                                                <tr>
                                                    <td class="table-text">
                                                        <div>{{$datas->type}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->idNum}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->personName}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->surname}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->cell}}</div>
                                                    </td>
                                                    <td class="table-text">
                                                        <div>{{$datas->email}}</div>
                                                    </td>
                                                    <td class="ta>ble-text">
                                                        <div>{{$datas->gender}}</div>
                                                    </td>

                                                    <td>

                                                       
                                                        <a href="http://54.246.148.187/admin/public/taxiProfile/{{$datas->taxiassocId}}" class="label label-success" style="margin-left: 15px; font-size: 1.3rem">
<i class="fas fa-eye"></i>
                                                        </a>
                                                     
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->

                    </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->

            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->


    <!-- Main Footer -->
    <footer class="main-footer">
        <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.js') }}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
<script type="text/javascript">
    $("document").ready(function() {
        setTimeout(function() {
            $("div.alert").fadeOut();
        }, 5000); // 5 secs

    });
</script>
<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
<script>
    function goBack() {
        window.history.back();
    }
</script>


<!-- Modal -->
<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form method="post" action="{{ route('decline_reason') }}">
                    @csrf
                    <h4 align="center"><u>Reason for decline</u></h4>
                    <br />
                    <select name="dReason" class="form-control" style="color:#000000;">
                        <option value="Association not in existence">
                            Association not in existence
                        </option>
                        <option value="Association fake">
                            Association fake
                        </option>
                        <option value="Association not a registered association">
                            Association not a registered association
                        </option>
                        <option value="Association illegal">
                            Association illegal
                        </option>

                    </select>

                    <br />
                    <!-- Additional comments:<br/><br/>
<textarea class="form-control" name="addReason">
</textarea>
<br/>-->
                    <input type="hidden" name="id" value="{{ $id }}">
                    <button type="submit" class="btn btn-primary" style="width:100%;background-color: #000000;color: #ffffff;border:1px solid #000000;">Submit</button>
                </form>
            </div>

        </div>
    </div>
</div>
</body>

</html>
@else
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Access Denied</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

        <style>
            * {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            body {
                padding: 0;
                margin: 0;
            }

            #notfound {
                position: relative;
                height: 100vh;
                background: #f6f6f6;
            }

            #notfound .notfound {
                position: absolute;
                left: 50%;
                top: 50%;
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
            }

            .notfound {
                max-width: 767px;
                width: 100%;
                line-height: 1.4;
                padding: 110px 40px;
                text-align: center;
                background: #fff;
                -webkit-box-shadow: 0 15px 15px -10px rgba(0, 0, 0, 0.1);
                box-shadow: 0 15px 15px -10px rgba(0, 0, 0, 0.1);
            }

            .notfound .notfound-404 {
                position: relative;
                height: 180px;
            }

            .notfound .notfound-404 h1 {
                font-family: 'Roboto', sans-serif;
                position: inherit;
                left: 50%;
                top: 50%;
                -webkit-transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
                font-size: 40px;
                font-weight: 700;
                margin: 0px;
                color: #262626;
                text-transform: uppercase;
            }

            .notfound .notfound-404 h1>span {
                color: #e40404;
            }

            .notfound h2 {
                font-family: 'Roboto', sans-serif;
                font-size: 22px;
                font-weight: 400;
                text-transform: uppercase;
                color: #151515;
                margin-top: 0px;
                margin-bottom: 25px;
            }

            .notfound .notfound-search {
                position: relative;
                max-width: 320px;
                width: 100%;
                margin: auto;
            }

            .notfound .notfound-search>input {
                font-family: 'Roboto', sans-serif;
                width: 100%;
                height: 50px;
                padding: 3px 65px 3px 30px;
                color: #151515;
                font-size: 16px;
                background: transparent;
                border: 2px solid #c5c5c5;
                border-radius: 40px;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .notfound .notfound-search>input:focus {
                border-color: #00b7ff;
            }

            .notfound .notfound-search>button {
                position: absolute;
                right: 15px;
                top: 5px;
                width: 40px;
                height: 40px;
                text-align: center;
                border: none;
                background: transparent;
                padding: 0;
                cursor: pointer;
            }

            .notfound .notfound-search>button>span {
                width: 15px;
                height: 15px;
                position: absolute;
                left: 50%;
                top: 50%;
                -webkit-transform: translate(-50%, -50%) rotate(-45deg);
                -ms-transform: translate(-50%, -50%) rotate(-45deg);
                transform: translate(-50%, -50%) rotate(-45deg);
                margin-left: -3px;
            }

            .notfound .notfound-search>button>span:after {
                position: absolute;
                content: '';
                width: 10px;
                height: 10px;
                left: 0px;
                top: 0px;
                border-radius: 50%;
                border: 4px solid #c5c5c5;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .notfound-search>button>span:before {
                position: absolute;
                content: '';
                width: 4px;
                height: 10px;
                left: 7px;
                top: 17px;
                border-radius: 2px;
                background: #c5c5c5;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .notfound .notfound-search>button:hover>span:after {
                border-color: #00b7ff;
            }

            .notfound .notfound-search>button:hover>span:before {
                background-color: #00b7ff;
            }

            @media only screen and (max-width: 767px) {
                .notfound h2 {
                    font-size: 18px;
                }
            }

            @media only screen and (max-width: 480px) {
                .notfound .notfound-404 h1 {
                    font-size: 141px;
                }
            }

        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1 style="margin: 0px;">403</h1>
                <h1><span>ACCESS FORBIDDEN</span></h1>
            </div>
            <h2>You dont have Sufficient Privileges to view this page.</h2>

        </div>
    </div>
    </body>

    </html>
@endrole
