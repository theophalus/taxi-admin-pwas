@role('Taxi association admin')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Santaco | Dashboard</title>
    <!-- Bootstrap 4 -->
    <link href="{{ asset('bootstrap/dist/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Font Awesome Icons -->
    <link href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- overlayScrollbars -->
    <link href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link href="{{ asset('dist/css/adminlte.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        body {
            font-family: Helvetica;
        }

        /*Side Menu*/
        .side-nav-logo {
            box-shadow: 0px 3px 10px #196AC0;
            border-radius: 6px;
            border: 2px solid #196AC0;
        }

        .side-menu-bk {
            background-color: #000000;
            background-image: linear-gradient(#000000, #1C1C1C);
        }

        .side-menu-items {
            border: 1px solid blue;
            border-radius: 20px !important;
            background-image: linear-gradient(#007bff, #0C64C2);
            text-shadow: 0 0 7px black;
        }

        .menu-items {
            font-size: 13px;
        }

        /*END OF Side Menu*/

        .custom-container {
            padding-top: 20px;
        }

        .custom-box {
            box-shadow: 6px 10px 8px #888888;
        }

        .bg-dark:hover {
            background-color: green;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed text-gray-dark layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-light navbar-light text-bold">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Admin Portal</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li>
                    <!--Lougout -->
                    <div class="">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-primary elevation-4 side-menu-bk">
            <!-- Brand Logo -->
            <a href="" class="brand-link side-nav-logo">
                <!--<img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">-->
                <span class="brand-text font-weight-light text-center">

                    <h4 class="text-center"><img src="{{ asset('images/logos-04.jpg') }}" width="230"> </h4>
                    <h4 style="color: white;font-weight: bold;text-shadow: 2px 2px 4px #000000;">{{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} </h4>
                </span>
                <br />
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <!--<img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">-->
                    </div>
                    <div class="info text-bold">
                        <a href="#" class="d-block">
                            {{ Auth::user()->first_name . '  ' . Auth::user()->last_name}} <span class="caret"></span>
                        </a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview" style="padding-top: 12px;">
                            <br /><br />
                            <a href="http://54.246.148.187/admin/public/taxiMain" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-bus"></i>
                                <p class="menu-items">
                                    Taxi Associations
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            
                            <!-- @role('Claimant admin')
                            <a href="{{ route('claimants')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Claimants
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            @endrole -->
                            
                            @role('Query management agent')
                            <a href="{{ route('query_management')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-users"></i>
                                <p class="menu-items">
                                    Query Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            @endrole
                            
                            @role('Fund administrator')
                            <a href="{{ route('fund_administrator')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-hand-holding-usd"></i>
                                <p class="menu-items">
                                    Fund Administrator
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            @endrole
                            
                            @role('Super user')
                            <a href="{{ route('user_management')  }}" class="nav-link active side-menu-items">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p class="menu-items">
                                    User Management
                                    <i class="right fas fa-angle-right"></i>
                                </p>
                            </a>
                            @endrole
                            <br /><br /><br />
                            <ul class="nav nav-treeview">

                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Home
                                        </p>
                                    </a>
                                </li>
                            </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">


            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <!-- Main content -->
                    <section class="content">
                        <div class="container-fluid custom-container">
                            <div class="col-lg-12">
                                <h3><strong>Taxi Association</strong></h3>
                            </div>
                            <br />
                            <div class="col-lg-12">
                                @if(Session::has('success_msg'))
                                <div class="alert alert-success">{{ Session::get('success_msg') }}</div>
                                @endif
                                <!-- Posts list -->
                                <div class="row">

                                </div>
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-2 col-6"></div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="http://54.246.148.187/admin/public/taxiMain">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="text-center">All Taxi Associations</h4>
                                                <h4 class="text-center">{{ $taxi_assoc_count }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="http://54.246.148.187/admin/public/taxiApproved">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="text-center">Approved Associations</h4>
                                                <h4 class="text-center">{{ $member_countss }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-2 col-6"></div>
                                </div>
                                <!-- /.row -->

                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-lg-2 col-6"></div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="http://54.246.148.187/admin/public/taxiPending">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="text-center">Pending Associations</h4>
                                                <h4 class="text-center">{{ $member_counts }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <!-- small box -->
                                        <a href="http://54.246.148.187/admin/public/taxiDeclined">
                                            <div class="small-box bg-dark small-box-modules custom-box">
                                                <h4 class="text-center">Declined Associations</h4>
                                                <h4 class="text-center">{{ $member_countsss }}</h4>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-6"></div>
                                <!-- /.row -->
                            </div>
                            <!-- /.card -->

                        </div>
                        <!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->

                </div>
                <!--/. container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <p style="font-size: 12px; color: #000000;">Copyright &copy; TIRCF 2020 </p>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('dist/js/adminlte.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

    <!-- PAGE SCRIPTS -->
    <script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>
    <script type="text/javascript">
        $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").fadeOut();
            }, 5000); // 5 secs

        });
    </script>
</body>

</html>
@else
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Access Denied</title>

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <style>
        * {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        body {
            padding: 0;
            margin: 0;
        }

        #notfound {
            position: relative;
            height: 100vh;
            background: #f6f6f6;
        }

        #notfound .notfound {
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }

        .notfound {
            max-width: 767px;
            width: 100%;
            line-height: 1.4;
            padding: 110px 40px;
            text-align: center;
            background: #fff;
            -webkit-box-shadow: 0 15px 15px -10px rgba(0, 0, 0, 0.1);
            box-shadow: 0 15px 15px -10px rgba(0, 0, 0, 0.1);
        }

        .notfound .notfound-404 {
            position: relative;
            height: 180px;
        }

        .notfound .notfound-404 h1 {
            font-family: 'Roboto', sans-serif;
            position: inherit;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            font-size: 40px;
            font-weight: 700;
            margin: 0px;
            color: #262626;
            text-transform: uppercase;
        }

        .notfound .notfound-404 h1>span {
            color: #e40404;
        }

        .notfound h2 {
            font-family: 'Roboto', sans-serif;
            font-size: 22px;
            font-weight: 400;
            text-transform: uppercase;
            color: #151515;
            margin-top: 0px;
            margin-bottom: 25px;
        }

        .notfound .notfound-search {
            position: relative;
            max-width: 320px;
            width: 100%;
            margin: auto;
        }

        .notfound .notfound-search>input {
            font-family: 'Roboto', sans-serif;
            width: 100%;
            height: 50px;
            padding: 3px 65px 3px 30px;
            color: #151515;
            font-size: 16px;
            background: transparent;
            border: 2px solid #c5c5c5;
            border-radius: 40px;
            -webkit-transition: 0.2s all;
            transition: 0.2s all;
        }

        .notfound .notfound-search>input:focus {
            border-color: #00b7ff;
        }

        .notfound .notfound-search>button {
            position: absolute;
            right: 15px;
            top: 5px;
            width: 40px;
            height: 40px;
            text-align: center;
            border: none;
            background: transparent;
            padding: 0;
            cursor: pointer;
        }

        .notfound .notfound-search>button>span {
            width: 15px;
            height: 15px;
            position: absolute;
            left: 50%;
            top: 50%;
            -webkit-transform: translate(-50%, -50%) rotate(-45deg);
            -ms-transform: translate(-50%, -50%) rotate(-45deg);
            transform: translate(-50%, -50%) rotate(-45deg);
            margin-left: -3px;
        }

        .notfound .notfound-search>button>span:after {
            position: absolute;
            content: '';
            width: 10px;
            height: 10px;
            left: 0px;
            top: 0px;
            border-radius: 50%;
            border: 4px solid #c5c5c5;
            -webkit-transition: 0.2s all;
            transition: 0.2s all;
        }

        .notfound-search>button>span:before {
            position: absolute;
            content: '';
            width: 4px;
            height: 10px;
            left: 7px;
            top: 17px;
            border-radius: 2px;
            background: #c5c5c5;
            -webkit-transition: 0.2s all;
            transition: 0.2s all;
        }

        .notfound .notfound-search>button:hover>span:after {
            border-color: #00b7ff;
        }

        .notfound .notfound-search>button:hover>span:before {
            background-color: #00b7ff;
        }

        @media only screen and (max-width: 767px) {
            .notfound h2 {
                font-size: 18px;
            }
        }

        @media only screen and (max-width: 480px) {
            .notfound .notfound-404 h1 {
                font-size: 141px;
            }
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body>
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h1 style="margin: 0px;">403</h1>
                <h1><span>ACCESS FORBIDDEN</span></h1>
            </div>
            <h2>You dont have Sufficient Privileges to view this page.</h2>

        </div>
    </div>
</body>

</html>

@endrole