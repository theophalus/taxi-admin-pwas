<p class="mt-8 text-center text-xs text-80">
    <a href="applord.co" class="text-primary dim no-underline">Applord (Pty) Ltd</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} All rights reserved.
    <span class="px-1">&middot;</span>

</p>
